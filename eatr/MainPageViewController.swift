//
//  MainPageViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 04/02/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Firebase

class MainPageViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func goToLogin(){
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginPageID") as! ViewController
        self.present(nextViewController, animated: true, completion: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if(!Reachability.isConnectedToNetwork()){
            showNoInternetMessage()
        }
        if(FBSDKAccessToken.current() == nil || Auth.auth().currentUser == nil){
            if(FBSDKAccessToken.current() == nil){
                print("token nill")
            }
            if(Auth.auth().currentUser == nil){
                print("auth nill")
            }
            goToLogin()
        }
    }

}
