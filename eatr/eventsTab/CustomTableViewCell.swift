//
//  CustomTableViewCell.swift
//  eatr
//
//  Created by Yorick Bolster on 11/02/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var maxPeopleLbl: UILabel!
    @IBOutlet weak var priceRangeLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
