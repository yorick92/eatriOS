//
//  RestaurantsPopUpViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 09/03/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit

class RestaurantsPopUpViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var restaurantsList : [RestaurantItem] = []
    var restaurantsListOriginal : [RestaurantItem] = []
    var restProtocol : pickedRestProtocol?
    var type : String!
    
    @IBOutlet weak var sortText: UILabel!
    @IBOutlet weak var cancelButtonOutlet: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var sortSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        restaurantsListOriginal = restaurantsList
        sortRestaurants()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        cancelButtonOutlet.setTitle("Cancel", for: .normal)

        popUpView.layer.cornerRadius = 15
        popUpView.layer.shadowColor = UIColor.black.cgColor
        popUpView.layer.shadowOffset = CGSize(width: 0, height: 10)
        popUpView.layer.shadowOpacity = 0.9
        popUpView.layer.shadowRadius = 5
    }
    
    func sortRestaurants(){
        if(sortSwitch.isOn){
            restaurantsList = restaurantsList.sorted(by: {Double($0.rating)! > Double($1.rating)!})
            sortText.text = "By rating"
        } else{
            restaurantsList = restaurantsList.sorted(by: {$0.distance < $1.distance})
            sortText.text = "By distance"
        }
        tableView.reloadData()
    }
    
    @IBAction func cancelButton(_ sender: Any) {
        restProtocol?.indexOfPickedRest = -1
        restProtocol?.dismissPresentedView()
    }
    
    @IBAction func sortSwitchChanged(_ sender: UISwitch) {
        sortRestaurants()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurantsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let restCell = tableView.dequeueReusableCell(withIdentifier: "restCell") as! RestTableViewCell
        let restOption = restaurantsList[indexPath.row]
        restCell.nameLbl.text = restOption.name
        if(restOption.rating == "-1.0"){
            restCell.ratingLbl.text = "no rating"
        } else{
            restCell.ratingLbl.text = restOption.rating
        }
        restCell.distanceLbl.text = String(restOption.distance) + " km"
        
        restCell.restImage.image = UIImage(named: "noImageAvailable.png")
        
        if(restOption.priceLevel == -1){
            restCell.priceLevel.text = "Price level ?/3"
        } else{
            restCell.priceLevel.text = "Price level " + String(restOption.priceLevel) + "/3"
        }
        
        
        let session = URLSession(configuration: .default)
        if(restOption.icon != "no icon"){
            let urlString = restOption.icon
            let restImageFromUrl = session.dataTask(with: URL(string: urlString)!){ (data, response, error) in
                if let e = error{
                    print("an error occured: \(e)")
                } else{
                    if(response as? HTTPURLResponse) != nil{
                        if let imageData = data{
                            let theImage = UIImage(data: imageData)
                            DispatchQueue.main.async {
                                restCell.restImage.image = theImage
                            }
                        } else{
                            print("no image found")
                        }
                    } else{
                        print("no server response")
                    }
                }
            }
            restImageFromUrl.resume()
        }else{
            // nothing
        }
        
        return restCell
//        let restCell = tableView.dequeueReusableCell(withIdentifier: "restCell") as! RestTableViewCell
//        let restOption = restaurantsList[indexPath.row]
//        restCell.nameLbl.text = restOption.name
//        if(restOption.rating == "-1.0"){
//            restCell.ratingLbl.text = "no rating"
//        } else{
//           restCell.ratingLbl.text = restOption.rating
//        }
//        restCell.distanceLbl.text = restOption.vicinity
//
//        return restCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let placeIdRest = restaurantsList[indexPath.row].placeId
        print(restaurantsList[indexPath.row].name)
        if let indexOfRest = restaurantsListOriginal.index(where: { $0.placeId == placeIdRest}){
            restProtocol?.indexOfPickedRest = indexOfRest
            _ = UIViewController.displaySpinner(onView: self.view)
            restProtocol?.dismissPresentedView()
        }
    }
    
}
