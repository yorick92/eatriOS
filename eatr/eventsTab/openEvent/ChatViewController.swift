//
//  ChatViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 06/04/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseStorage
import Firebase
import CoreData

class ChatViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextViewDelegate {
    var thisEventChatRef: DatabaseReference?
    var messages = [message]()
    var userColorCombo = [String: UIColor]()
    var colors = [UIColor]()
    var eventId : String?
    var userId : String?
    var eventName : String?
    var context : NSManagedObjectContext!
    
    @IBOutlet weak var textFieldMessage: UITextView!
    @IBOutlet weak var sendButtonOutlet: UIButton!
    @IBOutlet weak var chatMain: UICollectionView!

    func deleteAllData(entity: String)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            for managedObject in results
            {
                managedContext.delete(managedObject as! NSManagedObject)
            }
        } catch let error as NSError {
            print("Detele all data in \(entity) error : \(error) \(error.userInfo)")
        }
        
        do{
            try managedContext.save()
        } catch{
            print("could not save deletions into local storage")
        }
    }
    
    func createColors(){
        colors.append(UIColor(red: 128, green: 0, blue: 0))
        colors.append(UIColor(red: 128, green: 128, blue: 0))
        colors.append(UIColor(red: 0, green: 128, blue: 0))
        colors.append(UIColor(red: 128, green: 0, blue: 128))
        colors.append(UIColor(red: 0, green: 128, blue: 128))
        colors.append(UIColor(red: 0, green: 0, blue: 128))
        colors.append(UIColor(red: 220, green: 20, blue: 60))
        colors.append(UIColor(red: 255, green: 127, blue: 80))
        colors.append(UIColor(red: 250, green: 128, blue: 114))
        colors.append(UIColor(red: 255, green: 165, blue: 0))
        colors.append(UIColor(red: 184, green: 134, blue: 11))
        colors.append(UIColor(red: 238, green: 232, blue: 170))
        colors.append(UIColor(red: 124, green: 252, blue: 0))
        colors.append(UIColor(red: 0, green: 100, blue: 0))
        colors.append(UIColor(red: 32, green: 178, blue: 170))
        colors.append(UIColor(red: 64, green: 224, blue: 208))
        colors.append(UIColor(red: 100, green: 149, blue: 237))
        colors.append(UIColor(red: 139, green: 0, blue: 139))
        colors.append(UIColor(red: 255, green: 192, blue: 203))
    }
    
    func setUpOutlets(){
        sendButtonOutlet.layer.borderWidth = 0.5
        textFieldMessage.layer.borderWidth = 0.5
        self.title = eventName! + " chat"
        thisEventChatRef = Database.database().reference().child("eventChats").child(eventId!)
        
        if(!Reachability.isConnectedToNetwork()){
            sendButtonOutlet.isEnabled = false
        }
    }
    
    func chatExists(eventId : String)->NSManagedObject?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "EventChat")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "eventId") as! String == eventId}){
                    return result as? NSManagedObject;
                } else {
                    return nil
                }
            }
        } catch{
            return nil
        }
        return nil
    }
    
    func messageExists(messageId : String)->NSManagedObject?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Message")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "messageId") as! String == messageId}){
                    return result as? NSManagedObject;
                } else {
                    return nil
                }
            }
        } catch{
            return nil
        }
        return nil
    }
    
    func loadEventChatFromCoreData(eventId : String){
        let theObject = chatExists(eventId: eventId)
        
        if(theObject == nil){
            // do nothing
        } else{
            messages = [message]()
            let messagesSet : NSSet = theObject!.value(forKey: "messages") as! NSSet
            for messageAny in messagesSet.allObjects{
                if let aMessage = messageAny as? NSManagedObject{
                    let senderIdString = aMessage.value(forKey: "senderId") as! String
                    let messageString = aMessage.value(forKey: "message") as! String
                    let dateInMsString = aMessage.value(forKey: "dateInMs") as! String
                    let messageIdString = aMessage.value(forKey: "messageId") as! String
                    let userName = aMessage.value(forKey: "userName") as! String
                    
                    self.messages.append(message(senderId: senderIdString, message: messageString, dateInMs: dateInMsString, messageId: messageIdString, userName: userName))
                }
            }
        }
        messages = messages.sorted(by: {$0.dateInMs < $1.dateInMs})
        if self.messages.count > 0 {
            self.chatMain.scrollToItem(at:IndexPath(item: (self.messages.count-1), section: 0), at: .bottom, animated: false)
        }
    }
    
    func getMessages(){
        if(!Reachability.isConnectedToNetwork()){ // no internet get stored messages
            loadEventChatFromCoreData(eventId: eventId!)
        } else { // internet, get latest messages
            thisEventChatRef?.observe(DataEventType.value, with: { (snapshot) in
                let enumerator = snapshot.children
                self.messages = [message]()
                var userIds = [String]()
                var messagesForChat = NSSet()
                while let rest = enumerator.nextObject() as? DataSnapshot {
                    if let dict = rest.value as? [String: String]{
                        
                        let messageString = dict["message"]!
                        let dateInMsString = dict["date"]!
                        let senderIdString = dict["senderId"]!
                        print(rest.key)
                        
                        let theMessage = self.messageExists(messageId: rest.key)
                        if(theMessage != nil){
                            print("message is nil")
                            theMessage?.setValue(senderIdString, forKey: "senderId")
                            theMessage?.setValue(messageString, forKey: "message")
                            theMessage?.setValue(dateInMsString, forKey: "dateInMs")
                            messagesForChat = messagesForChat.adding(theMessage! as Any) as NSSet
                        } else {
                            print("new message made")
                            let newMessage = NSEntityDescription.insertNewObject(forEntityName: "Message" , into: self.context)
                            newMessage.setValue(senderIdString, forKey: "senderId")
                            newMessage.setValue(messageString, forKey: "message")
                            newMessage.setValue(dateInMsString, forKey: "dateInMs")
                            newMessage.setValue(rest.key, forKey: "messageId")
                            messagesForChat = messagesForChat.adding(newMessage as Any) as NSSet
                        }
                        
                        let theContact = self.contactExists(contactId: senderIdString, entityName: "MyContact", context: self.context)
                        
                        var userName = ""
                        if(theContact != nil){
                            userName = theContact?.value(forKey: "name") as! String
                        }
                        
                        self.messages.append(message(senderId: senderIdString, message: messageString, dateInMs: dateInMsString, messageId: rest.key, userName: userName))
                        if !userIds.contains(senderIdString){
                            userIds.append(senderIdString)
                        }
                    }
                }
                
                let theChat = self.chatExists(eventId: self.eventId!)
                theChat?.setValue(messagesForChat, forKey: "messages")
                
                do{
                    try self.context.save()
                } catch{
                    print("could not save eventchat into local storage")
                }
                
                var counter = 0
                for id in userIds{
                    self.userColorCombo[id] = self.colors[counter % self.colors.count]
                    counter += 1
                }
                
                self.chatMain.reloadData()
                if self.messages.count > 0 {
                    self.chatMain.scrollToItem(at:IndexPath(item: (self.messages.count-1), section: 0), at: .bottom, animated: false)
                }
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//
//        deleteAllData(entity: "Message")
//        deleteAllData(entity: "EventChat")
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        
        let theChat = self.chatExists(eventId: eventId!)
        if(theChat != nil){
            // nothing
        } else {
            let newChat = NSEntityDescription.insertNewObject(forEntityName: "EventChat" , into: self.context)
            newChat.setValue(eventId!, forKey: "eventId")
        }
        do{
            try context.save()
        } catch{
            print("could not save deletions into local storage")
        }
        
        createColors()
        setUpOutlets()
        getMessages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.hidesBarsOnSwipe = false
    }
    
    @IBAction func send(_ sender: UIButton) {
        if(textFieldMessage.text != nil && textFieldMessage.text != ""){
            let miliseconds : String! = "\(Date().millisecondsSince1970)"
            let message = ["senderId" : userId, "message": textFieldMessage.text, "date" : miliseconds]
            thisEventChatRef?.childByAutoId().setValue(message)
            textFieldMessage.text = ""
        }
    }
    
    @objc func tapFunction(sender:MyTapGesture) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileOtherID") as! OpenOtherProfileViewController
        nextViewController.profileUserId = sender.userId
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: 250, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrame = NSString(string: messages[indexPath.item].message).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)], context: nil)
        if(messages[indexPath.row].senderId == userId!){
            return CGSize(width: view.frame.width, height: estimatedFrame.height + 20)
        } else{
            return CGSize(width: view.frame.width, height: estimatedFrame.height + 40)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    func getDateString(timeInMs: Double) -> String{
        let date = Date(timeIntervalSince1970: (timeInMs))
        let dateToday = Date()
        let calendar = Calendar.current
        
        let day = calendar.component(.day, from: date)
        let month = calendar.component(.month, from: date)
        let year = calendar.component(.year, from: date)
        
        let dayToday = calendar.component(.day, from: dateToday)
        let monthToday = calendar.component(.month, from: dateToday)
        let yearToday = calendar.component(.year, from: dateToday)
        
        var timeString : String = ""
        if(day == dayToday && month == monthToday && year == yearToday){
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm"
            timeString = "today at " + dateFormatter.string(from: date)
        } else{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy hh:mm"
            timeString = dateFormatter.string(from: date)
        }
        return timeString
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        return changedText.count <= 400
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ChatCell", for: indexPath) as! ChatCollectionViewCell
        cell.chatCellView.layer.cornerRadius = 10.0
        cell.messageText.layer.cornerRadius = 10.0
        
        cell.messageText.text = messages[indexPath.row].message
        
        let timeString = getDateString(timeInMs: Double(messages[indexPath.row].dateInMs)! / 1000)
        cell.timeLbl.text = timeString
        
        let size = CGSize(width: 250, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrameTextMessage = NSString(string: messages[indexPath.item].message).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)], context: nil)
        
        let estimatedFrameName = NSString(string: messages[indexPath.item].senderId).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 14)], context: nil)
        
        let estimatedFrameTime = NSString(string: timeString).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11)], context: nil)
        
        let widthOfCellViewOther = max(estimatedFrameTextMessage.width, max(estimatedFrameTime.width, estimatedFrameName.width))
        let widthOfCellViewSelf = max(estimatedFrameTextMessage.width, estimatedFrameTime.width)
        
        if(messages[indexPath.row].senderId == userId!){
            cell.chatCellView.backgroundColor = UIColor(red: 77, green: 194, blue: 71)
            cell.messageText.backgroundColor = UIColor(red: 77, green: 194, blue: 71)
            
            cell.chatCellView.frame = CGRect(x: (cell.frame.width - (widthOfCellViewSelf + 24))-8,
                                             y:0,
                                             width: widthOfCellViewSelf + 24,
                                             height: estimatedFrameTextMessage.height+20)
            cell.nameLbl.frame = CGRect(x: 8,
                                        y:0,
                                        width: 0,
                                        height: 0)
            cell.messageText.frame = CGRect(x: 8,
                                            y:0,
                                            width: widthOfCellViewSelf + 16,
                                            height: estimatedFrameTextMessage.height+20)
            cell.timeLbl.frame = CGRect(x: ((widthOfCellViewSelf + 24 - estimatedFrameTime.width)-5),
                                        y : estimatedFrameTextMessage.height+20-estimatedFrameTime.height,
                                        width: estimatedFrameTime.width,
                                        height: estimatedFrameTime.height)
            
        } else{
            cell.chatCellView.backgroundColor = UIColor.white
            cell.messageText.backgroundColor = UIColor.white
            
            cell.chatCellView.frame = CGRect(x: 8,
                                             y:0,
                                             width: widthOfCellViewOther + 24,
                                             height: estimatedFrameTextMessage.height+40)
            cell.nameLbl.frame = CGRect(x: 8,
                                        y:0,
                                        width: widthOfCellViewOther + 24,
                                        height: 20)
            cell.messageText.frame = CGRect(x: 8,
                                            y:20,
                                            width: widthOfCellViewOther + 16,
                                            height: estimatedFrameTextMessage.height+20)
            cell.timeLbl.frame = CGRect(x: (widthOfCellViewOther + 24 - estimatedFrameTime.width)-5,
                                        y : estimatedFrameTextMessage.height+40-estimatedFrameTime.height,
                                        width: estimatedFrameTime.width,
                                        height: estimatedFrameTime.height)
            
            cell.nameLbl.text = messages[indexPath.row].userName
            cell.nameLbl.textColor = userColorCombo[messages[indexPath.row].senderId]
            let tap = MyTapGesture(target: self, action: #selector(tapFunction))
            tap.userId = messages[indexPath.row].senderId
            cell.nameLbl.isUserInteractionEnabled = true
            cell.nameLbl.addGestureRecognizer(tap)
        }
        
        cell.messageText.isEditable = false
        
        return cell
    }
}
