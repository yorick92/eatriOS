//
//  AddMorePeopleViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 16/04/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Firebase
import CoreData

class AddMorePeopleViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UICollectionViewDataSource, UICollectionViewDelegate {
    var usersReference : DatabaseReference?
    var eventInviteesReference : DatabaseReference?
    var localFacebookFriends = [localContactStruct]()
    var localFacebookFriendsCurrent = [localContactStruct]()
    var friendsToInvite = [localContactStruct]()
    var alreadyInvited = [localContactStruct]()
    var addMorePeopleProtocol : addMoreInvitesProtocol?
    var context : NSManagedObjectContext!
    var userId : String?
    var eventId : String!
    
    @IBOutlet weak var contactsList: UITableView!
    @IBOutlet weak var contactsCollectionView: UICollectionView!
    @IBOutlet weak var contactsSearchBar: UISearchBar!
    
    func setUpDatabaseReferences(){
        usersReference = Database.database().reference().child("Users")
        eventInviteesReference = Database.database().reference().child("EventInvitees")
    }
    
    func setUpVariables(){
        userId = Auth.auth().currentUser!.uid
    }
    
    func loadFriends(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MyContact")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                for result in results as! [NSManagedObject]{
                    let theId = result.value(forKey: "userId") as! String
                    let theName = result.value(forKey: "name") as! String
                    let theFavCuis = result.value(forKey: "favCuis") as! String
                    let theFavRest = result.value(forKey: "favRest") as! String
                    let theGender = result.value(forKey: "gender") as! String
                    let picChangedAmount = result.value(forKey: "picChangedAmount") as! String
                    let timeLastChanged = result.value(forKey: "timeLastChanged") as! String
                    
                    if(!self.alreadyInvited.contains(where: {x in x.id == theId})){
                        self.localFacebookFriends.append(localContactStruct(id: theId, name: theName, favCuis: theFavCuis, favRest: theFavRest, gender: theGender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
                    }
                    
                }
            }
        } catch{
            print("error")
        }
        
        do{
            try self.context.save()
            print("got myContacts")
        } catch{
            
        }
        self.localFacebookFriendsCurrent = self.localFacebookFriends
        self.contactsList.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        
        setUpDatabaseReferences()
        setUpVariables()
        loadFriends()
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        addMorePeopleProtocol?.dismissMoreInvites()
    }

    @IBAction func done(_ sender: UIButton) {
        // save to database
        let myGroup = DispatchGroup()
        for friend in (friendsToInvite){
            myGroup.enter()
            eventInviteesReference?.child(eventId).child(friend.id!).setValue(friend.id!){ (error, snapshot) in
                myGroup.leave()
            }
        }
        myGroup.notify(queue: .main){
            // save to main memory
            self.addMorePeopleProtocol?.newInvites = self.friendsToInvite
            self.addMorePeopleProtocol?.dismissMoreInvites()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return friendsToInvite.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContactCell", for: indexPath) as! ContactCollectionViewCell
        cell.contactName.text = friendsToInvite[indexPath.row].name
        cell.contactImage.image = UIImage(named: "profile_icon.png")
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        localFacebookFriendsCurrent.append(friendsToInvite[indexPath.row])
        localFacebookFriends.append(friendsToInvite[indexPath.row])
        friendsToInvite.remove(at: indexPath.row)
        
        contactsList.reloadData()
        contactsCollectionView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return localFacebookFriendsCurrent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCellID") as! ContactCellTableViewCell
        cell.nameLbl.text = localFacebookFriendsCurrent[indexPath.row].name
        cell.profilePic.image = UIImage(named: "profile_icon.png")
        cell.cuisineLbl.text = "Cuisine: " + localFacebookFriendsCurrent[indexPath.row].favCuis!
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        friendsToInvite.append(localFacebookFriendsCurrent[indexPath.row])
        localFacebookFriendsCurrent.remove(at: indexPath.row)
        
        contactsList.reloadData()
        contactsCollectionView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            localFacebookFriendsCurrent = localFacebookFriends
            contactsList.reloadData()
            return}
        
        localFacebookFriendsCurrent = localFacebookFriends.filter({ (localFriend) -> Bool in
            guard let text = searchBar.text else {return false}
            return (localFriend.name?.lowercased().contains(text.lowercased()))!
        })
        
        contactsList.reloadData()
    }
}
