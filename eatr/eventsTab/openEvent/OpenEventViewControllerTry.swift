//
//  OpenEventViewControllerTry.swift
//  eatr
//
//  Created by Yorick Bolster on 22/05/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import FirebaseDatabase
import CoreLocation
import GooglePlacePicker
import GoogleMaps
import CoreData

class OpenEventViewControllerTry: UIViewController, UIScrollViewDelegate, pickedRestProtocol, CLLocationManagerDelegate, pickedTimeProtocol, pickedPriceRangeProtocol, addMoreInvitesProtocol, GMSPlacePickerViewControllerDelegate {
    
    var theWidth : Double!
    var theViewHeight : Double!
    var theScrollHeight : Double!
    
    var theScrollView : UIScrollView!
    var eventPic : UIImageView!
    var viewForBackground : UIView!
    
    /******* VARIABLES *******/
    
    var Db : DatabaseReference?
    var usersReference : DatabaseReference?
    var eventRef : DatabaseReference?
    var eventInviteesReference : DatabaseReference?
    var eventPicRef : StorageReference?
    var newInvites = [localContactStruct]()
    var invitedFriends = [localContactStruct]()
    var attendingFriends = [localContactStruct]()
    var notRepondedFriends = [localContactStruct]()
    var allRestaurantOptions = [RestaurantItem]()
    var theEvent: Event?
    var eventId : String?
    var userId : String?
    var latitude : String?
    var longitude : String?
    var pickedLatitude : String?
    var pickedLongitude : String?
    var creator : String?
    var indexOfPickedRest : Int = -1
    var newMin: Int = -1
    var newMax: Int = -1
    var isCreator = false
    var hasJoined = false
    var newDate = Date()
    let myDispGroup = DispatchGroup()
    let locationManager = CLLocationManager()
    var context : NSManagedObjectContext!
    var refreshControl = UIRefreshControl()
    var reloadProtocol : reloadAllDataProtocol!
    var theImage : UIImage?
    
    var lastHeight : CGFloat = 0
    
    var showLocationInfoView : UITextView!
    var showLocationImageView : UIImageView!
    var dateInfoView : UITextView!
    var costInfoView : UITextView!
    var showCreatorImageView : UIImageView!
    var creatorInfoView : UITextView!
    var changeLocationButton : UIButton!
    var changePriceRangeButton : UIButton!
    var changeTimeButton : UIButton!
    
    var costButtonsView : UIView!
    var dateButtonsView : UIView!
    var creatorPartView : UIView!
    var LocationButtonsView : UIView!
    var eventInfoView : UIView!
    var topButtonsView : UIView!
    var remarksView : UIView!
    
    var addMoreAttendeesButton : UIButton!
    
    var attendeesView : UIView!
    var attendeesExpanded = false
    var collexpAttendeesButton : UIButton!
    
    var notRespondedView : UIView!
    var notRespondedExpanded = false
    var collexpNotRespondedButton : UIButton!
    
    var deleteButton : UIButton!
    
    var leaveButton : UIButton!
    var chatButton : UIButton!
    var spinnerView : UIView!
    
    var attendDeclineView : UIView!
    var declineButton : UIButton!
    var attendButton : UIButton!
    
    var attendeesViews = [UIView]()
    var notRespondedViews = [UIView]()
    
    /******* HELPER AND SETUP FUNCTIONS *******/
    
    func addToLastHeight(lastHeightLocal : CGFloat, viewHeight : CGFloat)->CGFloat{
        return lastHeightLocal + viewHeight + CGFloat(20)
    }
    
    func getString(object : Any?) -> String{
        if let a = object as? String{
            return a
        } else{
            return ""
        }
    }
    
    @objc func goBack(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func setUpLocationManager(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let originalY = ((self.navigationController?.navigationBar.frame.height)! + 20)
        if((scrollView.contentOffset.y + 64) < originalY){
            self.eventPic.frame.origin.y = originalY - (scrollView.contentOffset.y + 64)
        }
    }
    
    func setUpDatabaseReferences(){
        Db = Database.database().reference()
        eventRef = Db?.child("Events").child(eventId!)
        usersReference = Db?.child("Users")
        eventInviteesReference = Database.database().reference().child("EventInvitees")
        eventPicRef = Storage.storage().reference(withPath: "pics/Events/" + self.eventId! + "/eventPic.jpg")
    }
    
    func findOutIfCreator(){
        if (theEvent?.creator == self.userId!){
            self.isCreator = true
            if(theEvent?.joinedPeople.contains(where: {$0.id! == userId!}))!{
                self.hasJoined = true
            } else{
                self.hasJoined = false
            }
        } else {
            if(theEvent?.joinedPeople.contains(where: {$0.id! == userId!}))!{
                self.hasJoined = true
            } else{
                self.hasJoined = false
            }
        }
    }
    
    func setUpNavigationController(){
        self.title = theEvent?.eventName
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12)]
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "< back", style: .plain, target: self, action: #selector(goBack))
        
//        self.eventPic.frame.origin.y = 64 //((self.navigationController?.navigationBar.frame.height)! + 20)
//        self.viewForBackground.frame.origin.y = 200 //self.eventPic.frame.origin.y + self.eventPic.frame.height - 64
    }
    
    func getEventPic(){
        var nrOfTimesPicChanged = "0"
        if theEvent?.picChangedAmount != ""{
            nrOfTimesPicChanged = (theEvent?.picChangedAmount)!
        }
        
        if(UserDefaults.standard.object(forKey: self.eventId! + "/" + nrOfTimesPicChanged ) != nil){
            print("from storage")
            let picData = UserDefaults.standard.object(forKey: self.eventId! + "/" + nrOfTimesPicChanged) as! NSData
            self.eventPic.image = UIImage(data: picData as Data)
        } else {
            // remove old one from local database
            var amountChanged : Int = Int(nrOfTimesPicChanged)!
            amountChanged -= 1
            let lastEventChangedAmount = String(amountChanged)
            UserDefaults.standard.removeObject(forKey: self.eventId! + "/" + lastEventChangedAmount)
            
            print("from database")
            //            self.eventPicRef = Storage.storage().reference(withPath: "pics/Events/" + self.eventId! + "/eventPic.jpg")
            self.eventPicRef!.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    print(error)
                    DispatchQueue.main.async {
                        let image = UIImage(named: "noImageAvailable.png")
                        let imageData : NSData = UIImagePNGRepresentation(image!)! as NSData
                        UserDefaults.standard.set(imageData, forKey: self.eventId! + "/" + nrOfTimesPicChanged )
                        self.eventPic.image = image
                    }
                } else {
                    let image = UIImage(data: data!)
                    let imageData : NSData = UIImagePNGRepresentation(image!)! as NSData
                    UserDefaults.standard.set(imageData, forKey: self.eventId! + "/" + nrOfTimesPicChanged )
                    DispatchQueue.main.async {
                        self.eventPic.image = image
                    }
                }
            }
        }
    }
    
    func setUpVariables(){
        // this is changed if you set a new date.
        newDate = (theEvent?.dateStart)!
        eventId = theEvent?.eventId
        userId = Auth.auth().currentUser!.uid
        
        attendeesExpanded = false
        notRespondedExpanded = false
        attendeesViews = [UIView]()
        notRespondedViews = [UIView]()
        lastHeight = CGFloat(0)
        
        attendingFriends = [localContactStruct]()
        notRepondedFriends = [localContactStruct]()
    }
    
    func getPeopleInfo(){
        self.invitedFriends = (theEvent?.invitedPeople)!
        self.attendingFriends = (theEvent?.joinedPeople)!
        for friend in self.invitedFriends{
            if attendingFriends.index(where: { $0.id == friend.id }) == nil {
                self.notRepondedFriends.append(friend)
            }
        }
        
        self.findOutIfCreator()
    }
    
    /*** SETTING VIEWS ***/ //todo move to a different class
    
    func setUpParentViewsAndImage(){
        theScrollHeight = 1000
        theScrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: theWidth, height: theViewHeight))
        theScrollView.contentSize = CGSize(width: theWidth, height: theScrollHeight)
        theScrollView.delegate = self
        view.addSubview(theScrollView)
        
        let eventPicHeight = 200.0
        eventPic = UIImageView(frame: CGRect(x: 0, y: 0, width: theWidth, height: eventPicHeight))
        eventPic.image = UIImage(named: "noImageAvailable.png")
        view.addSubview(eventPic)
        view.sendSubview(toBack: eventPic)
        
        viewForBackground = UIView(frame: CGRect(x: 0, y: eventPicHeight, width: theWidth, height: theScrollHeight))
        viewForBackground.backgroundColor = UIColor.white
        theScrollView.addSubview(viewForBackground)
    }
    
    func removeAttendDeclineView(){
        if(attendDeclineView != nil){
            attendDeclineView.removeFromSuperview()
        }
        if(attendButton != nil){
            attendButton.removeFromSuperview()
        }
        if(declineButton != nil){
            declineButton.removeFromSuperview()
        }
    }
    
    func makeChatButton(theView : UIView){
        removeAttendDeclineView()
        let xValue = CGFloat(0)
        let yValue = CGFloat(0)
        let widthValue = theView.frame.width
        let heightValue = CGFloat(40)
        
        chatButton = UIButton(frame: CGRect(x: xValue, y: yValue, width: widthValue, height: heightValue))
        chatButton.backgroundColor = UIColor.white
        chatButton.addTarget(self, action: #selector(openChat), for: .touchUpInside)
        
        chatButton.addBottomBorderWithColor(color: UIColor.gray, width: 1)
        
        let icon = UIImage(named: "chat_icon.png")
        chatButton.setImage(icon, for: .normal)
        chatButton.imageView?.contentMode = .scaleAspectFit
        let toLeft = chatButton.frame.width/2
        chatButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: -toLeft, bottom: 10, right: 0)
        
        chatButton.setTitleColor(UIColor.black, for: .normal)
        chatButton.setTitle("Open chat", for: .normal)
        chatButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: -65 , bottom: 0, right: 0)
        
        theView.addSubview(chatButton)
    }
    
    func makeLeaveButton(theView : UIView){
        removeAttendDeclineView()
        
        let xValue = CGFloat(0)
        let yValue = CGFloat(40)
        let widthValue = theView.frame.width
        let heightValue = CGFloat(40)
        
        leaveButton = UIButton(frame: CGRect(x: xValue, y: yValue, width: widthValue, height: heightValue))
        leaveButton.backgroundColor = UIColor.white
        leaveButton.addTarget(self, action: #selector(leaveEvent), for: .touchUpInside)
        
        let icon = UIImage(named: "cancel_icon.png")
        leaveButton.setImage(icon, for: .normal)
        leaveButton.imageView?.contentMode = .scaleAspectFit
        let toLeft = leaveButton.frame.width/2
        leaveButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: -toLeft, bottom: 10, right: 0)
        
        leaveButton.setTitleColor(UIColor.black, for: .normal)
        leaveButton.setTitle("Leave event", for: .normal)
        leaveButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: (toLeft * -1.3) , bottom: 0, right: 0)
        
        theView.addSubview(leaveButton)
    }
    
    func makeAttendDeclineButton(theView : UIView){
        if(leaveButton != nil){
            leaveButton.removeFromSuperview()
        }
        if(chatButton != nil){
            chatButton.removeFromSuperview()
        }
        
        /*** picture and message ***/
        attendDeclineView = UIView(frame: CGRect(x: 0, y: 0, width: theView.frame.width, height: 50))
        let localImageView = UIImageView(frame: CGRect(x: 0, y: 5 , width: 40, height: 40))
        localImageView.makePortrait()
        localImageView.image = UIImage(named: "profile_icon.png")
        localImageView.backgroundColor = UIColor.white
        
        let contactObject = contactExists(contactId: (theEvent?.creator)!, entityName: "Contact", context: self.context)
        let contactName = contactObject?.value(forKey: "name") as! String
        let contactPicChangedAmount = contactObject?.value(forKey: "picChangedAmount") as! String
        
        getPicDataForUser(theUserId: (theEvent?.creator)!, picChangedAmount: contactPicChangedAmount, completionHandler: {picData in
            localImageView.image = UIImage(data: picData as Data)
        })
        attendDeclineView.addSubview(localImageView)
        
        let localTextView = UITextView(frame: CGRect(x: 40, y: 0, width: theView.frame.width - 40, height: 50))
        localTextView.font = .systemFont(ofSize: 14)
        localTextView.text = contactName + " invited you to join everyone! \nLet " + contactName + " know if you're attending."
        attendDeclineView.addSubview(localTextView)
        
        /*** attend button ***/
        attendButton = UIButton(frame: CGRect(x: 0, y: 50, width: theView.frame.width/2, height: 30))
        attendButton.backgroundColor = UIColor.white
        attendButton.addTarget(self, action: #selector(attendEvent), for: .touchUpInside)
        
        let checkIcon = UIImage(named: "check_icon.png")
        attendButton.setImage(checkIcon, for: .normal)
        attendButton.imageView?.contentMode = .scaleAspectFit
        attendButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: -120, bottom: 5, right: 0)
        
        /*** decline button ***/
        declineButton = UIButton(frame: CGRect(x: theView.frame.width/2, y: 50, width: theView.frame.width/2, height: 30))
        declineButton.backgroundColor = UIColor.white
        declineButton.addTarget(self, action: #selector(declineEvent), for: .touchUpInside)
        
        let crossIcon = UIImage(named: "cross_icon.png")
        declineButton.setImage(crossIcon, for: .normal)
        declineButton.imageView?.contentMode = .scaleAspectFit
        declineButton.imageEdgeInsets = UIEdgeInsets(top: 5, left: 120, bottom: 5, right: 0)
        
        /*** adding the views ***/
        theView.addSubview(declineButton)
        theView.addSubview(attendButton)
        theView.addSubview(attendDeclineView)
    }
    
    func setTopButtons(){
        let xValue = viewForBackground.frame.origin.x
        let yValue = viewForBackground.frame.origin.y
        let widthValue = viewForBackground.frame.width
        let heightValue = CGFloat(80)
        
        if(topButtonsView != nil){
            topButtonsView.removeFromSuperview()
        }
        
        topButtonsView = UIView(frame: CGRect(x: xValue, y: yValue + lastHeight, width: widthValue, height: heightValue))
        lastHeight = addToLastHeight(lastHeightLocal: lastHeight, viewHeight: heightValue)
        topButtonsView.addShadow()
        
        topButtonsView.backgroundColor = UIColor.white
        
        self.theScrollView.addSubview(topButtonsView)
        
        if(isCreator && !(theEvent?.isGroupEvent)! ){
            // make only a chat button
            makeChatButton(theView: topButtonsView)
        } else if(isCreator && (theEvent?.isGroupEvent)! && hasJoined){
            makeChatButton(theView: topButtonsView)
            makeLeaveButton(theView: topButtonsView)
            // make chat button and leave button
        } else if(hasJoined){
            makeChatButton(theView: topButtonsView)
            makeLeaveButton(theView: topButtonsView)
        } else if(!hasJoined){
            makeAttendDeclineButton(theView : topButtonsView)
        }
    }
    
    func makeLocationButtons(eventInfoView : UIView){
        let totalWidth = viewForBackground.frame.width
        
        // add the full width view
        LocationButtonsView = UIView(frame: CGRect(x: 0, y: 0, width: totalWidth, height: 80))
        LocationButtonsView.backgroundColor = UIColor.white
        LocationButtonsView.layer.borderColor = UIColor.lightGray.cgColor
        LocationButtonsView.layer.borderWidth = 1
        
        let theViewsHeight = LocationButtonsView.frame.height
        // add the get route button
        let getRouteButton = UIButton(frame: CGRect(x: 0.0, y: 0.0, width: totalWidth/6, height: theViewsHeight))
        getRouteButton.addTarget(self, action: #selector(getRoute), for: .touchUpInside)
        getRouteButton.setImage(UIImage(named: "pin_icon.png"), for: .normal)
        getRouteButton.imageView?.contentMode = .scaleAspectFit
        getRouteButton.imageEdgeInsets = UIEdgeInsets(top: 30, left: 0, bottom: 30, right: 0)
        LocationButtonsView.addSubview(getRouteButton)
        
        // add the location image view
        showLocationImageView = UIImageView(frame: CGRect(x: totalWidth/6, y: (80-totalWidth/6)/2, width: totalWidth/6, height: totalWidth/6))
        showLocationImageView.image = self.eventPic.image
        showLocationImageView.contentMode = .scaleAspectFill
        showLocationImageView.makePortrait()
        LocationButtonsView.addSubview(showLocationImageView)
        
        // add the location info view
        showLocationInfoView = UITextView(frame: CGRect(x: totalWidth/3, y: 0, width: totalWidth/2, height: theViewsHeight))
        showLocationInfoView.text = (theEvent?.locationName)! + " \n" + "stars strimg" + "\nopen now?" + "\ndistance in km"
        showLocationInfoView.font = UIFont.systemFont(ofSize: 14)
        showLocationInfoView.isEditable = false
        showLocationInfoView.isScrollEnabled = false
        LocationButtonsView.addSubview(showLocationInfoView)
        
        if(isCreator){
            // add change button
            changeLocationButton = UIButton(frame: CGRect(x: (totalWidth/6)*5, y: 0, width: totalWidth/6, height: theViewsHeight))
            changeLocationButton.addTarget(self, action: #selector(changeEventLocation), for: .touchUpInside)
            changeLocationButton.setImage(UIImage(named: "pencil_icon.png"), for: .normal)
            changeLocationButton.imageView?.contentMode = .scaleAspectFit
            changeLocationButton.imageEdgeInsets = UIEdgeInsets(top: 30, left: 0, bottom: 30, right: 0)
            LocationButtonsView.addSubview(changeLocationButton)
        }
        
        eventInfoView.addSubview(LocationButtonsView)
    }
    
    func makeDateButtons(eventInfoView : UIView){
        let totalWidth = viewForBackground.frame.width
        
        dateButtonsView = UIView(frame: CGRect(x: 0.0, y: 80.0, width: totalWidth, height: 60.0))
        dateButtonsView.backgroundColor = UIColor.white
        dateButtonsView.layer.borderColor = UIColor.lightGray.cgColor
        dateButtonsView.layer.borderWidth = 1
        
        let theViewsHeight = dateButtonsView.frame.height
        
        // add icon
        let dateView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: totalWidth/6, height: theViewsHeight))
        dateView.backgroundColor = UIColor.white
        let dateIconImageView = UIImageView(frame: CGRect(x: (totalWidth/6-20)/2, y: 20, width: 20, height: 20))
        dateIconImageView.image = UIImage(named: "clock_icon.png")
        dateIconImageView.contentMode = .scaleAspectFill
        dateView.addSubview(dateIconImageView)
        dateButtonsView.addSubview(dateView)
        
        // add date text
        dateInfoView = UITextView(frame: CGRect(x: totalWidth/6, y: 0, width: (totalWidth/6)*4, height: theViewsHeight))
        dateInfoView.font = UIFont.systemFont(ofSize: 14)
        dateInfoView.isEditable = false
        dateInfoView.isScrollEnabled = false
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let dateString = formatter.string(from: (theEvent?.dateStart)!)
        formatter.dateFormat = "HH:mm"
        let timeString = formatter.string(from: (theEvent?.dateStart)!)
        dateInfoView.text = dateString + "\n" + timeString
        dateButtonsView.addSubview(dateInfoView)
        
        if(isCreator){
            // add change button
            changeTimeButton = UIButton(frame: CGRect(x: (totalWidth/6)*5, y: 0, width: totalWidth/6, height: theViewsHeight))
            changeTimeButton.addTarget(self, action: #selector(changeEventTime), for: .touchUpInside)
            changeTimeButton.setImage(UIImage(named: "pencil_icon.png"), for: .normal)
            changeTimeButton.imageView?.contentMode = .scaleAspectFit
            changeTimeButton.imageEdgeInsets = UIEdgeInsets(top: (theViewsHeight/2)-10, left: 0, bottom: (theViewsHeight/2)-10, right: 0)
            dateButtonsView.addSubview(changeTimeButton)
        }
        
        eventInfoView.addSubview(dateButtonsView)
    }
    
    func makeCostsButtons(eventInfoView : UIView){
        let totalWidth = viewForBackground.frame.width
        
        costButtonsView = UIView(frame: CGRect(x: 0.0, y: 140.0, width: totalWidth, height: 60.0))
        costButtonsView.backgroundColor = UIColor.white
        costButtonsView.layer.borderColor = UIColor.lightGray.cgColor
        costButtonsView.layer.borderWidth = 1
        costButtonsView.isUserInteractionEnabled = true
        
        let theViewsHeight = costButtonsView.frame.height
        
        // add icon
        let costView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: totalWidth/6, height: theViewsHeight))
        costView.backgroundColor = UIColor.white
        let dateIconImageView = UIImageView(frame: CGRect(x: (totalWidth/6-20)/2, y: 20, width: 20, height: 20))
        dateIconImageView.image = UIImage(named: "euro_icon.png")
        dateIconImageView.contentMode = .scaleAspectFill
        costView.addSubview(dateIconImageView)
        costButtonsView.addSubview(costView)
        
        // add cost text
        costInfoView = UITextView(frame: CGRect(x: totalWidth/6, y: 0, width: (totalWidth/6)*4, height: theViewsHeight))
        costInfoView.font = UIFont.systemFont(ofSize: 14)
        costInfoView.isEditable = false
        costInfoView.isScrollEnabled = false
        if(theEvent?.priceBegin != "-1"){
            costInfoView.text = "€ " + (theEvent?.priceBegin)! + " - " + (theEvent?.priceEnd)! + "\nEstimated costs"
        } else{
            costInfoView.text = "€ ?? - ?? \nEstimated costs"
        }
        costButtonsView.addSubview(costInfoView)
        
        if(isCreator){
            // add change button
            changePriceRangeButton = UIButton(frame: CGRect(x: (totalWidth/6)*5, y: 0, width: totalWidth/6, height: theViewsHeight))
            changePriceRangeButton.addTarget(self, action: #selector(changeCosts), for: .touchUpInside)
            changePriceRangeButton.setImage(UIImage(named: "pencil_icon.png"), for: .normal)
            changePriceRangeButton.imageView?.contentMode = .scaleAspectFit
            changePriceRangeButton.imageEdgeInsets = UIEdgeInsets(top: (theViewsHeight/2)-10, left: 0, bottom: (theViewsHeight/2)-10, right: 0)
            costButtonsView.addSubview(changePriceRangeButton)
        }
        
        eventInfoView.addSubview(costButtonsView)
    }
    
    func makeCreatorPart(eventInfoView : UIView){
        let totalWidth = viewForBackground.frame.width
        
        creatorPartView = UIView(frame: CGRect(x: 0.0, y: 200.0, width: totalWidth, height: 60.0))
        creatorPartView.backgroundColor = UIColor.white
        creatorPartView.layer.borderColor = UIColor.lightGray.cgColor
        creatorPartView.layer.borderWidth = 1
        
        let theViewsHeight = creatorPartView.frame.height
        
        // add icon
        let creatorView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: totalWidth/6, height: theViewsHeight))
        creatorView.backgroundColor = UIColor.white
        let creatorImageView = UIImageView(frame: CGRect(x: (totalWidth/6-20)/2, y: 20, width: 20, height: 20))
        creatorImageView.image = UIImage(named: "profile_icon.png")
        creatorImageView.contentMode = .scaleAspectFill
        creatorView.addSubview(creatorImageView)
        creatorPartView.addSubview(creatorView)
        
        // add the creator image view
        showCreatorImageView = UIImageView(frame: CGRect(x: totalWidth/6, y: 10, width: theViewsHeight-20, height: theViewsHeight-20))
        showCreatorImageView.image = UIImage(named: "profile_icon.png")
        
        let contactObject = contactExists(contactId: (theEvent?.creator)!, entityName: "Contact", context: self.context)
        let contactName = contactObject?.value(forKey: "name") as! String
        let contactPicChangedAmount = contactObject?.value(forKey: "picChangedAmount") as! String
        getPicDataForUser(theUserId: (theEvent?.creator)!, picChangedAmount: contactPicChangedAmount, completionHandler: {picData in
            self.showCreatorImageView.image = UIImage(data: picData as Data)
        })
        showCreatorImageView.contentMode = .scaleAspectFill
        showCreatorImageView.makePortrait()
        creatorPartView.addSubview(showCreatorImageView)
        
        // add info text
        creatorInfoView = UITextView(frame: CGRect(x: totalWidth/3, y: 0, width: (totalWidth/6)*4, height: theViewsHeight))
        creatorInfoView.font = UIFont.systemFont(ofSize: 14)
        creatorInfoView.isEditable = false
        creatorInfoView.isScrollEnabled = false
        creatorInfoView.text = "Created by " + contactName
        creatorPartView.addSubview(creatorInfoView)
        
        eventInfoView.addSubview(creatorPartView)
    }
    
    func makeMaxPeoplePart(eventInfoView: UIView){
        let totalWidth = viewForBackground.frame.width
        
        let maxPeoplePartView = UIView(frame: CGRect(x: 0.0, y: 260.0, width: totalWidth, height: 60.0))
        maxPeoplePartView.backgroundColor = UIColor.white
        maxPeoplePartView.layer.borderColor = UIColor.lightGray.cgColor
        maxPeoplePartView.layer.borderWidth = 1
        
        let theViewsHeight = maxPeoplePartView.frame.height
        
        // add icon
        let creatorView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: totalWidth/6, height: theViewsHeight))
        creatorView.backgroundColor = UIColor.white
        let creatorImageView = UIImageView(frame: CGRect(x: (totalWidth/6-30)/2, y: 15, width: 30, height: 30))
        creatorImageView.image = UIImage(named: "group_icon.png")
        creatorImageView.contentMode = .scaleAspectFill
        creatorView.addSubview(creatorImageView)
        maxPeoplePartView.addSubview(creatorView)
        
        // add info text
        let maxPeopleInfoView = UITextView(frame: CGRect(x: totalWidth/3, y: 0, width: (totalWidth/6)*4, height: theViewsHeight))
        maxPeopleInfoView.font = UIFont.systemFont(ofSize: 14)
        maxPeopleInfoView.isEditable = false
        maxPeopleInfoView.isScrollEnabled = false
        maxPeopleInfoView.text = "Max attendees: " + (theEvent?.maxPeople)!
        maxPeoplePartView.addSubview(maxPeopleInfoView)
        
        eventInfoView.addSubview(maxPeoplePartView)
    }
    
    func setEventInfoButtons(){
        if(eventInfoView != nil){
            eventInfoView.removeFromSuperview()
        }
        
        let totalWidth = viewForBackground.frame.width
        let xValue = viewForBackground.frame.origin.x
        let yValue = viewForBackground.frame.origin.y
        let theHeight = CGFloat(320)
        eventInfoView = UIView(frame: CGRect(x: xValue, y: yValue + lastHeight, width: totalWidth, height: theHeight))
        lastHeight = addToLastHeight(lastHeightLocal: lastHeight, viewHeight: theHeight)
        eventInfoView.backgroundColor = UIColor.white
        eventInfoView.addShadow()
        
        theScrollView.addSubview(eventInfoView)
        
        makeLocationButtons(eventInfoView: eventInfoView)
        makeDateButtons(eventInfoView: eventInfoView)
        makeCostsButtons(eventInfoView: eventInfoView)
        makeCreatorPart(eventInfoView: eventInfoView)
        makeMaxPeoplePart(eventInfoView: eventInfoView)
    }
    
    func addRemarksField(){
        if(remarksView != nil){
            remarksView.removeFromSuperview()
        }
        
        let totalWidth = viewForBackground.frame.width
        let xValue = viewForBackground.frame.origin.x
        let yValue = viewForBackground.frame.origin.y
        let theHeight = CGFloat(100)
        remarksView = UIView(frame: CGRect(x: xValue, y: yValue + lastHeight, width: totalWidth, height: theHeight))
        lastHeight = addToLastHeight(lastHeightLocal: lastHeight, viewHeight: theHeight)
        remarksView.backgroundColor = UIColor.white
        remarksView.addShadow()
        
        
        let localLabel = UILabel(frame: CGRect(x: 10, y: 0, width: totalWidth - 10, height: 15))
        localLabel.text = "Remarks"
        localLabel.textColor = UIColor.red
        localLabel.font = UIFont.systemFont(ofSize: 12)
        remarksView.addSubview(localLabel)
        
        let localTextView = UITextView(frame: CGRect(x: 10, y: 15, width: totalWidth - 10, height: 85))
        localTextView.text = theEvent?.remarks
        localTextView.isEditable = false
        remarksView.addSubview(localTextView)
        
        theScrollView.addSubview(remarksView)
    }
    
    func addMoreInviteesButton(){
        if(addMoreAttendeesButton != nil){
            addMoreAttendeesButton.removeFromSuperview()
        }
        
        if(isCreator && !(theEvent?.isGroupEvent)!){
            let totalWidth = viewForBackground.frame.width
            let xValue = viewForBackground.frame.origin.x
            let yValue = viewForBackground.frame.origin.y
            
            addMoreAttendeesButton = UIButton(type: UIButtonType.custom)
            let theHeight = CGFloat(40)
            addMoreAttendeesButton.frame = CGRect(x: xValue, y: yValue + lastHeight, width: totalWidth, height: theHeight)
            lastHeight = addToLastHeight(lastHeightLocal: lastHeight, viewHeight: theHeight)
            addMoreAttendeesButton.backgroundColor = UIColor.white
            addMoreAttendeesButton.addTarget(self, action: #selector(addMoreInvitees), for: .touchUpInside)
            addMoreAttendeesButton.addShadow()
            
            let icon = UIImage(named: "group_icon.png")
            
            addMoreAttendeesButton.setImage(icon, for: .normal)
            addMoreAttendeesButton.imageView?.contentMode = .scaleAspectFit
            addMoreAttendeesButton.sendSubview(toBack: addMoreAttendeesButton.imageView!)
            let frameWidth = addMoreAttendeesButton.frame.width
            addMoreAttendeesButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: frameWidth*0.75)
            
            addMoreAttendeesButton.setTitleColor(UIColor.red, for: .normal)
            addMoreAttendeesButton.setTitle("Invite more people", for: .normal)
            
            let imageWidth = addMoreAttendeesButton.imageView?.frame.width
            addMoreAttendeesButton.titleEdgeInsets = UIEdgeInsets(top: 10, left: imageWidth! * -5.25 , bottom: 10, right: 0)
            
            theScrollView.addSubview(addMoreAttendeesButton)
        }
    }
    
    func makeAttendeesPart(){
        if(attendeesView != nil){
            attendeesView.removeFromSuperview()
        }
        
        let totalWidth = viewForBackground.frame.width
        let xValue = viewForBackground.frame.origin.x
        let yValue = viewForBackground.frame.origin.y
        let height = CGFloat(30)
        attendeesView = UIView(frame: CGRect(x: xValue, y: yValue + lastHeight, width: totalWidth, height: height))
        attendeesView.backgroundColor = UIColor.white
        lastHeight = addToLastHeight(lastHeightLocal: lastHeight, viewHeight: height)
        attendeesView.addShadow()
        
        let localLabel = UILabel(frame: CGRect(x: 10, y: 0, width: totalWidth/2 - 10, height: height))
        localLabel.text = "Attendees"
        localLabel.textColor = UIColor.red
        localLabel.font = UIFont.systemFont(ofSize: 12)
        attendeesView.addSubview(localLabel)
        
        collexpAttendeesButton = UIButton(frame: CGRect(x: totalWidth/2, y: 0, width: totalWidth/2, height: height))
        collexpAttendeesButton.addTarget(self, action: #selector(expandAttendees), for: .touchUpInside)
        collexpAttendeesButton.setImage(UIImage(named: "expand_icon.png"), for: .normal)
        collexpAttendeesButton.imageView?.contentMode = .scaleAspectFit
        collexpAttendeesButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -totalWidth/3)
        attendeesView.addSubview(collexpAttendeesButton)
        
        theScrollView.addSubview(attendeesView)
    }

    func makeNotRespondedPart(){
        if(notRespondedView != nil){
            notRespondedView.removeFromSuperview()
        }
        
        let totalWidth = viewForBackground.frame.width
        let xValue = viewForBackground.frame.origin.x
        let yValue = viewForBackground.frame.origin.y
        let height = CGFloat(30)
        notRespondedView = UIView(frame: CGRect(x: xValue, y: yValue + lastHeight, width: totalWidth, height: height))
        notRespondedView.backgroundColor = UIColor.white
        lastHeight = addToLastHeight(lastHeightLocal: lastHeight, viewHeight: height)
        notRespondedView.addShadow()
        
        let localLabel = UILabel(frame: CGRect(x: 10, y: 0, width: totalWidth/2 - 10, height: height))
        localLabel.text = "Not Responded"
        localLabel.textColor = UIColor.red
        localLabel.font = UIFont.systemFont(ofSize: 12)
        notRespondedView.addSubview(localLabel)
        
        collexpNotRespondedButton = UIButton(frame: CGRect(x: totalWidth/2, y: 0, width: totalWidth/2, height: height))
        collexpNotRespondedButton.addTarget(self, action: #selector(expandNotResponded), for: .touchUpInside)
        collexpNotRespondedButton.setImage(UIImage(named: "expand_icon.png"), for: .normal)
        collexpNotRespondedButton.imageView?.contentMode = .scaleAspectFit
        collexpNotRespondedButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -totalWidth/3)
        notRespondedView.addSubview(collexpNotRespondedButton)
        
        theScrollView.addSubview(notRespondedView)
    }
    
    func makeDeleteEventPart(){
        if(deleteButton != nil){
            deleteButton.removeFromSuperview()
        }
        
        if(isCreator && !(theEvent?.isGroupEvent)!){
            let totalWidth = viewForBackground.frame.width
            let xValue = viewForBackground.frame.origin.x
            let yValue = viewForBackground.frame.origin.y
            
            deleteButton = UIButton(type: UIButtonType.custom)
            let theHeight = CGFloat(40)
            deleteButton.frame = CGRect(x: xValue, y: yValue + lastHeight, width: totalWidth, height: theHeight)
            lastHeight = addToLastHeight(lastHeightLocal: lastHeight, viewHeight: theHeight)
            deleteButton.backgroundColor = UIColor.white
            deleteButton.addTarget(self, action: #selector(deleteTheEvent), for: .touchUpInside)
            deleteButton.addShadow()
            
            let icon = UIImage(named: "delete_icon.png")
            
            deleteButton.setImage(icon, for: .normal)
            deleteButton.imageView?.contentMode = .scaleAspectFit
            deleteButton.sendSubview(toBack: deleteButton.imageView!)
            let frameWidth = deleteButton.frame.width
            deleteButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: frameWidth*0.75)
            
            deleteButton.setTitleColor(UIColor.red, for: .normal)
            deleteButton.setTitle("Delete Event", for: .normal)
            
            let imageWidth = deleteButton.imageView?.frame.width
            deleteButton.titleEdgeInsets = UIEdgeInsets(top: 10, left: imageWidth! * -5.25 , bottom: 10, right: 0)
            
            theScrollView.addSubview(deleteButton)
        }
    }
    // TODO disable if no internet function
    
    /******* VIEWDIDLOAD/ VIEWWILLAPPEAR *******/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        theWidth = Double(view.frame.width)
        theViewHeight = Double(view.frame.height)
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        
        addTopViewColor(colorNavBar: true)
        
        setUpVariables()
        setUpDatabaseReferences()
        setUpNavigationController()
        setUpLocationManager()
        getPeopleInfo()
        setUpParentViewsAndImage()
        getEventPic()
        setTopButtons()
        setEventInfoButtons()
        addRemarksField()
        addMoreInviteesButton()
        makeAttendeesPart()
        makeNotRespondedPart()
        makeDeleteEventPart()
//        disableIfNoInternet()
        
        self.eventPic.frame.origin.y = ((self.navigationController?.navigationBar.frame.height)! + 20)
        self.viewForBackground.frame.origin.y = self.eventPic.frame.origin.y + self.eventPic.frame.height - 64
        
        refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: .valueChanged)
        theScrollView.refreshControl = refreshControl
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.hidesBarsOnSwipe = true
    }
    
    /*** ACTION HELPER FUNCTIONS ***/
    
    func loadSingleEventFromDatabase(eventId : String){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CoreDataEvent")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "eventId") as! String == eventId}) as? NSManagedObject{
                    let creator = result.value(forKey: "creator") as! String
                    let dateMilliseconds = result.value(forKey: "dateStart") as! Int
                    let dateStart = Date(milliseconds: dateMilliseconds)
                    let eventId = result.value(forKey: "eventId") as! String
                    let eventName = result.value(forKey: "eventName") as! String
                    let isGroupEvent = result.value(forKey: "isGroupEvent") as! Bool
                    let latitude = result.value(forKey: "latitude") as! String
                    let locationAddress = result.value(forKey: "locationAddress") as! String
                    let locationId = result.value(forKey: "locationId") as! String
                    let locationName = result.value(forKey: "locationName") as! String
                    let longitude = result.value(forKey: "longitude") as! String
                    let maxPeople = result.value(forKey: "maxPeople") as! String
                    let nrOfPeopleInvited = result.value(forKey: "nrOfPeopleInvited") as! String
                    let picChangedAmount = result.value(forKey: "picChangedAmount") as! String
                    let priceBegin = result.value(forKey: "priceBegin") as! String
                    let priceEnd = result.value(forKey: "priceEnd") as! String
                    let remarks = result.value(forKey: "remarks") as! String
                    let timeLastChanged = result.value(forKey: "timeLastChanged") as! String
                    let timeMade = result.value(forKey: "timeMade") as! String
                    let typeOfEvent = result.value(forKey: "typeOfEvent") as! String
                    
                    let invitedPeopleSet : NSSet = result.value(forKey: "invitedPeople") as! NSSet
                    let joinedPeopleSet : NSSet = result.value(forKey: "joinedPeople") as! NSSet
                    
                    var joinedPeople = [localContactStruct]()
                    for personAny in joinedPeopleSet.allObjects{
                        if let person = personAny as? NSManagedObject{
                            let id = person.value(forKey: "userId") as! String
                            let name = person.value(forKey: "name") as! String
                            let favCuis = person.value(forKey: "favCuis") as! String
                            let favRest = person.value(forKey: "favRest") as! String
                            let gender = person.value(forKey: "gender") as! String
                            let picChangedAmount = person.value(forKey: "picChangedAmount") as! String
                            let timeLastChanged = person.value(forKey: "timeLastChanged") as! String
                            
                            joinedPeople.append(localContactStruct(id: id, name: name, favCuis: favCuis, favRest: favRest, gender: gender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
                        }
                    }
                    
                    var invitedPeople = [localContactStruct]()
                    for personAny in invitedPeopleSet{
                        if let person = personAny as? NSManagedObject{
                            let id = person.value(forKey: "userId") as! String
                            let name = person.value(forKey: "name") as! String
                            let favCuis = person.value(forKey: "favCuis") as! String
                            let favRest = person.value(forKey: "favRest") as! String
                            let gender = person.value(forKey: "gender") as! String
                            let picChangedAmount = person.value(forKey: "picChangedAmount") as! String
                            let timeLastChanged = person.value(forKey: "timeLastChanged") as! String
                            
                            invitedPeople.append(localContactStruct(id: id, name: name, favCuis: favCuis, favRest: favRest, gender: gender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
                        }
                    }
                    
                    self.theEvent = Event(creator: creator, eventId: eventId, isGroupEvent: isGroupEvent, latitude: latitude, locationAddress: locationAddress, locationId: locationId, locationName: locationName, longitude: longitude, maxPeople: maxPeople, dateStart: dateStart, eventName: eventName, picChangedAmount: picChangedAmount, priceBegin: priceBegin, priceEnd: priceEnd, remarks: remarks, typeOfEvent: typeOfEvent, timeMade: timeMade, timeLastChanged: timeLastChanged, nrOfPeopleInvited: nrOfPeopleInvited,joinedPeople: joinedPeople, invitedPeople: invitedPeople)
                }
            }
        } catch{
            print("error")
        }
    }
    
    func ReloadEverything(){
        loadSingleEventFromDatabase(eventId: eventId!)
        setUpVariables()
        setUpDatabaseReferences()
        setUpNavigationController()
        setUpLocationManager()
        getEventPic()
        getPeopleInfo()
        setTopButtons()
        setEventInfoButtons()
        addRemarksField()
        addMoreInviteesButton()
        makeAttendeesPart()
        makeNotRespondedPart()
        makeDeleteEventPart()
//        disableIfNoInternet()
    }
    
    func giveSettingsAlert(path : String, message : String){
        let alert = UIAlertController(title: "", message: "You have not given permission for location services, would you like to go to your settings to change it?" , preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Go to settings", style: .default, handler: { (action) -> Void in
            guard let settingsUrl = URL(string: path) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                })
            }
        })
        alert.addAction(action1)
        let cancel = UIAlertAction(title: "No, I'll just set a location on the map", style: .destructive, handler: { (action) -> Void in })
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    func checkForLocationServices(pickedALocation : Bool){
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                giveSettingsAlert(path: UIApplicationOpenSettingsURLString, message: "You have not given permission for location services, would you like to go to your settings to give permission?")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                presentThePopup(pickedALocation: pickedALocation)
            }
        } else {
            print("Location services are not enabled")
            giveSettingsAlert(path: "App-Prefs:root=Privacy&path=LOCATION", message: "Your location services are turned off, would you like to go to your settings to turn it on?")
        }
        
    }
    
    func presentThePopup(pickedALocation : Bool){
        var urlString = ""
        var localLat = ""
        var localLong = ""
        if(pickedALocation){
            localLat = pickedLatitude!
            localLong = pickedLongitude!
//            urlString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + pickedLatitude!
//            urlString += "," + pickedLongitude! + "&type=" + (theEvent?.typeOfEvent)! + "&rankby=distance&key=AIzaSyCeLVUpx-gIibTYo10URfS1oyKNf72XbmM"
        } else{
            localLat = latitude!
            localLong = longitude!
        }
        
        urlString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + localLat
        urlString += "," + localLong + "&type=" + (theEvent?.typeOfEvent)! + "&rankby=distance&key=AIzaSyCeLVUpx-gIibTYo10URfS1oyKNf72XbmM"
        
        let url = URL(string: urlString)
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error != nil{
                print("restaurantsListError")
                print(error ?? "error")
            }
            else{
                if let content = data {
                    do {
                        self.allRestaurantOptions.removeAll()
                        let restInfoList = try JSONDecoder().decode(restaurantInfoListStruct.self, from: content)
                        for object in restInfoList.results{
                            self.allRestaurantOptions.append(RestaurantItem(
                                name: object.name ?? "no name",
                                placeId: object.place_id ?? "no place id",
                                rating:  NSString(format: "%.1f", object.rating ?? -1.0) as String,
                                vicinity: object.vicinity ?? "no vicinity",
                                photoReference: object.photos?[0].photo_reference ?? "no photos",
                                lat: NSString(format: "%.13f", object.geometry?.location?.lat ?? 0.0) as String,
                                lng: NSString(format: "%.13f", object.geometry?.location?.lng ?? 0.0) as String,
                                personLat: localLat,
                                personLng: localLong,
                                icon: object.icon ?? "no icon",
                                priceLevel: object.price_level ?? -1
                            ))
                        }
                        DispatchQueue.main.async {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "restaurantsPopUp") as! RestaurantsPopUpViewController
                            vc.restaurantsList = self.allRestaurantOptions
                            vc.restProtocol = self
                            self.present(vc, animated: true, completion: nil)
                        }
                    } catch let jsonErr{
                        print("Error json: ", jsonErr)
                    }
                }
            }
        }
        task.resume()
    }
    
    func presentPopupForLocations(pickedALocation : Bool){
        if(pickedALocation){
            presentThePopup(pickedALocation: pickedALocation)
        } else {
            self.checkForLocationServices(pickedALocation: pickedALocation)
        }
    }
    
    func deleteEvent(lastInGroupToLeave : Bool){
        var message = ""
        if(lastInGroupToLeave){
            message = "There will be nobody left in this event if you leave, leaving will delete the event. Continue?"
        } else{
            message = "Really delete event \"" + (theEvent?.eventName)! + "\"? This cannot be reversed."
        }
        
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            print("removed observer on: " + self.eventId!)
            self.Db?.child("Events").child(self.eventId!).child("timeLastChanged").removeAllObservers()
            let myGroup = DispatchGroup()
            
            myGroup.enter()
            self.Db?.child("Events").child(self.eventId!).child("eventInvitees").observeSingleEvent(of: .value, with: { (snapshot) in
                let enumerator = snapshot.children
                print("in eventinvitees")
                print(snapshot)
                while let localUserId = enumerator.nextObject() as? DataSnapshot{
                    myGroup.enter()
                    self.Db?.child("publicProfiles").child(self.getString(object: localUserId.value)).child("events").child(self.eventId!).removeValue(){ (error, snapshot) in
                        print("removed event from user: " + self.getString(object: localUserId.value))
                        myGroup.leave()
                    }
                }
                myGroup.leave()
                
                // only delete after you have the eventInvitees
                myGroup.enter()
                self.eventRef?.removeValue(){ (error, snapshot) in
                    myGroup.leave()
                }
            })
            
            
            
            self.eventPicRef?.delete { error in
                if error != nil {
                    print("pic not deleted from database")
                } else {
                    print("pic deleted from database")
                }
            }
//            myGroup.enter()
//            self.eventInviteesReference?.child(self.eventId!).removeValue(){ (error, snapshot) in
//                myGroup.leave()
//            }
            
            myGroup.enter()
            self.Db?.child("eventChats").child(self.eventId!).removeValue(){ (error, snapshot) in
                myGroup.leave()
            }
            
            myGroup.enter()
            self.usersReference?.child(self.userId!).child("myEvents").child(self.eventId!).removeValue(){ (error, snapshot) in
                myGroup.leave()
            }
            
            myGroup.notify(queue: .main){
//                self.reloadProtocol.reloadEverything()
                self.goBack()
            }
        })
        let cancel = UIAlertAction(title: "No", style: .destructive, handler: { (action) -> Void in })
        
        alert.addAction(action1)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    /*** ACTIONS ***/
    
    @objc func refresh(sender:AnyObject) {
        ReloadEverything()
        self.refreshControl.endRefreshing()
    }
    
    @objc func openChat(){
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ChatID") as! ChatViewController
        nextViewController.eventId = eventId
        nextViewController.userId = userId
        nextViewController.eventName = (theEvent?.eventName)!
        self.navigationController?.pushViewController(nextViewController, animated: true)
    }
    
    func leaveLocally(){
        let theUserIndex = self.theEvent?.joinedPeople.index(where: {$0.id == self.userId})
        
        self.theEvent?.invitedPeople.append((self.theEvent?.joinedPeople[theUserIndex!])!)
        self.theEvent?.joinedPeople.remove(at: theUserIndex!)
        
        self.invitedFriends.append(self.attendingFriends[self.attendingFriends.index(where: {$0.id == self.userId})!])
        self.attendingFriends.remove(at: self.attendingFriends.index(where: {$0.id == self.userId})!)
//        self.attendingList.reloadData()
//        self.notRespondedList.reloadData()
    }
    
    func joinLocally(){
        let theUserIndex = self.theEvent?.invitedPeople.index(where: {$0.id == self.userId})
        
        self.theEvent?.joinedPeople.append((self.theEvent?.invitedPeople[theUserIndex!])!)
        self.theEvent?.invitedPeople.remove(at: theUserIndex!)
        
        self.attendingFriends.append(self.invitedFriends[self.invitedFriends.index(where: {$0.id == self.userId})!])
        self.invitedFriends.remove(at: self.invitedFriends.index(where: {$0.id == self.userId})!)
//        self.attendingList.reloadData()
//        self.notRespondedList.reloadData()
    }
    
    @objc func leaveEvent(){
        print("leaving event")
        spinnerView = UIViewController.displaySpinner(onView: self.view)
        let joinedPeopleRef = Db?.child("Events").child(eventId!).child("joinedPeople")
        joinedPeopleRef?.observeSingleEvent(of: .value, with: { (snapshot) in
            if var dict = snapshot.value as? [String : Any]{
                print(dict.count)
                
                if(dict.count > 1){
                    if(self.isCreator && (self.theEvent?.isGroupEvent)!){ // leaving as creator of a group event
                        print("leaving event as creator")
                        dict.removeValue(forKey: self.userId!)
                        let newAdmin = self.getString(object: Array(dict.values)[0])
                        
                        joinedPeopleRef?.child(self.userId!).removeValue(){ (error, snapshot) in
                            self.eventRef?.child("creator").setValue(newAdmin){ (error, snapshot) in
                                self.usersReference?.child(self.userId!).child("myEvents").child(self.eventId!).removeValue()
                                let millisecondsNow = String(Date().millisecondsSince1970)
                                self.Db?.child("Events").child(self.eventId!).child("timeLastChanged").setValue(millisecondsNow){ (error, snapshot) in
                                    self.leaveLocally()
                                    self.makeAttendDeclineButton(theView: self.topButtonsView)
                                    if(self.spinnerView != nil){
                                        UIViewController.removeSpinner(spinner: self.spinnerView!)
                                    }
                                }
                            }
                        }
                    } else { // leaving as attendee
                        print("leaving as attendee")
                        joinedPeopleRef?.child(self.userId!).removeValue(){ (error, snapshot) in
                            self.usersReference?.child(self.userId!).child("joinedEvents").child(self.eventId!).removeValue()
                            let millisecondsNow = String(Date().millisecondsSince1970)
                            self.Db?.child("Events").child(self.eventId!).child("timeLastChanged").setValue(millisecondsNow){ (error, snapshot) in
                                if(error == nil){
                                    self.leaveLocally()
                                    self.makeAttendDeclineButton(theView: self.topButtonsView)
                                    if(self.spinnerView != nil){
                                        UIViewController.removeSpinner(spinner: self.spinnerView!)
                                    }
                                }
                            }
                        }
                    }
                    
                } else {
                    self.deleteEvent(lastInGroupToLeave: true)
                    if(self.spinnerView != nil){
                        UIViewController.removeSpinner(spinner: self.spinnerView!)
                    }
                }
                
            }
        })
    }
    
    @objc func attendEvent(){
        print("joining event")
        spinnerView = UIViewController.displaySpinner(onView: self.view)
        let joinedPeopleRef = Db?.child("Events").child(eventId!).child("joinedPeople")
        joinedPeopleRef?.observeSingleEvent(of: .value, with: { (snapshot) in
            if let dict = snapshot.value as? [String : Any]{
                if(dict.count < Int((self.theEvent?.maxPeople)!)!){ // you can join
                    
                    joinedPeopleRef?.child(self.userId!).setValue(self.userId){ (error, snapshot) in
                        self.usersReference?.child(self.userId!).child("joinedEvents").child(self.eventId!).setValue(self.eventId){ (error, snapshot) in
                            let millisecondsNow = String(Date().millisecondsSince1970)
                            self.Db?.child("Events").child(self.eventId!).child("timeLastChanged").setValue(millisecondsNow){(error, snapshot) in
                                self.joinLocally()
                                self.makeChatButton(theView: self.topButtonsView)
                                self.makeLeaveButton(theView: self.topButtonsView)
                                if(self.spinnerView != nil){
                                    UIViewController.removeSpinner(spinner: self.spinnerView!)
                                }
                            }
                        }
                    }
                    
                    self.joinLocally()
                    
                    self.ReloadEverything()
                    
                } else{
                    if(self.spinnerView != nil){
                        UIViewController.removeSpinner(spinner: self.spinnerView!)
                    }
                }
            }
        })
    }
    
    @objc func declineEvent(){
        spinnerView = UIViewController.displaySpinner(onView: self.view)
        self.Db?.child("publicProfiles").child(self.userId!).child("events").child(self.eventId!).removeValue(){(error, snapshot) in
            self.eventRef?.child("eventInvitees").child(self.userId!).removeValue(){ (error, snapshot) in
                let millisecondsNow = String(Date().millisecondsSince1970)
                self.Db?.child("Events").child(self.eventId!).child("timeLastChanged").setValue(millisecondsNow){ (error, snapshot) in
                    self.reloadProtocol.reloadEverything()
                    if(self.spinnerView != nil){
                        UIViewController.removeSpinner(spinner: self.spinnerView!)
                    }
                    self.goBack()
                }
            }
        }
    }
    
    @objc func getRoute(){
        self.latitude = theEvent?.latitude
        self.longitude = theEvent?.longitude
        
        var directionsURL = ""
        let alert = UIAlertController(title: "mode of transportation", message: "What way will you be travelling?", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Driving", style: .default, handler: { (action) -> Void in
            directionsURL = "https://www.google.com/maps/?daddr=" + self.latitude! + "," + self.longitude! + "&dirflg=d"
            self.getDirections(directionsURL: directionsURL)
        })
        let action2 = UIAlertAction(title: "Walking", style: .default, handler: { (action) -> Void in
            directionsURL = "https://www.google.com/maps/?daddr=" + self.latitude! + "," + self.longitude! + "&dirflg=w"
            self.getDirections(directionsURL: directionsURL)
        })
        let action3 = UIAlertAction(title: "Bicycling", style: .default, handler: { (action) -> Void in
            directionsURL = "https://www.google.com/maps/?daddr=" + self.latitude! + "," + self.longitude! + "&dirflg=b"
            self.getDirections(directionsURL: directionsURL)
        })
        let action4 = UIAlertAction(title: "Public transport", style: .default, handler: { (action) -> Void in
            directionsURL = "https://www.google.com/maps/?daddr=" + self.latitude! + "," + self.longitude! + "&dirflg=r"
            self.getDirections(directionsURL: directionsURL)
        })
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        alert.addAction(action4)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func changeEventLocation(){
        let alert = UIAlertController(title: "mode of transportation", message: "Set a location or use your location?", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Set a location", style: .default, handler: { (action) -> Void in
            let config = GMSPlacePickerConfig(viewport: nil)
            let placePicker = GMSPlacePickerViewController(config: config)
            placePicker.delegate = self
            print("ayy3")
            self.present(placePicker, animated: true, completion: nil)
        })
        let action2 = UIAlertAction(title: "Use my location", style: .default, handler: { (action) -> Void in
            self.presentPopupForLocations(pickedALocation: false)
        })
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func changeEventTime(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "changeTimeID") as! ChangeTimeViewController
        vc.theEvent = theEvent
        vc.timeProtocol = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func changeCosts(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "changePriceRangeID") as! changePriceRangeViewController
        vc.theEvent = theEvent
        vc.priceRangeProtocol = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func addMoreInvitees(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "AddMorePeopleID") as! AddMorePeopleViewController
        vc.alreadyInvited = invitedFriends
        vc.eventId = eventId
        vc.addMorePeopleProtocol = self
        self.present(vc, animated: true, completion: nil)
    }
    
    @objc func expandAttendees(){
        var imageName = ""
        var valueChanged : CGFloat = 0
        let nrOfAttending = attendingFriends.count
        if (attendeesExpanded){
            attendeesExpanded = false
            imageName = "expand_icon.png"
            valueChanged = CGFloat(nrOfAttending * 50 * -1)
            
            for view in attendeesViews{
                view.removeFromSuperview()
            }
            
        } else {
            attendeesExpanded = true
            imageName = "collapse_icon.png"
            valueChanged = CGFloat(nrOfAttending * 50)
            
            var counter = 0;
            for friend in attendingFriends {
                let localFriendView = UIView(frame: CGRect(x: 0, y: Double(30 + counter * 50), width: theWidth, height: 50))
                attendeesView.addSubview(localFriendView)
                
                let friendImageView = UIImageView(frame: CGRect(x: 5, y: 5, width: 40, height: 40))
                friendImageView.image = UIImage(named: "profile_icon.png")
                
                let contactObject = contactExists(contactId: friend.id!, entityName: "Contact", context: self.context)
                let contactName = contactObject?.value(forKey: "name") as! String
                let contactFavCuis = contactObject?.value(forKey: "favCuis") as! String
                let contactPicChangedAmount = contactObject?.value(forKey: "picChangedAmount") as! String
                getPicDataForUser(theUserId: friend.id!, picChangedAmount: contactPicChangedAmount, completionHandler: {picData in
                    friendImageView.image = UIImage(data: picData as Data)
                })
                
                friendImageView.contentMode = .scaleAspectFill
                friendImageView.makePortrait()
                
                let nameLabel = UILabel(frame: CGRect(x: 65, y: 5, width: theWidth - 40, height: 25))
                nameLabel.font = UIFont.systemFont(ofSize: 16)
                nameLabel.text = contactName
                
                let favCuisLabel = UILabel(frame: CGRect(x: 65, y: 25, width: theWidth - 40, height: 25))
                favCuisLabel.font = UIFont.systemFont(ofSize: 12)
                favCuisLabel.text = "Pref. cuisine: " + contactFavCuis
                
                localFriendView.addSubview(friendImageView)
                localFriendView.addSubview(nameLabel)
                localFriendView.addSubview(favCuisLabel)
                attendeesView.addSubview(localFriendView)
                attendeesViews.append(localFriendView)
                
                let tappy = MyTapGesture(target: self, action: #selector(self.tappedAttendees))
                localFriendView.addGestureRecognizer(tappy)
                tappy.userId = friend.id!
                
                counter += 1
            }
        }
        
        
        collexpAttendeesButton.setImage(UIImage(named: imageName), for: .normal)
        attendeesView.frame = CGRect(x: attendeesView.frame.origin.x, y: attendeesView.frame.origin.y, width: CGFloat(theWidth), height: attendeesView.frame.height + valueChanged)
        viewForBackground.frame = CGRect(x: viewForBackground.frame.origin.x, y: viewForBackground.frame.origin.y, width: viewForBackground.frame.width, height: viewForBackground.frame.height + valueChanged)
        theScrollHeight = theScrollHeight + Double(valueChanged)
        theScrollView.contentSize = CGSize(width: theWidth, height: theScrollHeight)
        notRespondedView.frame = CGRect(x: notRespondedView.frame.origin.x, y: notRespondedView.frame.origin.y + valueChanged, width: CGFloat(theWidth), height: notRespondedView.frame.height)
        if(deleteButton != nil){
            deleteButton.frame = CGRect(x: deleteButton.frame.origin.x, y: deleteButton.frame.origin.y + valueChanged, width: deleteButton.frame.width, height: deleteButton.frame.height)
        }
    }
    
    @objc func tappedAttendees(sender : MyTapGesture){
        let person = attendingFriends.first(where: {$0.id == sender.userId})!
        let alert = UIAlertController(title: "", message: "view " + person.name! + "'s profile?" , preferredStyle: .alert)
        
        let action2 = UIAlertAction(title: "View Profile", style: .default, handler: { (action) -> Void in
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileOtherID") as! OpenOtherProfileViewController
            nextViewController.theContact = person
            let navController = UINavigationController(rootViewController: nextViewController)
            self.present(navController, animated: true, completion: nil)
        })
        alert.addAction(action2)
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        
        
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    @objc func expandNotResponded(){
        var imageName = ""
        var valueChanged : CGFloat = 0
        let nrNotResponded = notRepondedFriends.count
        if (notRespondedExpanded){
            notRespondedExpanded = false
            imageName = "expand_icon.png"
            valueChanged = CGFloat(nrNotResponded * 50 * -1)
            
            for view in notRespondedViews{
                view.removeFromSuperview()
            }
            
        } else {
            notRespondedExpanded = true
            imageName = "collapse_icon.png"
            valueChanged = CGFloat(nrNotResponded * 50)
            
            var counter = 0
            for friend in notRepondedFriends {
                let localFriendView = UIView(frame: CGRect(x: 0, y: Double(30 + counter * 50), width: theWidth, height: 50))
                notRespondedView.addSubview(localFriendView)
                
                let friendImageView = UIImageView(frame: CGRect(x: 5, y: 5, width: 40, height: 40))
                friendImageView.image = UIImage(named: "profile_icon.png")
                
                let contactObject = contactExists(contactId: friend.id!, entityName: "Contact", context: self.context)
                let contactName = contactObject?.value(forKey: "name") as! String
                let contactFavCuis = contactObject?.value(forKey: "favCuis") as! String
                let contactPicChangedAmount = contactObject?.value(forKey: "picChangedAmount") as! String
                getPicDataForUser(theUserId: friend.id!, picChangedAmount: contactPicChangedAmount, completionHandler: {picData in
                    friendImageView.image = UIImage(data: picData as Data)
                })
                
                friendImageView.contentMode = .scaleAspectFill
                friendImageView.makePortrait()
                
                let nameLabel = UILabel(frame: CGRect(x: 65, y: 5, width: theWidth - 40, height: 25))
                nameLabel.font = UIFont.systemFont(ofSize: 16)
                nameLabel.text = contactName
                
                let favCuisLabel = UILabel(frame: CGRect(x: 65, y: 25, width: theWidth - 40, height: 25))
                favCuisLabel.font = UIFont.systemFont(ofSize: 12)
                favCuisLabel.text = "Pref. cuisine: " + contactFavCuis
                
                localFriendView.addSubview(friendImageView)
                localFriendView.addSubview(nameLabel)
                localFriendView.addSubview(favCuisLabel)
                notRespondedView.addSubview(localFriendView)
                notRespondedViews.append(localFriendView)
                
                let tappy = MyTapGesture(target: self, action: #selector(self.tappedInvitees))
                localFriendView.addGestureRecognizer(tappy)
                tappy.userId = friend.id!
                
                counter += 1
            }
        }
        
        collexpNotRespondedButton.setImage(UIImage(named: imageName), for: .normal)
        notRespondedView.frame = CGRect(x: notRespondedView.frame.origin.x, y: notRespondedView.frame.origin.y, width: CGFloat(theWidth), height: notRespondedView.frame.height + valueChanged)
        viewForBackground.frame = CGRect(x: viewForBackground.frame.origin.x, y: viewForBackground.frame.origin.y, width: viewForBackground.frame.width, height: viewForBackground.frame.height + valueChanged)
        theScrollHeight = theScrollHeight + Double(valueChanged)
        theScrollView.contentSize = CGSize(width: theWidth, height: theScrollHeight)
        if(deleteButton != nil){
            deleteButton.frame = CGRect(x: deleteButton.frame.origin.x, y: deleteButton.frame.origin.y + valueChanged, width: deleteButton.frame.width, height: deleteButton.frame.height)
        }
    }
    
    @objc func tappedInvitees(sender : MyTapGesture){
        let person = invitedFriends.first(where: {$0.id == sender.userId})!
        var alert = UIAlertController(title: "", message: "view " + person.name! + "'s profile?" , preferredStyle: .alert)
        
        if(isCreator && !(theEvent?.isGroupEvent)! && Reachability.isConnectedToNetwork()){ // let them delete people
            alert = UIAlertController(title: "", message: "uninvite " + person.name! + ", or view profile?" , preferredStyle: .alert)
            let action1 = UIAlertAction(title: "Uninvite", style: .default, handler: { (action) -> Void in
                
                self.Db?.child("publicProfiles").child(person.id!).child("events").child(self.eventId!).removeValue(){(error, snapshot) in
                    self.eventRef?.child("eventInvitees").child(person.id!).removeValue(){ (error, snapshot) in
                        
                        self.expandNotResponded()
                        
                        if let i = self.invitedFriends.index(where: {$0.id == person.id!}){
                            self.invitedFriends.remove(at: i)
                        }
                        if let i = self.notRepondedFriends.index(where: {$0.id == person.id!}){
                            self.notRepondedFriends.remove(at: i)
                        }
                        
                        let millisecondsNow = String(Date().millisecondsSince1970)
                        self.Db?.child("Events").child(self.eventId!).child("timeLastChanged").setValue(millisecondsNow)
                    }
                }
            })
            alert.addAction(action1)
        }
        
        let action2 = UIAlertAction(title: "View Profile", style: .default, handler: { (action) -> Void in
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileOtherID") as! OpenOtherProfileViewController
            nextViewController.theContact = person
            let navController = UINavigationController(rootViewController: nextViewController)
            self.present(navController, animated: true, completion: nil)
        })
        alert.addAction(action2)
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        
        
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)

    }
    
    @objc func deleteTheEvent(){
        deleteEvent(lastInGroupToLeave: false)
    }
    
    /*** PROTOCOLS ***/
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        latitude = String(format:"%f", location.coordinate.latitude)
        longitude = String(format:"%f", location.coordinate.longitude)
    }
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        viewController.dismiss(animated: true, completion: nil)
        pickedLatitude = NSString(format: "%.13f", place.coordinate.latitude) as String
        pickedLongitude = NSString(format: "%.13f", place.coordinate.longitude) as String
        presentPopupForLocations(pickedALocation: true)
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func dismissPriceRangePicker() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
        if(newMin != -1){
            // write to database
            let minString = "\(newMin)"
            let maxString = "\(newMax)"
            self.eventRef?.child("priceBegin").setValue(minString)
            self.eventRef?.child("priceEnd").setValue(maxString)
            let millisecondsNow = String(Date().millisecondsSince1970)
            self.Db?.child("Events").child(eventId!).child("timeLastChanged").setValue(millisecondsNow)
            
            // write to main memory
            theEvent?.priceBegin = minString
            theEvent?.priceEnd = maxString
            
            // write to display
            costInfoView.text = "€ " + (theEvent?.priceBegin)! + " - " + (theEvent?.priceEnd)!
        }
    }
    
    func dismissMoreInvites() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
        if(newInvites.count > 0){
            
            if(attendeesExpanded){
                expandAttendees()
            }
            if(notRespondedExpanded){
                expandNotResponded()
            }
            
            for invitee in newInvites{
                notRepondedFriends.append(invitee)
                invitedFriends.append(invitee)
            }
            
            let millisecondsNow = String(Date().millisecondsSince1970)
            self.Db?.child("Events").child(eventId!).child("timeLastChanged").setValue(millisecondsNow)
            
            
//            self.notRespondedList.reloadData()
        }
    }
    
    func dismissDatePicker() {
        self.presentedViewController?.dismiss(animated: true, completion: nil)
        if(newDate != theEvent?.dateStart){
            
            // write to database
            let miliseconds = "\(newDate.millisecondsSince1970)"
            self.eventRef?.child("miliseconds").setValue(miliseconds)
            
            let millisecondsNow = String(Date().millisecondsSince1970)
            self.Db?.child("Events").child(eventId!).child("timeLastChanged").setValue(millisecondsNow)
            
            // write to main memory
            theEvent?.dateStart = newDate
            
            // write to display
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            let dateString = formatter.string(from: (theEvent?.dateStart)!)
            formatter.dateFormat = "HH:mm"
            let timeString = formatter.string(from: (theEvent?.dateStart)!)
            dateInfoView.text = dateString + "\n" + timeString
        }
    }
    
    func dismissPresentedView() {
        if(indexOfPickedRest != -1){
            let thePickedRest = allRestaurantOptions[indexOfPickedRest]
            
            var ratingString = thePickedRest.rating
            if(ratingString == "-1.0"){
                ratingString = "no rating"
            }
            
            let aDispatchGroup = DispatchGroup()
            aDispatchGroup.enter()
            let session = URLSession(configuration: .default)
            if(thePickedRest.photoReference != "no photos"){
                let urlString = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + thePickedRest.photoReference + "&key=AIzaSyCeLVUpx-gIibTYo10URfS1oyKNf72XbmM";
                let restImageFromUrl = session.dataTask(with: URL(string: urlString)!){ (data, response, error) in
                    if let e = error{
                        print("an error occured: \(e)")
                    } else{
                        if(response as? HTTPURLResponse) != nil{
                            if let imageData = data{
                                self.theImage = UIImage(data: imageData)?.resizedto150KB()
                            } else{
                                self.theImage = UIImage(named: "noImageAvailable.png")
                                print("no image found")
                            }
                        } else{
                            self.theImage = UIImage(named: "noImageAvailable.png")
                            print("no server response")
                        }
                    }
                    aDispatchGroup.leave()
                }
                restImageFromUrl.resume()
            }else{
                self.theImage = UIImage(named: "noImageAvailable.png")
            }
            
            self.eventRef?.child("locationAddress").setValue(thePickedRest.vicinity)
            self.eventRef?.child("locationName").setValue(thePickedRest.name)
            self.eventRef?.child("locationId").setValue(thePickedRest.placeId)
            self.eventRef?.child("latitude").setValue(thePickedRest.lat)
            self.eventRef?.child("longitude").setValue(thePickedRest.lng)
            
            // write to main memory
            theEvent?.locationAddress = thePickedRest.vicinity
            theEvent?.locationName = thePickedRest.name
            
            // write to display
            showLocationInfoView.text = (theEvent?.locationName)! + " \n" + "stars strimg" + "\nopen now?" + "\ndistance in km"
            aDispatchGroup.wait()
            
            // set new picture
            DispatchQueue.main.async {
                self.showLocationImageView.image = self.theImage!
                self.eventPic.image = self.theImage!
            }
            
            let data = UIImagePNGRepresentation(self.theImage!)
            let uploadTask = self.eventPicRef?.putData(data!, metadata: nil) { (metadata, error) in
                
                self.Db?.child("Events").child(self.eventId!).child("picChangedAmount").observeSingleEvent(of: .value, with: { (snapshot) in
                    var changedString = ""
                    if let a = snapshot.value as? String{
                        changedString = a
                    } else{
                        changedString = "0"
                    }
                    
                    var newChangedAmount : Int = Int(changedString)!
                    newChangedAmount += 1
                    let newChangedAmountString = String(newChangedAmount)
                    print(newChangedAmountString)
                    self.Db?.child("Events").child(self.eventId!).child("picChangedAmount").setValue(newChangedAmountString) { (snapshot, error) in
                        let millisecondsNow = String(Date().millisecondsSince1970)
                        self.Db?.child("Events").child(self.eventId!).child("timeLastChanged").setValue(millisecondsNow)
                        self.presentedViewController?.dismiss(animated: true, completion: nil)
                    }
                    
                })
                guard metadata != nil else {
                    return
                }
            }
            
            _ = uploadTask?.observe(.progress) { snapshot in
                print(snapshot)
            }
        } else {
            self.presentedViewController?.dismiss(animated: true, completion: nil)
        }
    }
    
    func getDirections(directionsURL : String){
        guard let url = URL(string: directionsURL) else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
}
