//
//  changePriceRangeViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 15/04/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import RangeSeekSlider

class changePriceRangeViewController: UIViewController {
    var theEvent : Event!
    var priceRangeProtocol : pickedPriceRangeProtocol!
    
    @IBOutlet weak var priceRangeSlider: RangeSeekSlider!
    
    func setUpOutlets(){
        let priceMin = (theEvent.priceEnd as NSString).floatValue
        if(priceMin != -1.0){
            priceRangeSlider.selectedMinValue = CGFloat((theEvent.priceBegin as NSString).floatValue)
            priceRangeSlider.selectedMaxValue = CGFloat((theEvent.priceEnd as NSString).floatValue)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpOutlets()
    }
    
    @IBAction func cancel(_ sender: Any) {
        priceRangeProtocol.dismissPriceRangePicker()
    }
    
    @IBAction func changePriceRange(_ sender: UIButton) {
        priceRangeProtocol.newMin = Int(priceRangeSlider.selectedMinValue)
        priceRangeProtocol.newMax = Int(priceRangeSlider.selectedMaxValue)
        priceRangeProtocol.dismissPriceRangePicker()
    }
    
    
}
