//
//  RestTableViewCell.swift
//  eatr
//
//  Created by Yorick Bolster on 09/03/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit

class RestTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var ratingLbl: UILabel!
    @IBOutlet weak var distanceLbl: UILabel!
    @IBOutlet weak var restImage: UIImageView!
    @IBOutlet weak var priceLevel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
