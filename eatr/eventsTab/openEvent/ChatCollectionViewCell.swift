//
//  ChatCollectionViewCell.swift
//  eatr
//
//  Created by Yorick Bolster on 06/04/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit

class ChatCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var messageText: UITextView!
    @IBOutlet weak var chatCellView: UIView!
}
