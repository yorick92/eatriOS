//
//  changeDateViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 15/04/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit

class ChangeTimeViewController: UIViewController {
    var theEvent : Event!
    var timeProtocol : pickedTimeProtocol?
    
    @IBOutlet weak var timePickerOutlet: UIDatePicker!
    
    func setUpOutlets(){
        timePickerOutlet.setDate(theEvent.dateStart, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpOutlets()
    }
    
    @IBAction func changeTime(_ sender: Any) {
        timeProtocol?.newDate = timePickerOutlet.date
        timeProtocol?.dismissDatePicker()
    }
    
    @IBAction func cancel(_ sender: UIButton) {
        timeProtocol?.dismissDatePicker()
    }
}
