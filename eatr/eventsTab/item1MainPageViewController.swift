//
//  item1MainPageViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 04/02/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
///Users/yorickbolster/Desktop/eatr/Event.swift

import UIKit
import FBSDKLoginKit
import FirebaseDatabase
import Firebase
import Floaty
import CoreData
import CoreImage

class item1MainPageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, reloadAllDataProtocol{
    var events = [Event]()
    var Db : DatabaseReference?
    var databaseHandle:DatabaseHandle?
    var invitedToEventIds = [String]()
    var myContactIds = [String]()
    var myGroupIds = [String]()
    var myPresetIds = [String]()
    var userId : String?
    let myGroup = DispatchGroup()
    let myEventsDispatchGroup = DispatchGroup()
    let myContactsDispatchGroup = DispatchGroup()
    var context : NSManagedObjectContext!
    var eventsCounter = 0
    var myContactsCounter = 0
    var refreshControl = UIRefreshControl()
    var lastRefresh = 0
    var allObserveRefs = [DatabaseReference]()
    var allGroupObserveRefs = [DatabaseReference]()
    var allEventObserveRefs = [DatabaseReference]()
    var spinnerView : UIView?
    var floaty : Floaty!
    
    var theLastScrollPosition = CGFloat(0)
    
    var theWidth : CGFloat!
    var sortingView : UIView!
    var sortingButton : UIButton!
    var sortingViewText : UILabel!
    var sortingViews = [UIView]()
    var sortingExpaned = false
    
    @IBOutlet weak var tableView: UITableView!
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let theOffsetY = scrollView.contentOffset.y
        if(theLastScrollPosition < theOffsetY){
            if(theOffsetY > 0){
                floaty.fadeOut()
            }
        } else{
            if(theOffsetY < scrollView.contentSize.height - scrollView.frame.height){
                floaty.fadeIn()
            }
        }
        theLastScrollPosition = theOffsetY
    }
    
    func getString(object : Any?) -> String{
        if let a = object as? String{
            return a
        } else{
            return ""
        }
    }
    
    func getEventsFromStorage(){
        self.events = [Event]()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CoreDataEvent")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                for result in results as! [NSManagedObject]{
                    let creator = result.value(forKey: "creator") as! String
                    let dateMilliseconds = result.value(forKey: "dateStart") as! Int
                    let dateStart = Date(milliseconds: dateMilliseconds)
                    let eventId = result.value(forKey: "eventId") as! String
                    let eventName = result.value(forKey: "eventName") as! String
                    let isGroupEvent = result.value(forKey: "isGroupEvent") as! Bool
                    let latitude = result.value(forKey: "latitude") as! String
                    let locationAddress = result.value(forKey: "locationAddress") as! String
                    let locationId = result.value(forKey: "locationId") as! String
                    let locationName = result.value(forKey: "locationName") as! String
                    let longitude = result.value(forKey: "longitude") as! String
                    let maxPeople = result.value(forKey: "maxPeople") as! String
                    let nrOfPeopleInvited = result.value(forKey: "nrOfPeopleInvited") as! String
                    let picChangedAmount = result.value(forKey: "picChangedAmount") as! String
                    let priceBegin = result.value(forKey: "priceBegin") as! String
                    let priceEnd = result.value(forKey: "priceEnd") as! String
                    let remarks = result.value(forKey: "remarks") as! String
                    let timeLastChanged = result.value(forKey: "timeLastChanged") as! String
                    let timeMade = result.value(forKey: "timeMade") as! String
                    let typeOfEvent = result.value(forKey: "typeOfEvent") as! String
                    
                    let invitedPeopleSet : NSSet = result.value(forKey: "invitedPeople") as! NSSet
                    let joinedPeopleSet : NSSet = result.value(forKey: "joinedPeople") as! NSSet
                    
                    var joinedPeople = [localContactStruct]()
                    for personAny in joinedPeopleSet{
                        if let person = personAny as? NSManagedObject{
                            let id = person.value(forKey: "userId") as! String
                            let name = person.value(forKey: "name") as! String
                            let favCuis = person.value(forKey: "favCuis") as! String
                            let favRest = person.value(forKey: "favRest") as! String
                            let gender = person.value(forKey: "gender") as! String
                            let picChangedAmount = person.value(forKey: "picChangedAmount") as! String
                            let timeLastChanged = person.value(forKey: "timeLastChanged") as! String
                            
                            joinedPeople.append(localContactStruct(id: id, name: name, favCuis: favCuis, favRest: favRest, gender: gender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
                        }
                    }
                    
                    var invitedPeople = [localContactStruct]()
                    for personAny in invitedPeopleSet{
                        if let person = personAny as? NSManagedObject{
                            let id = person.value(forKey: "userId") as! String
                            let name = person.value(forKey: "name") as! String
                            let favCuis = person.value(forKey: "favCuis") as! String
                            let favRest = person.value(forKey: "favRest") as! String
                            let gender = person.value(forKey: "gender") as! String
                            let picChangedAmount = person.value(forKey: "picChangedAmount") as! String
                            let timeLastChanged = person.value(forKey: "timeLastChanged") as! String
                            
                            invitedPeople.append(localContactStruct(id: id, name: name, favCuis: favCuis, favRest: favRest, gender: gender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
                        }
                    }
                    
                    self.events.append(Event(creator: creator, eventId: eventId, isGroupEvent: isGroupEvent, latitude: latitude, locationAddress: locationAddress, locationId: locationId, locationName: locationName, longitude: longitude, maxPeople: maxPeople, dateStart: dateStart, eventName: eventName, picChangedAmount: picChangedAmount, priceBegin: priceBegin, priceEnd: priceEnd, remarks: remarks, typeOfEvent: typeOfEvent, timeMade: timeMade, timeLastChanged: timeLastChanged, nrOfPeopleInvited: nrOfPeopleInvited,joinedPeople: joinedPeople, invitedPeople: invitedPeople))
                }
            }
        } catch{
            print("error")
        }
        
        do{
            try self.context.save()
            self.sortEvents()
            self.refreshControl.endRefreshing()
        } catch{
            
        }
    }
    
    func deleteEventFromCoreData(eventId : String){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CoreDataEvent")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "eventId") as! String == eventId}){
                    self.context.delete(result as! NSManagedObject)
                }
            }
        } catch{
            print("error")
        }
        
        do{
            try self.context.save()
        } catch{
            
        }
    }
    
    func moveEvent(eventId : String, dict : [String : Any], snapshot: DataSnapshot, theRef: DatabaseReference!){
        // stop the observation of the ref you are about to delete:
        theRef.removeAllObservers()
        
        // delete event images
        let localDispatchGroup = DispatchGroup()
        localDispatchGroup.enter()
        let eventPicRef = Storage.storage().reference(withPath: "pics/Events/" + eventId + "/eventPic.jpg")
        eventPicRef.delete { error in
            if error != nil {
                localDispatchGroup.leave()
            } else {
                localDispatchGroup.leave()
            }
        }
        
        // move the rest from firebase
        let creator = String(describing: dict["creator"]!)
        var joinedPeopleList = [String]()
        if let joinedPeople = dict["joinedPeople"] as? [String: String]{
            for (key, _) in joinedPeople{
                joinedPeopleList.append(key)
            }
        }
        
//        localDispatchGroup.enter()
//        self.Db?.child("EventInvitees").child(eventId).observeSingleEvent(of: .value, with: { (snapshot) in
//            self.Db?.child("oldEventInvitees").child(eventId).setValue(snapshot.value){ (error, snapshot) in
//                self.Db?.child("EventInvitees").child(eventId).removeValue(){ (error, snapshot) in
//                    self.Db?.child("Users").child(creator).child("myEvents").child(eventId).removeValue(){ (error, snapshot) in
//                        localDispatchGroup.leave()
//                    }
//                }
//            }
//        })

        // remove from people's public profile
        localDispatchGroup.enter()
        let enumerator = snapshot.childSnapshot(forPath: "eventInvitees").children
        while let localUserId = enumerator.nextObject() as? DataSnapshot{
            localDispatchGroup.enter()
            self.Db?.child("publicProfiles").child(self.getString(object: localUserId.value)).child("events").child(eventId).removeValue(){ (error, snapshot) in
                localDispatchGroup.leave()
            }
        }
        localDispatchGroup.leave()
        
        // move event data
        localDispatchGroup.enter()
        self.Db?.child("oldEvents").child(eventId).setValue(snapshot.value){ (error, snapshot) in
            self.Db?.child("Events").child(eventId).removeValue(){ (error, snapshot) in
                localDispatchGroup.leave()
            }
        }
        
        // move chats
        localDispatchGroup.enter()
        self.Db?.child("eventChats").child(eventId).observeSingleEvent(of: .value, with: { (snapshot) in
            if(snapshot.value != nil){
                self.Db?.child("oldEventChats").child(eventId).setValue(snapshot.value){ (error, snapshot) in
                    self.Db?.child("eventChats").child(eventId).removeValue(){ (error, snapshot) in
                        localDispatchGroup.leave()
                    }
                }
            }
        })
        
        // remove joined people
        for personId in joinedPeopleList{
            if(personId != creator){
                localDispatchGroup.enter()
                self.Db?.child("Users").child(personId).child("joinedEvents").child(eventId).removeValue(){ (error, snapshot) in
                    localDispatchGroup.leave()
                }
            }
        }

        localDispatchGroup.notify(queue: .main){
            self.deleteEventFromCoreData(eventId: eventId)
            self.checkIfInitialLoadingOrDatabaseChangeEvent()
            
        }
    }
    
    func loadEvent(dict: [String : Any], snapshot: DataSnapshot){
        let creator = self.getString(object: dict["creator"])
        let eventId = self.getString(object: dict["eventId"])
        let isGroupEvent = (String(describing: dict["groupOrNot"]!) == "group")
        let latitude = self.getString(object: dict["latitude"])
        let locationAddress = self.getString(object: dict["locationAddress"])
        let locationId = self.getString(object: dict["locationId"])
        let locationName = self.getString(object: dict["locationName"])
        let longitude = self.getString(object: dict["longitude"])
        let maxPeople = self.getString(object: dict["maxPeople"])
        let dateStart = Date(milliseconds: Int(String(describing: dict["miliseconds"]!))!)
        let eventName = self.getString(object: dict["name"])
        let picChangedAmount = self.getString(object: dict["picChangedAmount"])
        let priceBegin = self.getString(object: dict["priceBegin"])
        let priceEnd = self.getString(object: dict["priceEnd"])
        let remarks = self.getString(object: dict["remarks"])
        let typeOfEvent = self.getString(object: dict["typeOfEvent"])
        let timeMade = self.getString(object: dict["timeMade"])
        let timeLastChanged = self.getString(object: dict["timeLastChanged"])
        let nrOfPeopleInvited = self.getString(object: dict["nrOfPeopleInvited"])
        
        var invitedPeople = [localContactStruct]()
        var joinedPeople = [localContactStruct]()
        
        let myGroup = DispatchGroup()
        myGroup.enter()
        
        // get joined people into localcontactstruct
        let joinedPeopleSnapshot = snapshot.childSnapshot(forPath: "joinedPeople")
        let enumeratorJP = joinedPeopleSnapshot.children
        while let person = enumeratorJP.nextObject() as? DataSnapshot{
            let theNSObject = self.contactExists(contactId: person.key, entityName: "Contact", context: self.context)
            var theContactChanged = false
            if(theNSObject != nil){
                let theTimeLastChangedString = theNSObject?.value(forKey: "timeLastChanged") as! String
                let theTimeLastChanged = Int64(theTimeLastChangedString)
                theContactChanged = contactInfoChanged(userId: person.key, timeLastChangedDatabase: theTimeLastChanged!)
            }
            if(theNSObject != nil && !theContactChanged){
                let personId = theNSObject?.value(forKey: "userId") as! String
                let personName = theNSObject?.value(forKey: "name") as! String
                let personFavCuis = theNSObject?.value(forKey: "favCuis") as! String
                let personFavRest = theNSObject?.value(forKey: "favRest") as! String
                let personGender = theNSObject?.value(forKey: "gender") as! String
                let picChangedAmount = theNSObject?.value(forKey: "picChangedAmount") as! String
                let timeLastChanged = theNSObject?.value(forKey: "timeLastChanged") as! String
                
                joinedPeople.append(localContactStruct(id: personId, name: personName, favCuis: personFavCuis, favRest: personFavRest, gender: personGender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
            } else {
                myGroup.enter()
                self.Db?.child("publicProfiles").child(person.key).observeSingleEvent(of: .value, with: { (snapshot) in
                    if let dict = snapshot.value as? [String : Any]{
                        var favCuisString = self.getString(object: dict["favCuis"])
                        if(favCuisString == ""){
                            favCuisString = "no favourite cuisine"
                        }
                        
                        var favRestString = self.getString(object: dict["favRest"])
                        if(favRestString == ""){
                            favRestString = "no favourite restaurant"
                        }
                        
                        var gender = self.getString(object: dict["favRest"])
                        if(gender == ""){
                            gender = "no gender filled in"
                        }
                        
                        let nameString = self.getString(object: dict["name"])
                        
                        var picChangedAmount = self.getString(object: dict["picChangedAmount"])
                        if(picChangedAmount == ""){
                            picChangedAmount = "0"
                        }
                        
                        let timeLastChanged = self.getString(object: dict["timeLastChanged"])
                        
                        joinedPeople.append(localContactStruct(id: String(describing: person.key), name: String(describing: nameString), favCuis: favCuisString, favRest: favRestString, gender: gender, picChangedAmount: picChangedAmount, timeLastChanged : timeLastChanged))
                        myGroup.leave()
                    }
                    
                })
            }
        }
        myGroup.leave()
        
        // get invited people into localcontactstructs
        let snapshotLocal = snapshot.childSnapshot(forPath: "eventInvitees")
        let enumeratorLocal = snapshotLocal.children
        while let person = enumeratorLocal.nextObject() as? DataSnapshot{
            myGroup.enter()
            let theNSObject = self.contactExists(contactId: person.key, entityName: "Contact", context: self.context) // checks if you already have the contact locally
            var theContactChanged = false
            if(theNSObject != nil){
                let theTimeLastChangedString = theNSObject?.value(forKey: "timeLastChanged") as! String
                let theTimeLastChanged = Int64(theTimeLastChangedString)
                theContactChanged = self.contactInfoChanged(userId: person.key, timeLastChangedDatabase: theTimeLastChanged!)
            }
            if(theNSObject != nil && !theContactChanged){ // you do have it locally, use local storage
                let personId = theNSObject?.value(forKey: "userId") as! String
                let personName = theNSObject?.value(forKey: "name") as! String
                let personFavCuis = theNSObject?.value(forKey: "favCuis") as! String
                let personFavRest = theNSObject?.value(forKey: "favRest") as! String
                let personGender = theNSObject?.value(forKey: "gender") as! String
                let picChangedAmount = theNSObject?.value(forKey: "picChangedAmount") as! String
                let timeLastChanged = theNSObject?.value(forKey: "timeLastChanged") as! String

                invitedPeople.append(localContactStruct(id: personId, name: personName, favCuis: personFavCuis, favRest: personFavRest, gender: personGender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
                myGroup.leave()
            } else { // you don't have it locally, use database
                self.Db?.child("Users").child(person.key).observeSingleEvent(of: .value, with: { (snapshot) in
                    if let dict = snapshot.value as? [String : Any]{
                        var favCuisString = self.getString(object: dict["favCuis"])
                        if(favCuisString == ""){
                            favCuisString = "no favourite cuisine"
                        }

                        var favRestString = self.getString(object: dict["favRest"])
                        if(favRestString == ""){
                            favRestString = "no favourite restaurant"
                        }

                        var gender = self.getString(object: dict["favRest"])
                        if(gender == ""){
                            gender = "no gender filled in"
                        }

                        let nameString = self.getString(object: dict["name"])

                        var picChangedAmount = self.getString(object: dict["picChangedAmount"])
                        if(picChangedAmount == ""){
                            picChangedAmount = "0"
                        }

                        let timeLastChanged = self.getString(object: dict["timeLastChanged"])
                        invitedPeople.append(localContactStruct(id: String(describing: person.key), name: String(describing: nameString), favCuis: favCuisString, favRest: favRestString, gender: gender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
                        myGroup.leave()
                    }

                })
            }
        }
//        myGroup.leave()
        
        myGroup.notify(queue: .main){
            if let i = self.events.index(where: {$0.eventId == eventId}){
                self.events.remove(at: i)
            } // if event already exists, delete it
            
            self.deleteEventFromCoreData(eventId: eventId)

            self.events.append(Event(creator: creator, eventId: eventId, isGroupEvent: isGroupEvent, latitude: latitude, locationAddress: locationAddress, locationId: locationId, locationName: locationName, longitude: longitude, maxPeople: maxPeople, dateStart: dateStart, eventName: eventName, picChangedAmount: picChangedAmount, priceBegin: priceBegin, priceEnd: priceEnd, remarks: remarks, typeOfEvent: typeOfEvent, timeMade: timeMade, timeLastChanged: timeLastChanged, nrOfPeopleInvited: nrOfPeopleInvited, joinedPeople: joinedPeople, invitedPeople: invitedPeople))
            
            var invitedPeopleCoreData = NSSet()
            for invitedPerson in invitedPeople{
                let theContact = self.contactExists(contactId: invitedPerson.id!, entityName: "Contact", context: self.context)
                if(theContact != nil){
                    theContact?.setValue(invitedPerson.id, forKey: "userId")
                    theContact?.setValue(invitedPerson.name, forKey: "name")
                    theContact?.setValue(invitedPerson.favCuis, forKey: "favCuis")
                    theContact?.setValue(invitedPerson.favRest, forKey: "favRest")
                    theContact?.setValue(invitedPerson.gender, forKey: "gender")
                    theContact?.setValue(invitedPerson.picChangedAmount, forKey: "picChangedAmount")
                    theContact?.setValue(invitedPerson.timeLastChanged, forKey: "timeLastChanged")
                    invitedPeopleCoreData = invitedPeopleCoreData.adding(theContact as Any) as NSSet
                } else {
                    let newContact = NSEntityDescription.insertNewObject(forEntityName: "Contact" , into: self.context)
                    newContact.setValue(invitedPerson.id, forKey: "userId")
                    newContact.setValue(invitedPerson.name, forKey: "name")
                    newContact.setValue(invitedPerson.favCuis, forKey: "favCuis")
                    newContact.setValue(invitedPerson.favRest, forKey: "favRest")
                    newContact.setValue(invitedPerson.gender, forKey: "gender")
                    newContact.setValue(invitedPerson.picChangedAmount, forKey: "picChangedAmount")
                    newContact.setValue(invitedPerson.timeLastChanged, forKey: "timeLastChanged")
                    invitedPeopleCoreData = invitedPeopleCoreData.adding(newContact as Any) as NSSet
                }
            }
            
            // save inbetween
            do{
                try self.context.save()
            } catch{
                
            }
            
            var joinedPeopleCoreData = NSSet()
            for joinedPerson in joinedPeople{
                let theContact = self.contactExists(contactId: joinedPerson.id!, entityName: "Contact", context: self.context)
                if(theContact != nil){
                    theContact?.setValue(joinedPerson.id, forKey: "userId")
                    theContact?.setValue(joinedPerson.name, forKey: "name")
                    theContact?.setValue(joinedPerson.favCuis, forKey: "favCuis")
                    theContact?.setValue(joinedPerson.favRest, forKey: "favRest")
                    theContact?.setValue(joinedPerson.gender, forKey: "gender")
                    theContact?.setValue(joinedPerson.picChangedAmount, forKey: "picChangedAmount")
                    theContact?.setValue(joinedPerson.timeLastChanged, forKey: "timeLastChanged")
                    
                    joinedPeopleCoreData = joinedPeopleCoreData.adding(theContact as Any) as NSSet
                } else {
                    let newContact = NSEntityDescription.insertNewObject(forEntityName: "Contact" , into: self.context)
                    newContact.setValue(joinedPerson.id, forKey: "userId")
                    newContact.setValue(joinedPerson.name, forKey: "name")
                    newContact.setValue(joinedPerson.favCuis, forKey: "favCuis")
                    newContact.setValue(joinedPerson.favRest, forKey: "favRest")
                    newContact.setValue(joinedPerson.gender, forKey: "gender")
                    newContact.setValue(joinedPerson.picChangedAmount, forKey: "picChangedAmount")
                    newContact.setValue(joinedPerson.timeLastChanged, forKey: "timeLastChanged")
                    
                    joinedPeopleCoreData = joinedPeopleCoreData.adding(newContact as Any) as NSSet
                }
            }
            
            let newEvent = NSEntityDescription.insertNewObject(forEntityName: "CoreDataEvent" , into: self.context)
            newEvent.setValue(creator, forKey: "creator")
            newEvent.setValue(dateStart.millisecondsSince1970, forKey: "dateStart")
            newEvent.setValue(eventId, forKey: "eventId")
            newEvent.setValue(eventName, forKey: "eventName")
            newEvent.setValue(isGroupEvent, forKey: "isGroupEvent")
            newEvent.setValue(latitude, forKey: "latitude")
            newEvent.setValue(locationAddress, forKey: "locationAddress")
            newEvent.setValue(locationId, forKey: "locationId")
            newEvent.setValue(locationName, forKey: "locationName")
            newEvent.setValue(longitude, forKey: "longitude")
            newEvent.setValue(maxPeople, forKey: "maxPeople")
            newEvent.setValue(nrOfPeopleInvited, forKey: "nrOfPeopleInvited")
            newEvent.setValue(picChangedAmount, forKey: "picChangedAmount")
            newEvent.setValue(priceBegin, forKey: "priceBegin")
            newEvent.setValue(priceEnd, forKey: "priceEnd")
            newEvent.setValue(remarks, forKey: "remarks")
            newEvent.setValue(timeLastChanged, forKey: "timeLastChanged")
            newEvent.setValue(timeMade, forKey: "timeMade")
            newEvent.setValue(typeOfEvent, forKey: "typeOfEvent")
            
            newEvent.setValue(joinedPeopleCoreData, forKey: "joinedPeople")
            newEvent.setValue(invitedPeopleCoreData, forKey: "invitedPeople")
            
            do{
                try self.context.save()
            } catch{
                print("could not save event into local storage")
            }
            
            self.checkIfInitialLoadingOrDatabaseChangeEvent() 
        }
    }
    
    func checkIfInitialLoadingOrDatabaseChangeEvent(){
//        print("eventscounter: ")
//        print(self.eventsCounter)
        if(self.eventsCounter > 0){ // initial loading
            self.myEventsDispatchGroup.leave()
            self.eventsCounter -= 1
        } else if(self.eventsCounter == 0){ // information in database has changed
            self.sortEvents()
        }
    }
    
    func loadSingleEventFromDatabase(eventId : String){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CoreDataEvent")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "eventId") as! String == eventId}) as? NSManagedObject{
                    let creator = result.value(forKey: "creator") as! String
                    let dateMilliseconds = result.value(forKey: "dateStart") as! Int
                    let dateStart = Date(milliseconds: dateMilliseconds)
                    let eventId = result.value(forKey: "eventId") as! String
                    let eventName = result.value(forKey: "eventName") as! String
                    let isGroupEvent = result.value(forKey: "isGroupEvent") as! Bool
                    let latitude = result.value(forKey: "latitude") as! String
                    let locationAddress = result.value(forKey: "locationAddress") as! String
                    let locationId = result.value(forKey: "locationId") as! String
                    let locationName = result.value(forKey: "locationName") as! String
                    let longitude = result.value(forKey: "longitude") as! String
                    let maxPeople = result.value(forKey: "maxPeople") as! String
                    let nrOfPeopleInvited = result.value(forKey: "nrOfPeopleInvited") as! String
                    let picChangedAmount = result.value(forKey: "picChangedAmount") as! String
                    let priceBegin = result.value(forKey: "priceBegin") as! String
                    let priceEnd = result.value(forKey: "priceEnd") as! String
                    let remarks = result.value(forKey: "remarks") as! String
                    let timeLastChanged = result.value(forKey: "timeLastChanged") as! String
                    let timeMade = result.value(forKey: "timeMade") as! String
                    let typeOfEvent = result.value(forKey: "typeOfEvent") as! String

                    let invitedPeopleSet : NSSet = result.value(forKey: "invitedPeople") as! NSSet
                    let joinedPeopleSet : NSSet = result.value(forKey: "joinedPeople") as! NSSet
                    
                    
                    var joinedPeople = [localContactStruct]()
                    for personAny in joinedPeopleSet.allObjects{
                        if let person = personAny as? NSManagedObject{
                            let id = person.value(forKey: "userId") as! String
                            let name = person.value(forKey: "name") as! String
                            let favCuis = person.value(forKey: "favCuis") as! String
                            let favRest = person.value(forKey: "favRest") as! String
                            let gender = person.value(forKey: "gender") as! String
                            let picChangedAmount = person.value(forKey: "picChangedAmount") as! String
                            let timeLastChanged = person.value(forKey: "timeLastChanged") as! String
                            
                            joinedPeople.append(localContactStruct(id: id, name: name, favCuis: favCuis, favRest: favRest, gender: gender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
                        }
                    }
                    
                    var invitedPeople = [localContactStruct]()
                    for personAny in invitedPeopleSet{
                        if let person = personAny as? NSManagedObject{
                            let id = person.value(forKey: "userId") as! String
                            let name = person.value(forKey: "name") as! String
                            let favCuis = person.value(forKey: "favCuis") as! String
                            let favRest = person.value(forKey: "favRest") as! String
                            let gender = person.value(forKey: "gender") as! String
                            let picChangedAmount = person.value(forKey: "picChangedAmount") as! String
                            let timeLastChanged = person.value(forKey: "timeLastChanged") as! String
                            
                            invitedPeople.append(localContactStruct(id: id, name: name, favCuis: favCuis, favRest: favRest, gender: gender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
                        }
                    }
                    
                    if let i = self.events.index(where: {$0.eventId == eventId}){
                        self.events.remove(at: i)
                    } // if event already exists, delete it
                    self.events.append(Event(creator: creator, eventId: eventId, isGroupEvent: isGroupEvent, latitude: latitude, locationAddress: locationAddress, locationId: locationId, locationName: locationName, longitude: longitude, maxPeople: maxPeople, dateStart: dateStart, eventName: eventName, picChangedAmount: picChangedAmount, priceBegin: priceBegin, priceEnd: priceEnd, remarks: remarks, typeOfEvent: typeOfEvent, timeMade: timeMade, timeLastChanged: timeLastChanged, nrOfPeopleInvited: nrOfPeopleInvited,joinedPeople: joinedPeople, invitedPeople: invitedPeople))
                }
            }
        } catch{
            print("error")
        }
        self.checkIfInitialLoadingOrDatabaseChangeEvent()
        
    }
    
    func eventInfoChanged(eventId : String, timeLastChangedDatabase : Int64)->Bool{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CoreDataEvent")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "eventId") as! String == eventId}) as? NSManagedObject{
                    let timeLastChangedLocalString = result.value(forKey: "timeLastChanged") as! String
                    let timeLastChangedLocal = Int64(timeLastChangedLocalString)
                    if(timeLastChangedDatabase != timeLastChangedLocal!){
                        return true
                    } else{
                        return false
                    }
                } else {
                    return true // event is not in database, so you need to load it from firebase
                }
            } else {
                return true // there are no events in the local database, so you need to load them from firebase
            }
        } catch {
            return false
        }
    }
    
    func deleteOldLocalEvents(){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "CoreDataEvent")
        fetchRequest.returnsObjectsAsFaults = false
        
        do
        {
            let results = try context.fetch(fetchRequest)
            for managedObject in results
            {
                let theManagedObject = managedObject as! NSManagedObject
                let theId = theManagedObject.value(forKey: "eventId") as? String
                if(!self.invitedToEventIds.contains(theId!)){
                    context.delete(managedObject as! NSManagedObject)
                    if(self.events.contains(where: {$0.eventId == theId})){
                        self.events.remove(at: events.index(where: {$0.eventId == theId!})!)
                    }
                }
            }
        } catch {
            print("old local events error")
        }
        
        do{
            try context.save()
        } catch{
            print("could not save old event deletions into local storage")
        }
    }
    
    func getEvents(){
        deleteOldLocalEvents()
        
        for eventId in self.invitedToEventIds{
            self.myEventsDispatchGroup.enter()
            eventsCounter += 1
            let localRef : DatabaseReference! = self.Db?.child("Events").child(eventId).child("timeLastChanged")
            self.allEventObserveRefs.append(localRef)
//            self.allObserveRefs.append(localRef)
            print("its outside the getevents observer")
            localRef.observe(DataEventType.value, with: { (snapshot) in
                print("observer acivated for: " + eventId)
                let timeLastChanged = self.getString(object: snapshot.value)
                if(self.eventInfoChanged(eventId: eventId, timeLastChangedDatabase: Int64(timeLastChanged)!)){ // event needs to be downloaded
                    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CoreDataEvent")
                    request.returnsObjectsAsFaults = false
                    
                    let localRef2 : DatabaseReference! = self.Db?.child("Events").child(eventId)
                    localRef2.observeSingleEvent(of: .value, with: { (snapshot) in
                        if let dict = snapshot.value as? [String : Any]{
                            let millisecondsEvent = Int(String(describing: dict["miliseconds"]!))!
                            let millisecondsNow = Date().millisecondsSince1970
                            // move event, it started over 6 hours ago
                            if(millisecondsEvent - millisecondsNow < -21600000){
                                self.moveEvent(eventId: eventId, dict: dict, snapshot: snapshot, theRef: localRef)
                            } // load event
                            else {
                                self.loadEvent(dict: dict, snapshot: snapshot)
                            }
                        }
                    })
                } else { // event can be taken from local storage
                    let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CoreDataEvent")
                    request.returnsObjectsAsFaults = false
                    
                    do {
                        let results = try self.context.fetch(request)
                        if results.count > 0{
                            if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "eventId") as! String == eventId}) as? NSManagedObject{
                                let millisecondsEvent = result.value(forKey: "dateStart") as! Int64
                                let millisecondsNow = Int64(Date().millisecondsSince1970)
                                
                                if(millisecondsEvent - millisecondsNow < -21600000){
                                    let localRef2 : DatabaseReference! = self.Db?.child("Events").child(eventId)
                                    localRef2.observeSingleEvent(of: .value, with: { (snapshot) in
                                        if let dict = snapshot.value as? [String : Any]{
                                            self.moveEvent(eventId: eventId, dict: dict, snapshot: snapshot, theRef: localRef)
                                        }
                                    })
                                } else {
                                    self.loadSingleEventFromDatabase(eventId: eventId)
                                }
                            }
                        }
                    } catch{
                        print("error")
                    }
                }
//                else{
//                    self.checkIfInitialLoadingOrDatabaseChangeEvent()
//                }
            })
        }
        self.myEventsDispatchGroup.notify(queue: .main){
            self.refreshControl.endRefreshing()
            self.sortEvents()
        }
    }
    
    func getEventsData(){
        // if there is no internet, get data from local storage.
        if(!Reachability.isConnectedToNetwork()){ // no internet
            getEventsFromStorage()
        }
        else{
            // new way
            Db?.child("publicProfiles").child(self.userId!).child("events").observe(DataEventType.value, with: { (snapshot) in
                for aRef in self.allEventObserveRefs{
                    aRef.removeAllObservers()
                }
                let enumerator = snapshot.children
                self.invitedToEventIds = [String]()
                while let rest = enumerator.nextObject() as? DataSnapshot{
                    self.invitedToEventIds.append(rest.key)
                    print(rest.key)
                }
                self.events = [Event]()
                self.getEvents()
            })
            // end new way
            
            // old way
//            Db?.child("EventInvitees").observeSingleEvent(of: .value, with: { (snapshot) in
//                let enumerator = snapshot.children
//                self.invitedToEventIds = [String]()
//
//                while let rest = enumerator.nextObject() as? DataSnapshot {
//                    if let dict = rest.value as? [String: String]{
//                        if(dict.keys.contains(self.userId!)){
//                            self.invitedToEventIds.append(rest.key)
//                        }
//                    }
//                }
//                self.events = [Event]()
//                self.getEvents()
//            })
            // end old way
        }
    }
    
    func myContactInfoChanged(userId : String, timeLastChangedDatabase : Int64)->Bool{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MyContact")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "userId") as! String == userId}) as? NSManagedObject{
                    if let timeLastChangedLocalString = result.value(forKey: "timeLastChanged") as? String{
                        let timeLastChangedLocal = Int64(timeLastChangedLocalString)
                        if(timeLastChangedDatabase != timeLastChangedLocal!){
                            return true
                        } else{
                            return false
                        }
                    } else {
                        return true // contact has no timeLastChanged variable
                    }
                    
                } else {
                    return true // contact is not in database, so you need to load it from firebase
                }
            } else {
                return true // there are no contacts in the local database, so you need to load them from firebase
            }
        } catch {
            return false
        }
    }
    
    func contactInfoChanged(userId : String, timeLastChangedDatabase : Int64)->Bool{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "userId") as! String == userId}) as? NSManagedObject{
                    if let timeLastChangedLocalString = result.value(forKey: "timeLastChanged") as? String{
                        let timeLastChangedLocal = Int64(timeLastChangedLocalString)
                        if(timeLastChangedDatabase != timeLastChangedLocal!){
                            return true
                        } else{
                            return myContactInfoChanged(userId: userId, timeLastChangedDatabase: timeLastChangedDatabase)
//                            return false
                        }
                    } else {
                        return true // contact has no timeLastChanged variable
                    }
                    
                } else {
                    return true // contact is not in database, so you need to load it from firebase
                }
            } else {
                return true // there are no contacts in the local database, so you need to load them from firebase
            }
        } catch {
            return false
        }
    }

    func userInfoChanged(userId : String, timeLastChangedDatabase : Int64)->Bool{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "userId") as! String == userId}) as? NSManagedObject{
                    if let timeLastChangedLocalString = result.value(forKey: "timeLastChanged") as? String{
                        let timeLastChangedLocal = Int64(timeLastChangedLocalString)
                        if(timeLastChangedDatabase != timeLastChangedLocal!){
                            return true
                        } else{
                            return false
                        }
                    } else {
                        return true // contact has no timeLastChanged variable
                    }
                    
                } else {
                    return true // contact is not in database, so you need to load it from firebase
                }
            } else {
                return true // there are no contacts in the local database, so you need to load them from firebase
            }
        } catch {
            return false
        }
    }
    
    func getUserInfo(){
        self.myContactsDispatchGroup.enter()
        self.myContactsCounter += 1
        let localRef : DatabaseReference! = self.Db?.child("publicProfiles").child(userId!).child("timeLastChanged")
        self.allObserveRefs.append(localRef)
        localRef.observe(DataEventType.value, with: { (snapshot) in
            let timeLastChanged = self.getString(object: snapshot.value)
            let contactId = self.userId!
            if(self.userInfoChanged(userId: contactId, timeLastChangedDatabase: Int64(timeLastChanged)!)){ // contact needs to be re-downloaded
                self.getUserInfoFromDatabase(userId: contactId)
                self.getContactFromDatabase(contactId: contactId)
            } else { // contact can be loaded from core data
                // no need to do anything
                self.myContactsCounter -= 1
                self.myContactsDispatchGroup.leave()
            }
        })
    }
    
    func getUserInfoFromDatabase(userId: String){
        Db?.child("publicProfiles").child(userId).observeSingleEvent(of: .value, with: { (snapshot) in
            if let dict = snapshot.value as? [String : Any]{
                
                var favCuis = ""
                if let a = dict["favCuis"]{
                    favCuis = self.getString(object: a)
                } else {
                    favCuis = "no favourite cuisine"
                }
                
                var favRest = ""
                if let b = dict["favRest"]{
                    favRest = self.getString(object: b)
                } else {
                    favRest = "no favourite restaurant"
                }
                
                var name = ""
                if let c = dict["name"]{
                    name = self.getString(object: c)
                } else {
                    name = "no name"
                }
                
                var gender = ""
                if let d = dict["gender"]{
                    gender = self.getString(object: d)
                } else {
                    gender = "no gender specified"
                }
                
                var picChangedAmount = ""
                if let e = dict["picChangedAmount"]{
                    picChangedAmount = self.getString(object: e)
                } else {
                    picChangedAmount = "0"
                }
                
                var timeLastChanged = ""
                if let f = dict["timeLastChanged"]{
                    timeLastChanged = self.getString(object: f)
                } else {
                    timeLastChanged = "0"
                }
                
                let theContact = self.contactExists(contactId: userId, entityName: "User", context: self.context)
                if(theContact != nil){
                    theContact?.setValue(favCuis, forKey: "favCuis")
                    theContact?.setValue(favRest, forKey: "favRest")
                    theContact?.setValue(name, forKey: "name")
                    theContact?.setValue(gender, forKey: "gender")
                    theContact?.setValue(timeLastChanged, forKey: "timeLastChanged")
                    theContact?.setValue(userId, forKey: "userId")
                    theContact?.setValue(picChangedAmount, forKey: "picChangedAmount")
                } else {
                    let newContact = NSEntityDescription.insertNewObject(forEntityName: "User" , into: self.context)
                    newContact.setValue(favCuis, forKey: "favCuis")
                    newContact.setValue(favRest, forKey: "favRest")
                    newContact.setValue(name, forKey: "name")
                    newContact.setValue(gender, forKey: "gender")
                    newContact.setValue(timeLastChanged, forKey: "timeLastChanged")
                    newContact.setValue(userId, forKey: "userId")
                    newContact.setValue(picChangedAmount, forKey: "picChangedAmount")
                }
                
                do{
                    try self.context.save()
                } catch{
                    print("could not save contact into local storage")
                }
                
            }
            self.checkIfInitialLoadingOrDatabaseChangeContact()
        })
    }
    
    func getContactFromDatabase(contactId : String){
        Db?.child("publicProfiles").child(contactId).observeSingleEvent(of: .value, with: { (snapshot) in
            if let dict = snapshot.value as? [String : Any]{
                var favCuis = ""
                if let a = dict["favCuis"]{
                    favCuis = self.getString(object: a)
                } else {
                    favCuis = "no favourite cuisine"
                }

                var favRest = ""
                if let b = dict["favRest"]{
                    favRest = self.getString(object: b)
                } else {
                    favRest = "no favourite restaurant"
                }

                var name = ""
                if let c = dict["name"]{
                    name = self.getString(object: c)
                } else {
                    name = "no name"
                }

                var gender = ""
                if let d = dict["gender"]{
                    gender = self.getString(object: d)
                } else {
                    gender = "no gender specified"
                }
                
                var picChangedAmount = ""
                if let e = dict["picChangedAmount"]{
                    picChangedAmount = self.getString(object: e)
                } else {
                    picChangedAmount = "0"
                }
                
                var timeLastChanged = ""
                if let f = dict["timeLastChanged"]{
                    timeLastChanged = self.getString(object: f)
                } else {
                    timeLastChanged = "0"
                }
                
//                print()
//                print()
//                print(contactId)
//                print(name)
//                print(favCuis)
//                print(favRest)
                
                let theContact = self.contactExists(contactId: contactId, entityName: "Contact", context: self.context)
                if(theContact != nil){
                    theContact?.setValue(favCuis, forKey: "favCuis")
                    theContact?.setValue(favRest, forKey: "favRest")
                    theContact?.setValue(name, forKey: "name")
                    theContact?.setValue(gender, forKey: "gender")
                    theContact?.setValue(timeLastChanged, forKey: "timeLastChanged")
                    theContact?.setValue(contactId, forKey: "userId")
                    theContact?.setValue(picChangedAmount, forKey: "picChangedAmount")
                } else {
                    let newContact = NSEntityDescription.insertNewObject(forEntityName: "Contact" , into: self.context)
                    newContact.setValue(favCuis, forKey: "favCuis")
                    newContact.setValue(favRest, forKey: "favRest")
                    newContact.setValue(name, forKey: "name")
                    newContact.setValue(gender, forKey: "gender")
                    newContact.setValue(timeLastChanged, forKey: "timeLastChanged")
                    newContact.setValue(contactId, forKey: "userId")
                    newContact.setValue(picChangedAmount, forKey: "picChangedAmount")
                }
                
                let MyContact = self.contactExists(contactId: contactId, entityName: "MyContact", context: self.context)
                if(MyContact != nil){
                    MyContact?.setValue(favCuis, forKey: "favCuis")
                    MyContact?.setValue(favRest, forKey: "favRest")
                    MyContact?.setValue(name, forKey: "name")
                    MyContact?.setValue(gender, forKey: "gender")
                    MyContact?.setValue(timeLastChanged, forKey: "timeLastChanged")
                    MyContact?.setValue(contactId, forKey: "userId")
                    MyContact?.setValue(picChangedAmount, forKey: "picChangedAmount")
                } else {
                    let newMyContact = NSEntityDescription.insertNewObject(forEntityName: "MyContact" , into: self.context)
                    newMyContact.setValue(favCuis, forKey: "favCuis")
                    newMyContact.setValue(favRest, forKey: "favRest")
                    newMyContact.setValue(name, forKey: "name")
                    newMyContact.setValue(gender, forKey: "gender")
                    newMyContact.setValue(timeLastChanged, forKey: "timeLastChanged")
                    newMyContact.setValue(contactId, forKey: "userId")
                    newMyContact.setValue(picChangedAmount, forKey: "picChangedAmount")
                }
                
                do{
                    try self.context.save()
                } catch{
                    print("could not save contact into local storage")
                }
                
            }
            self.checkIfInitialLoadingOrDatabaseChangeContact()
        })
    }
    
    func groupInfoChanged(groupId : String, timeLastChangedDatabase : Int64)->Bool{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Group")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "id") as! String == groupId}) as? NSManagedObject{
                    if let timeLastChangedLocalString = result.value(forKey: "timeLastChanged") as? String{
                        let timeLastChangedLocal = Int64(timeLastChangedLocalString)
                        if(timeLastChangedDatabase != timeLastChangedLocal!){
                            return true
                        } else{
                            return false
                        }
                    } else {
                        return true // group has no timeLastChanged variable
                    }
                    
                } else {
                    return true // group is not in database, so you need to load it from firebase
                }
            } else {
                return true // there are no groups in the local database, so you need to load them from firebase
            }
        } catch {
            return false
        }
    }
    
    func getGroupFromDatabase(groupId : String){
        Db?.child("groups").child(groupId).observeSingleEvent(of: .value, with: { (snapshot) in
            print("in group info")
            if let dict = snapshot.value as? [String : Any]{
                var groupName = ""
                if let a = dict["name"] as? String{
                    groupName = a
                }
                
                var groupAdmin = ""
                if let a = dict["admin"] as? String{
                    groupAdmin = a
                }
                
                var timeLastChanged = ""
                if let e = dict["timeLastChanged"] as? String{
                    timeLastChanged = e
                }
                
                let outerDispatchGroup = DispatchGroup()
                var contactsForGroup = NSSet()
                if let contacts = dict["contacts"] as? [String : Any]{
                    for (_, value) in contacts{
                        outerDispatchGroup.enter()
                        if let contact = value as? [String : String]{
                            let myContact = self.contactExists(contactId: contact["id"]!, entityName: "Contact", context: self.context)
                            if(myContact != nil){
                                contactsForGroup = contactsForGroup.adding(myContact as Any) as NSSet
                                outerDispatchGroup.leave()
                            } else {
                                let aDispatchGroup = DispatchGroup()
                                aDispatchGroup.enter()
                                var theContact : NSManagedObject?
                                self.Db?.child("publicProfiles").child(contact["id"]!).observeSingleEvent(of: .value, with: { (snapshot) in
                                    if let dict = snapshot.value as? [String : Any]{
                                        var favCuis = ""
                                        if let a = dict["favCuis"]{
                                            favCuis = self.getString(object: a)
                                        } else {
                                            favCuis = "no favourite cuisine"
                                        }
                                        
                                        var favRest = ""
                                        if let b = dict["favRest"]{
                                            favRest = self.getString(object: b)
                                        } else {
                                            favRest = "no favourite restaurant"
                                        }
                                        
                                        var name = ""
                                        if let c = dict["name"]{
                                            name = self.getString(object: c)
                                        } else {
                                            name = "no name"
                                        }
                                        
                                        var gender = ""
                                        if let d = dict["gender"]{
                                            gender = self.getString(object: d)
                                        } else {
                                            gender = "no gender specified"
                                        }
                                        
                                        var timeLastChanged = ""
                                        if let e = dict["timeLastChanged"]{
                                            timeLastChanged = self.getString(object: e)
                                        } else {
                                            timeLastChanged = "0"
                                        }
                                        
                                        var picChangedAmount = ""
                                        if let f = dict["picChangedAmount"]{
                                            picChangedAmount = self.getString(object: f)
                                        } else {
                                            picChangedAmount = "0"
                                        }
                                        
                                        theContact = self.contactExists(contactId: contact["id"]!, entityName: "Contact", context: self.context)
                                        if(theContact != nil){
                                            theContact?.setValue(favCuis, forKey: "favCuis")
                                            theContact?.setValue(favRest, forKey: "favRest")
                                            theContact?.setValue(name, forKey: "name")
                                            theContact?.setValue(gender, forKey: "gender")
                                            theContact?.setValue(timeLastChanged, forKey: "timeLastChanged")
                                            theContact?.setValue(contact["id"]!, forKey: "userId")
                                            theContact?.setValue(picChangedAmount, forKey: "picChangedAmount")
                                        } else {
                                            theContact = NSEntityDescription.insertNewObject(forEntityName: "Contact" , into: self.context)
                                            theContact?.setValue(favCuis, forKey: "favCuis")
                                            theContact?.setValue(favRest, forKey: "favRest")
                                            theContact?.setValue(name, forKey: "name")
                                            theContact?.setValue(gender, forKey: "gender")
                                            theContact?.setValue(timeLastChanged, forKey: "timeLastChanged")
                                            theContact?.setValue(contact["id"]!, forKey: "userId")
                                            theContact?.setValue(picChangedAmount, forKey: "picChangedAmount")
                                        }
                                        
                                        do{
                                            try self.context.save()
                                        } catch{
                                            print("could not save event into local storage")
                                        }
                                        aDispatchGroup.leave()
                                    } else{
                                        aDispatchGroup.leave()
                                    }
                                })
                                aDispatchGroup.notify(queue: .main){
                                    contactsForGroup = contactsForGroup.adding(theContact as Any) as NSSet
                                    outerDispatchGroup.leave()
                                }
                            }
                        }
                    }
                }
                
                outerDispatchGroup.notify(queue: .main){
                    print("notified")
                    let theGroup = self.groupExists(groupId: groupId)
                    print("group found / not found")
                    if(theGroup != nil){
                        theGroup?.setValue(groupName, forKey: "name")
                        theGroup?.setValue(groupAdmin, forKey: "admin")
                        theGroup?.setValue(timeLastChanged, forKey: "timeLastChanged")
                        theGroup?.setValue(groupId, forKey: "id")
                        theGroup?.setValue(contactsForGroup, forKey: "contacts")
                    } else {
                        for a in contactsForGroup{
                            print(a)
                        }
                        print("new group")
                        let newGroup = NSEntityDescription.insertNewObject(forEntityName: "Group" , into: self.context)
                        newGroup.setValue(groupName, forKey: "name")
                        newGroup.setValue(groupAdmin, forKey: "admin")
                        newGroup.setValue(timeLastChanged, forKey: "timeLastChanged")
                        newGroup.setValue(groupId, forKey: "id")
                        newGroup.setValue(contactsForGroup, forKey: "contacts")
                    }
                    print("done")
                    
                    do{
                        try self.context.save()
                    } catch{
                        print("could not save event into local storage")
                    }
                }
            }
        })
    }
    
    func deleteOldLocalGroups(){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Group")
        fetchRequest.returnsObjectsAsFaults = false
        
        do
        {
            let results = try context.fetch(fetchRequest)
            for managedObject in results
            {
                let theManagedObject = managedObject as! NSManagedObject
                let theId = theManagedObject.value(forKey: "id") as? String
                if(!self.myGroupIds.contains(theId!)){
                    print("deleted old local group: " + theId!)
                    context.delete(managedObject as! NSManagedObject)
                }
            }
        } catch {
            print("old local groups error")
        }
        
        do{
            try context.save()
        } catch{
            print("could not save old group deletions into local storage")
        }
    }
    
    func getGroupData(){
        if(!Reachability.isConnectedToNetwork()){ // no internet
        // nothing
        } else {
            Db?.child("publicProfiles").child(userId!).child("groups").observe(DataEventType.value, with: { (snapshot) in
                for aRef in self.allGroupObserveRefs{
                    aRef.removeAllObservers()
                }
                print("public profiles snap")
                let enumerator = snapshot.children
                while let nextGroup = enumerator.nextObject() as? DataSnapshot {
                    self.myGroupIds.append(nextGroup.key)
                    let localRef : DatabaseReference! = self.Db?.child("groups").child(nextGroup.key).child("timeLastChanged")
                    self.allGroupObserveRefs.append(localRef)
                    localRef.observe(DataEventType.value, with: { (snapshot) in
                        print("group snap")
                        let timeLastChanged = self.getString(object: snapshot.value)
                        let groupId = nextGroup.key
                        if(self.groupInfoChanged(groupId: groupId, timeLastChangedDatabase: Int64(timeLastChanged)!)){ // group needs to be re-downloaded
                            print("group info changed")
                            self.getGroupFromDatabase(groupId: groupId)
                        } else{
                            // do nothing
                        }
                    })
                }
                self.deleteOldLocalGroups()
            })
        }
    }
    
    func removeAllObserveRefs(){
        for aRef in allObserveRefs{
            aRef.removeAllObservers()
        }
        for aRef in allGroupObserveRefs{
            aRef.removeAllObservers()
        }
    }
    
    func checkIfInitialLoadingOrDatabaseChangeContact(){
//        print(myContactsCounter)
        if(self.myContactsCounter > 0){ // initial loading
            self.myContactsDispatchGroup.leave()
            self.myContactsCounter -= 1
        } else if(self.myContactsCounter == 0){ // information in database has changed
            
        }
    }
    
    func deleteOldLocalContacts(){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MyContact")
        fetchRequest.returnsObjectsAsFaults = false
            
        do
        {
//            print(self.myContactIds)
            let results = try context.fetch(fetchRequest)
            for managedObject in results
            {
                let theManagedObject = managedObject as! NSManagedObject
                let theId = theManagedObject.value(forKey: "userId") as? String
                if(!self.myContactIds.contains(theId!)){
                    context.delete(managedObject as! NSManagedObject)
                }
            }
        } catch {
            print("old local contacts error")
        }
        
        do{
            try context.save()
        } catch{
            print("could not save old contact deletions into local storage")
        }
    }
    
    func getContactsData(){
        if(!Reachability.isConnectedToNetwork()){ // no internet
            showNoInternetMessage()
            self.getEventsData() // cannot update contacts
        } else {
            removeAllObserveRefs()
            getUserInfo()
            
            myContactsDispatchGroup.enter()
            Db?.child("Users").child(userId!).child("facebookFriends").observeSingleEvent(of: .value, with: { (snapshot) in
                let enumerator = snapshot.children
                
                while let rest = enumerator.nextObject() as? DataSnapshot {
                    self.myContactsCounter += 1
                    
                    self.myContactIds.append(rest.key)
                    self.myContactsDispatchGroup.enter()
                    let localRef : DatabaseReference! = self.Db?.child("publicProfiles").child(rest.key).child("timeLastChanged")
                    self.allObserveRefs.append(localRef)
                    localRef.observe(DataEventType.value, with: { (snapshot) in
                        let timeLastChanged = self.getString(object: snapshot.value)
                        let contactId = rest.key
                        if(self.contactInfoChanged(userId: contactId, timeLastChangedDatabase: Int64(timeLastChanged)!)){ // contact needs to be re-downloaded
                            self.getContactFromDatabase(contactId: contactId)
                        } else { // contact can be loaded from core data
                            // no need to do anything
                            self.checkIfInitialLoadingOrDatabaseChangeContact()
                        }
                    })
                    
//                    if let dict = rest.value as? [String: String]{
//                        self.myContactIds.append(dict["id"]!)
//                        self.myContactsDispatchGroup.enter()
//                        let localRef : DatabaseReference! = self.Db?.child("publicProfiles").child(dict["id"]!).child("timeLastChanged")
//                        self.allObserveRefs.append(localRef)
//                        localRef.observe(DataEventType.value, with: { (snapshot) in
//                            let timeLastChanged = self.getString(object: snapshot.value)
//                            let contactId = dict["id"]!
//                            if(self.contactInfoChanged(userId: contactId, timeLastChangedDatabase: Int64(timeLastChanged)!)){ // contact needs to be re-downloaded
//                                self.getContactFromDatabase(contactId: contactId)
//                            } else { // contact can be loaded from core data
//                                // no need to do anything
//                                self.checkIfInitialLoadingOrDatabaseChangeContact()
//                            }
//                        })
//                    }
                }
                self.myContactsDispatchGroup.leave()
            })
            self.myContactsDispatchGroup.notify(queue: .main){
                print("gotten contacts data")
                self.deleteOldLocalContacts()
                print("deleted old local contacts")
                self.getEventsData()
                self.getGroupData()
                self.getPresets()
            }
        }
        
    }
    
    func goToLogin(){
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginPageID") as! ViewController
        self.present(nextViewController, animated: true, completion: nil)
    }
    
    func deleteAllData(entity: String)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            for managedObject in results
            {
                managedContext.delete(managedObject as! NSManagedObject)
            }
        } catch let error as NSError {
            print("Detele all data in \(entity) error : \(error) \(error.userInfo)")
        }
        
        do{
            try managedContext.save()
        } catch{
            print("could not save deletions into local storage")
        }
    }

    func makeFloaty(){
        if floaty != nil{
            floaty.removeFromSuperview()
        }
        floaty = Floaty(frame: CGRect(x: view.frame.width-70, y: view.frame.height-(tabBarController?.tabBar.frame.height)! - 70, width: 56, height: 56))
        floaty.buttonColor = UIColor(red: 191, green: 42, blue: 42)
        view.addSubview(floaty)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(item1MainPageViewController.addEvent(gesture:)))
        floaty.addGestureRecognizer(tapGesture)
        floaty.isUserInteractionEnabled = true
    }
    
    func addSortingView(){
        let theHeight = CGFloat(50)
        sortingView = UIView(frame: CGRect(x: 0, y: 20, width: theWidth, height: theHeight))
        sortingView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        let sortingIcon = UIImageView(frame: CGRect(x: theWidth/6, y: 10, width: theWidth/6-theWidth/24, height: theHeight-20))
        sortingIcon.image = UIImage(named: "sort_icon.png")?.withRenderingMode(.alwaysTemplate)
        sortingIcon.tintColor = UIColor.white
        
        sortingViewText = UILabel(frame: CGRect(x: theWidth/3, y: 0, width: theWidth/3, height: theHeight))
        sortingViewText.text = "by date"
        sortingViewText.textColor = UIColor.white
        
        
        sortingButton = UIButton(frame: CGRect(x: theWidth/3*2, y: 0, width: theWidth/3, height: theHeight))
        let tintedImage = UIImage(named: "expand_icon.png")?.withRenderingMode(.alwaysTemplate)
        sortingButton.setImage(tintedImage, for: .normal)
        sortingButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: theWidth/6, bottom: 10, right: theWidth/12)
        sortingButton.tintColor = UIColor.white
        sortingButton.addTarget(self, action: #selector(expandSorting), for: .touchUpInside)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(expandSorting))

        sortingView.addSubview(sortingIcon)
        sortingView.addSubview(sortingViewText)
        sortingView.addSubview(sortingButton)
        sortingView.addGestureRecognizer(tapGesture)
        
        tableView.frame = CGRect(x: tableView.frame.origin.x + 5, y: 70, width: tableView.frame.width - 10, height: tableView.frame.height)
        view.addSubview(sortingView)
    }
    
    @objc func expandSorting(){
        let theHeight = CGFloat(50)
        var valueChanged = CGFloat(0)
        var imageName = ""
        
        if(sortingExpaned){
            sortingExpaned = false
            valueChanged = CGFloat(sortingViews.count * 50 * -1)
            sortingView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            for view in sortingViews{
                view.removeFromSuperview()
            }
            sortingViews = [UIView]()
            imageName = "expand_icon.png"
        } else {
            sortingExpaned = true
            var optionAndPngName = [(optionName : String, pngName : String, selector : Selector)]()
            optionAndPngName.append(("by date", "clock_icon.png", #selector(sortByTime)))
            optionAndPngName.append(("by price", "distance_icon.png", #selector(sortByPrice)))
            
            valueChanged = CGFloat(50 * optionAndPngName.count)
            sortingView.backgroundColor = UIColor.gray
            
            var counter = 0
            for option in optionAndPngName{
                let sortOption = UIView(frame: CGRect(x: 0, y: CGFloat(50 + counter * 50), width: theWidth, height: theHeight))
                sortOption.addBottomBorderWithColor(color: UIColor.white, width: 0.5)
                sortOption.addTopBorderWithColor(color: UIColor.white, width: 0.5)
                let tapGesture = UITapGestureRecognizer(target: self, action: option.selector)
                sortOption.addGestureRecognizer(tapGesture)
                sortOption.isUserInteractionEnabled = true
                
                
                let optionIcon = UIImageView(frame: CGRect(x: theWidth/6, y: 10, width: theWidth/6-theWidth/24, height: theHeight-20))
                optionIcon.image = UIImage(named: option.pngName)?.withRenderingMode(.alwaysTemplate)
                optionIcon.tintColor = UIColor.white
                
                let sortOptionLabel = UILabel(frame: CGRect(x: theWidth/3, y: 0, width: theWidth/3, height: theHeight))
                sortOptionLabel.text = option.optionName
                sortOptionLabel.textColor = UIColor.white
                
                sortOption.addSubview(optionIcon)
                sortOption.addSubview(sortOptionLabel)
                
                sortingViews.append(sortOption)
                sortingView.addSubview(sortOption)
                counter += 1
            }
            imageName = "collapse_icon.png"
        }
        
        let tintedImage = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
        sortingButton.setImage(tintedImage, for: .normal)
        sortingView.frame = CGRect(x: sortingView.frame.origin.x, y: sortingView.frame.origin.y, width: sortingView.frame.width, height: sortingView.frame.height + valueChanged)
    }
    
    @objc func sortByTime(){
        events = events.sorted(by: {$0.dateStart < $1.dateStart})
        sortingViewText.text = "By date"
        tableView.reloadData()
        expandSorting()
    }
    
    @objc func sortByPrice(){
        let length = events.count
        if length > 0{
            for i in 0...(length-1){
                if(events[i].priceBegin == "-1"){
                    events[i].priceBegin = "999999"
                }
            }
            events = events.sorted(by: {Int($0.priceBegin)! < Int($1.priceBegin)!})
            for i in 0...(length-1){
                if(events[i].priceBegin == "999999"){
                    events[i].priceBegin = "-1"
                }
            }
        }
        sortingViewText.text = "By price"
        tableView.reloadData()
        expandSorting()
    }
    
    func sortEvents(){
        events = events.sorted(by: {$0.dateStart < $1.dateStart})
        if(spinnerView != nil){
            UIViewController.removeSpinner(spinner: spinnerView!)
        }
        tableView.reloadData()
    }
    
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.backgroundColor = UIColor(red: 191, green: 42, blue: 42)
        self.tabBarController?.tabBar.backgroundImage = UIImage(named: "1x1clear.png")
        self.tabBarController?.tabBar.items![0].image = UIImage(named: "event_icon")
        self.tabBarController?.tabBar.items![1].image = UIImage(named: "event_icon")
        self.tabBarController?.tabBar.items![2].image = UIImage(named: "ic_group_white")
        self.tabBarController?.tabBar.items![3].image = UIImage(named: "ic_person")
        
//        addTopViewColor(colorNavBar: true)
        
        Db = Database.database().reference()
        deleteAllData(entity: "Contact")
        deleteAllData(entity: "User")
        deleteAllData(entity: "MyContact")
        deleteAllData(entity: "CoreDataEvent")
        deleteAllData(entity: "Group")
        deleteAllData(entity: "Preset")
        deleteAllData(entity: "Message")
        deleteAllData(entity: "EventChat")
//        resetDefaults()
//        makePublicProfiles()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        tableView.delegate = self
        tableView.dataSource = self
        
        if(FBSDKAccessToken.current() == nil || Auth.auth().currentUser == nil){
            print("gotologin")
//            goToLogin()
        } else{
            print("notgotologin")
            theWidth = view.frame.width
            spinnerView = UIViewController.displaySpinner(onView: self.view)
            userId = Auth.auth().currentUser!.uid
            addSortingView()
            makeFloaty()
            getContactsData() // getEventsData and getgroup and getpreset are called after all contacts are gotten, getuserinfo is called asynchronous
        }

        refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    func presetExists(presetId : String)->NSManagedObject?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Preset")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "id") as! String == presetId}){
                    return result as? NSManagedObject;
                } else {
                    return nil
                }
            }
        } catch{
            return nil
        }
        return nil
    }
    
    func groupExists(groupId : String)->NSManagedObject?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Group")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "id") as! String == groupId}){
                    return result as? NSManagedObject;
                } else {
                    return nil
                }
            }
        } catch{
            return nil
        }
        return nil
    }

    func deleteOldLocalPresets(){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Preset")
        fetchRequest.returnsObjectsAsFaults = false
        
        do
        {
            let results = try context.fetch(fetchRequest)
            for managedObject in results
            {
                let theManagedObject = managedObject as! NSManagedObject
                let theId = theManagedObject.value(forKey: "id") as? String
                if(!self.myPresetIds.contains(theId!)){
                    context.delete(managedObject as! NSManagedObject)
                }
            }
        } catch {
            print("old local preset error")
        }
        
        do{
            try context.save()
        } catch{
            print("could not save old preset deletions into local storage")
        }
    }
    
    func getPresets(){
        let localRef : DatabaseReference! = Db?.child("Users").child(userId!).child("presets")
        self.allObserveRefs.append(localRef)
        localRef.observe(DataEventType.value, with: { (snapshot) in
            let enumerator = snapshot.children
            while let rest = enumerator.nextObject() as? DataSnapshot {
                if let dict = rest.value as? [String: Any]{
                    let presetName = dict["name"]! as! String
                    let presetId = dict["id"]! as! String
                    self.myPresetIds.append(presetId)
                    var contactsForPreset = NSSet()
                    if let contacts = dict["contacts"] as? [String : Any]{
                        for (_, value) in contacts{
                            if let contact = value as? [String : String]{
                                let myContact = self.contactExists(contactId: contact["id"]!, entityName: "MyContact", context: self.context)
                                if(myContact != nil){
                                    contactsForPreset = contactsForPreset.adding(myContact as Any) as NSSet
                                } else {
                                    print("contact not available for preset, this should be impossible")
                                }
                            }
                        }
                    }
                    let thePreset = self.presetExists(presetId: presetId)
                    if(thePreset != nil){
                        thePreset?.setValue(presetId, forKey: "id")
                        thePreset?.setValue(presetName, forKey: "name")
                        thePreset?.setValue(contactsForPreset, forKey: "contacts")
                    } else {
                        let newPreset = NSEntityDescription.insertNewObject(forEntityName: "Preset" , into: self.context)
                        newPreset.setValue(presetId, forKey: "id")
                        newPreset.setValue(presetName, forKey: "name")
                        newPreset.setValue(contactsForPreset, forKey: "contacts")
                    }
                    
                    do{
                        try self.context.save()
                    } catch{
                        print("could not save event into local storage")
                    }
                }
            }
            self.deleteOldLocalPresets()
        })
    }
    
    func reloadEverything() {
        getContactsData()
    }
    
    @objc func refresh(sender:AnyObject) {
        if(Date().millisecondsSince1970 - lastRefresh > 30000 ){ // 30 seconds since last refresh
            getContactsData()
            lastRefresh = Date().millisecondsSince1970
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { // change 2 to desired number of seconds
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    @objc func addEvent(gesture: UIGestureRecognizer) {
        if(Reachability.isConnectedToNetwork()){
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "PickEventTypeID") as! PickEventTypeViewController
            let navController = UINavigationController(rootViewController: nextViewController)
            self.present(navController, animated: true, completion: nil)
        }
    }

    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return events.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mainPageListCellID") as! MyEventsCell
        cell.selectionStyle = .none
        cell.backgroundColor = UIColor.darkGray
        
        let event = events[indexPath.section]
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm"
        let dateString = formatter.string(from: event.dateStart)
        
        cell.dateLbl.text = dateString
        cell.maxPeopleLbl.text = "Max people: " + String(event.maxPeople)
        if(event.priceBegin == "-1"){
            cell.priceRangeLbl.text = "€ ?? - ??"
        } else{
            cell.priceRangeLbl.text = "€ " + String(event.priceBegin) + " - " + String(event.priceEnd)
        }
        cell.nameLbl.text = event.eventName
        cell.locationLbl.text = "at: " + event.locationName
        
        // get contact who made it info
        let contactObject = contactExists(contactId: event.creator, entityName: "Contact", context: self.context)
        let contactName = contactObject?.value(forKey: "name") as! String
        let contactPicChangedAmount = contactObject?.value(forKey: "picChangedAmount") as! String
        
        cell.madeByLbl.text = "by " + contactName
        
        getPicDataForUser(theUserId: event.creator, picChangedAmount: contactPicChangedAmount, completionHandler: {picData in
            cell.madeByImage.makePortrait()
            cell.madeByImage.image = UIImage(data: picData as Data)
            cell.madeByImage.clipsToBounds = true
        })
        

        cell.joinedImage1.makePortrait()
        cell.joinedImage1.layer.zPosition = 1
        cell.joinedImage1.image = UIImage(named: "profile_icon.png")
        
        cell.joinedImage2.makePortrait()
        cell.joinedImage2.layer.zPosition = 2
        cell.joinedImage2.image = UIImage(named: "profile_icon.png")
        
        cell.joinedImage3.makePortrait()
        cell.joinedImage3.layer.zPosition = 3
        cell.joinedImage3.image = UIImage(named: "profile_icon.png")
        
        if(event.joinedPeople.count > 0){
            getPicDataForUser(theUserId: event.joinedPeople[0].id!, picChangedAmount: event.joinedPeople[0].picChangedAmount!, completionHandler: {picData in
                cell.joinedImage1.image = UIImage(data: picData as Data)
            })
        }
        
        if(event.joinedPeople.count > 1){
            getPicDataForUser(theUserId: event.joinedPeople[1].id!, picChangedAmount: event.joinedPeople[1].picChangedAmount!, completionHandler: {picData in
                cell.joinedImage2.image = UIImage(data: picData as Data)
            })
        }
        
        if(event.joinedPeople.count > 2){
            getPicDataForUser(theUserId: event.joinedPeople[2].id!, picChangedAmount: event.joinedPeople[2].picChangedAmount!, completionHandler: {picData in
                cell.joinedImage3.image = UIImage(data: picData as Data)
            })
        }
        
        cell.andMoreImage.image = UIImage(named: "plus_icon.png")
        cell.andMoreImage.layer.borderWidth = 1.0
        cell.andMoreImage.layer.masksToBounds = false
        cell.andMoreImage.layer.borderColor = UIColor.white.cgColor
        cell.andMoreImage.layer.cornerRadius = cell.madeByImage.frame.size.width / 2
        cell.andMoreImage.clipsToBounds = true
        cell.andMoreImage.layer.zPosition = 4
        
        cell.dateLbl.textColor = UIColor.white
        cell.maxPeopleLbl.textColor = UIColor.white
        cell.priceRangeLbl.textColor = UIColor.white
        cell.nameLbl.textColor = UIColor.white
        cell.locationLbl.textColor = UIColor.white
        cell.madeByLbl.textColor = UIColor.white
        
//        cell.backgroundView = UIImageView(image: UIImage(named: "profile_icon.png"))
        
        var nrOfTimesPicChanged = "0"
        if event.picChangedAmount != ""{
            nrOfTimesPicChanged = event.picChangedAmount
        }
        
        if(UserDefaults.standard.object(forKey: event.eventId + "/" + nrOfTimesPicChanged ) != nil){
            let picData = UserDefaults.standard.object(forKey: event.eventId + "/" + nrOfTimesPicChanged) as! NSData
            cell.backgroundView = UIImageView(image: UIImage(data: picData as Data))
            cell.backgroundView?.addBlurDark()
        } else {
            // remove old one from local database
            var amountChanged : Int = Int(nrOfTimesPicChanged)!
            amountChanged -= 1
            let lastEventChangedAmount = String(amountChanged)
            UserDefaults.standard.removeObject(forKey: event.eventId + "/" + lastEventChangedAmount)

            let eventPicRef = Storage.storage().reference(withPath: "pics/Events/" + event.eventId + "/eventPic.jpg")
            eventPicRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    print(error)
                    DispatchQueue.main.async {
                        let image = UIImage(named: "noImageAvailable.png")
                        let imageData : NSData = UIImagePNGRepresentation(image!)! as NSData
                        UserDefaults.standard.set(imageData, forKey: event.eventId + "/" + nrOfTimesPicChanged )
                        cell.backgroundView = UIImageView(image: UIImage(data: imageData as Data))
                        cell.backgroundView?.addBlurDark()
                    }
                } else {
                    let image = UIImage(data: data!)
                    let imageData : NSData = UIImagePNGRepresentation(image!)! as NSData
                    UserDefaults.standard.set(imageData, forKey: event.eventId + "/" + nrOfTimesPicChanged )
                    cell.backgroundView = UIImageView(image: UIImage(data: imageData as Data))
                    cell.backgroundView?.addBlurDark()
                }
            }
        }
        
        return cell
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OpenEventID") as! OpenEventViewControllerTry
//        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "openedEvent") as! OpenEvent
        nextViewController.theEvent = events[indexPath.section]
        nextViewController.reloadProtocol = self
        let navController = UINavigationController(rootViewController: nextViewController)
        self.present(navController, animated: true, completion: nil)
    }
    
//    func makePublicProfiles(){
//        Db?.child("Users").observeSingleEvent(of: .value, with: { (snapshot) in
//            if let dictOne = snapshot.value as? [String : Any]{
//                for (key, value) in dictOne{
//                    if let dict = value as? [String : Any]{ // this is a user
//                        let localUserId = key
//                        print(localUserId)
//                        var favCuis = ""
//                        if let a = dict["favCuis"] as? String{
//                            favCuis = a
//                        } else {
//                            favCuis = "no favourite cuisine"
//                        }
//
//                        var favRest = ""
//                        if let b = dict["favRest"] as? String{
//                            favRest = b
//                        } else {
//                            favRest = "no favourite restaurant"
//                        }
//
//                        var name = ""
//                        if let c = dict["name"] as? String{
//                            name = c
//                        } else {
//                            name = "no name"
//                        }
//
//                        var gender = ""
//                        if let d = dict["gender"] as? String{
//                            gender = d
//                        } else {
//                            gender = "no gender specified"
//                        }
//
//                        var picChangedAmount = ""
//                        if let d = dict["picChangedAmount"] as? String{
//                            picChangedAmount = d
//                        } else {
//                            picChangedAmount = "0"
//                        }
//
//                        let timeLastChanged = "\(Date().millisecondsSince1970)"
//
//                        self.Db?.child("publicProfiles").child(localUserId).child("favCuis").setValue(favCuis)
//                        self.Db?.child("publicProfiles").child(localUserId).child("favRest").setValue(favRest)
//                        self.Db?.child("publicProfiles").child(localUserId).child("name").setValue(name)
//                        self.Db?.child("publicProfiles").child(localUserId).child("gender").setValue(gender)
//                        self.Db?.child("publicProfiles").child(localUserId).child("picChangedAmount").setValue(picChangedAmount)
//                        self.Db?.child("publicProfiles").child(localUserId).child("timeLastChanged").setValue(timeLastChanged)
//
//
//                        self.Db?.child("Users").child(localUserId).child("timeLastChanged").setValue(timeLastChanged)
//                    }
//                }
//            }
//        })
//    }
    
}
