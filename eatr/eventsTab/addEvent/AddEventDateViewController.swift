//
//  AddEventDateViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 18/03/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit

class AddEventDateViewController: UIViewController {
    var pickedRestaurant : RestaurantItem?
    var textPickedRest : String?
    var typeOfEvent : String!
    var image : UIImage?
    var rating : String!
    
    var topImage : UIImageView!
    
    @IBOutlet weak var datePickerOutlet: UIDatePicker!
    
    func setUpAllViews(){
        let theWidth = view.frame.width
        let topImageHeight = CGFloat(150)
        
        topImage = UIImageView(frame: CGRect(x: 0, y: 20, width: theWidth, height: topImageHeight))
        topImage.image = image
        topImage.contentMode = .scaleToFill
        topImage.addShadow()
        
        let placeNextButtonAt = view.frame.height - 40
        let nextButtonView = UIView(frame: CGRect(x: 0, y: placeNextButtonAt, width: theWidth, height: 40))
        nextButtonView.backgroundColor = UIColor.lightGray
        let nextButton = UIButton(frame: CGRect(x: theWidth/2, y: 0, width: theWidth/2, height: 40))
        nextButton.setTitleColor(UIColor.black, for: .normal)
        nextButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: theWidth/4, bottom: 0, right: 0)
        nextButton.addTarget(self, action: #selector(goToNext), for: .touchUpInside)
        nextButton.backgroundColor = UIColor.lightGray
        nextButton.setTitle("NEXT >", for: .normal)
        
        let discardButton = UIButton(frame: CGRect(x: 0, y: 0, width: theWidth/2, height: 40))
        discardButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: theWidth/4)
        discardButton.setTitle("< Discard", for: .normal)
        discardButton.setTitleColor(UIColor.red, for: .normal)
        discardButton.backgroundColor = UIColor.lightGray
        discardButton.addTarget(self, action: #selector(goBackToMainPage), for: .touchUpInside)
        
        let infoLabel = UILabel(frame: CGRect(x: 50, y: 230, width: theWidth-100, height: 20))
        infoLabel.textAlignment = .center
        infoLabel.text = "When will the event start?"
        infoLabel.backgroundColor = UIColor.white
        
        datePickerOutlet.frame = CGRect(x: 25, y: 230, width: theWidth-50, height: view.frame.height - 250 - 40 - 40)
        datePickerOutlet.backgroundColor = UIColor.white
        datePickerOutlet.addShadow()
        
        nextButtonView.addSubview(discardButton)
        nextButtonView.addSubview(nextButton)
        view.addSubview(nextButtonView)
        view.addSubview(topImage)
        view.addSubview(infoLabel)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpAllViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.backgroundColor = UIColor(white: 1, alpha: 0.0)
    }
    
    @objc func goBackToMainPage(){
        let alert = UIAlertController(title: "", message: "Are you sure you want cancel? all your changes will be lost.", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
        })
        let cancel = UIAlertAction(title: "No", style: .destructive, handler: { (action) -> Void in })
        
        alert.addAction(action1)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    @objc func goToNext(){
        let pickedDate = datePickerOutlet.date
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PriceAndMaxPeopleID") as! SetPriceAndGroupOrNotViewController
        vc.typeOfEvent = typeOfEvent
        vc.pickedRestaurant = pickedRestaurant
        vc.pickedDate = pickedDate
        vc.image = image
        vc.rating = rating
        vc.textPickedRest = textPickedRest
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
