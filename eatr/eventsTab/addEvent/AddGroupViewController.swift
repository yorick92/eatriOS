//
//  AddGroupViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 31/03/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import Firebase
import CoreData

class AddGroupViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    var groups = [localGroupStruct]()
    var groupMembers = [localContactStruct]()
    var chosenGroup : localGroupStruct!
    var pickedRestaurant : RestaurantItem?
    var textPickedRest : String?
    var typeOfEvent : String!
    var userId : String?
    var priceMin: Int?
    var priceMax: Int?
    var maxPeople: Int?
    var pickedDate : Date?
    var image : UIImage?
    var context : NSManagedObjectContext!
    var rating : String!
    
    var nextButton : UIButton!
    
    @IBOutlet weak var groupsList: UITableView!
    @IBOutlet weak var memberList: UITableView!
    
    
    func getGroups(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Group")
        request.returnsObjectsAsFaults = false
        
        self.groups = [localGroupStruct]()
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                for result in results as! [NSManagedObject]{
                    
                    let groupId = result.value(forKey: "id") as! String
                    let groupName = result.value(forKey: "name") as! String
                    let admin = result.value(forKey: "admin") as! String
                    let contactsSet = result.value(forKey: "contacts") as! NSSet
                    
                    var contacts = [localContactStruct]()
                    for personAny in contactsSet.allObjects{
                        if let person = personAny as? NSManagedObject{
                            let id = person.value(forKey: "userId") as! String
                            let name = person.value(forKey: "name") as! String
                            let favCuis = person.value(forKey: "favCuis") as! String
                            let favRest = person.value(forKey: "favRest") as! String
                            let gender = person.value(forKey: "gender") as! String
                            let picChangedAmount = person.value(forKey: "picChangedAmount") as! String
                            let timeLastChanged = person.value(forKey: "timeLastChanged") as! String
                            
                            contacts.append(localContactStruct(id: id, name: name, favCuis: favCuis, favRest: favRest, gender: gender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
                        }
                    }
                    
                    self.groups.append(localGroupStruct(name: groupName, id: groupId, admin: admin, contacts: contacts))
                }
            }
        } catch{
            print("error")
        }
        self.groupsList.reloadData()
    }
    
    func setUpVariables(){
        userId = Auth.auth().currentUser!.uid
    }
    
    func addAllViews(){
        let theWidth = view.frame.width
        
        let groupListView = UIView(frame: CGRect(x: 25, y: 20 + (navigationController?.navigationBar.frame.height)! + 20, width: theWidth-50, height: 150))
        groupsList.frame = CGRect(x: 0, y: 0, width: theWidth-50, height: 150)
        groupsList.removeFromSuperview()
        groupListView.addSubview(groupsList)
        groupListView.addShadow()
        
        let memberListView = UIView(frame: CGRect(x: 5, y: groupListView.frame.origin.y + 180, width: theWidth-10, height: view.frame.height - groupListView.frame.origin.y - groupListView.frame.height - 90))
        memberList.frame = CGRect(x: 0, y: 0, width: theWidth-10, height: view.frame.height - groupListView.frame.origin.y - groupListView.frame.height - 90)
        memberList.removeFromSuperview()
        memberListView.addSubview(memberList)
        memberListView.addShadow()
        
        let placeNextButtonAt = view.frame.height - 40
        let nextButtonView = UIView(frame: CGRect(x: 0, y: placeNextButtonAt, width: theWidth, height: 40))
        nextButtonView.backgroundColor = UIColor.lightGray
        nextButton = UIButton(frame: CGRect(x: theWidth/2, y: 0, width: theWidth/2, height: 40))
        nextButton.setTitleColor(UIColor.black, for: .normal)
        nextButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: theWidth/4, bottom: 0, right: 0)
        nextButton.addTarget(self, action: #selector(goToNext), for: .touchUpInside)
        nextButton.backgroundColor = UIColor.lightGray
        nextButton.setTitle("NEXT >", for: .normal)
        nextButton.isHidden = true
        
        let discardButton = UIButton(frame: CGRect(x: 0, y: 0, width: theWidth/2, height: 40))
        discardButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: theWidth/4)
        discardButton.setTitle("< Discard", for: .normal)
        discardButton.setTitleColor(UIColor.red, for: .normal)
        discardButton.backgroundColor = UIColor.lightGray
        discardButton.addTarget(self, action: #selector(goBackToMainPage), for: .touchUpInside)
        
        nextButtonView.addSubview(nextButton)
        nextButtonView.addSubview(discardButton)
        view.addSubview(groupListView)
        view.addSubview(memberListView)
        view.addSubview(nextButtonView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Add group"
        addTopViewColor(colorNavBar: true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        
        addAllViews()
        
        setUpVariables()
        getGroups()
    }
    
    @objc func goToNext(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddEventOverviewID") as! AddEventOverviewViewController
        vc.pickedRestaurant = pickedRestaurant
        vc.pickedDate = pickedDate
        vc.priceMin = priceMin
        vc.priceMax = priceMax
        vc.maxPeople = maxPeople
        vc.image = image
        vc.textPickedRest = textPickedRest
        vc.group = chosenGroup
        vc.typeOfEvent = typeOfEvent
        vc.rating = rating
        vc.groupOrNot = "group"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func goBackToMainPage(){
        let alert = UIAlertController(title: "", message: "Are you sure you want cancel? all your changes will be lost.", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
        })
        let cancel = UIAlertAction(title: "No", style: .destructive, handler: { (action) -> Void in })
        
        alert.addAction(action1)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(tableView == groupsList){
            return groups.count
        } else{
            return groupMembers.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(tableView == groupsList){
            let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "groupCell")
            cell.textLabel?.text = groups[indexPath.row].name
            return cell
        } else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "contactCellID") as! ContactCellTableViewCell
            let contact = groupMembers[indexPath.row]
            cell.nameLbl.text = contact.name
            cell.cuisineLbl.text = "Cuisine: " + groupMembers[indexPath.row].favCuis!
            cell.profilePic.image = UIImage(named: "profile_icon.png")
            getPicDataForUser(theUserId: contact.id!, picChangedAmount: contact.picChangedAmount!, completionHandler: {picData in
                cell.profilePic.image = UIImage(data: picData as Data)
            })
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(tableView == groupsList){
            self.chosenGroup = self.groups[indexPath.row]
            self.groupMembers = chosenGroup.contacts!
            self.nextButton.isEnabled = true
            self.maxPeople = self.groupMembers.count
            self.memberList.reloadData()
            nextButton.isHidden = false
        } else{
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileOtherID") as! OpenOtherProfileViewController
            nextViewController.theContact = groupMembers[indexPath.row]
            let navController = UINavigationController(rootViewController: nextViewController)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    
}
