//
//  AddEventOverviewViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 18/03/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseStorage
import Firebase
import CoreData

class AddEventOverviewViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, UIScrollViewDelegate  {
    var ref:DatabaseReference?
    var usersReference : DatabaseReference?
    var groupsReference : DatabaseReference?
    var storageRef : StorageReference?
    var allFriendsPicked : [localContactStruct]?
    var pickedRestaurant : RestaurantItem?
    var group : localGroupStruct?
    var textPickedRest : String?
    var userId : String?
    var groupOrNot : String?
    var typeOfEvent : String!
    var priceMin: Int?
    var priceMax: Int?
    var maxPeople: Int?
    var pickedDate : Date!
    var image : UIImage?
    var rating : String!
    var context : NSManagedObjectContext!
    
    var latitude : String!
    var longitude : String!
    
    var theScrollHeight : CGFloat!
    var theWidth : CGFloat!
    var theViewHeight : CGFloat!
    
    var theScrollView : UIScrollView!
    var eventPic : UIImageView!
    var viewForBackground : UIView!
    
    var LocationButtonsView : UIView!
    var showLocationImageView : UIImageView!
    var showLocationInfoView : UITextView!
    
    var dateButtonsView : UIView!
    var dateInfoView : UITextView!
    
    var costButtonsView : UIView!
    var costInfoView : UITextView!
    
    var creatorPartView : UIView!
    var showCreatorImageView : UIImageView!
    var creatorInfoView : UITextView!
    
    var eventInfoView : UIView!
    
    var remarksTextView : UITextView!
    
    var attendeesView : UIView!
    var collexpAttendeesButton : UIButton!
    
    var invitedContactsExpanded = false
    var invitedContactViews  = [UIView]()
    
    var eventNameLabel : UITextField!
    
    var makeEventButton : UIButton!
    
//    @IBOutlet weak var groupOrContactIntro: UILabel!
//    @IBOutlet weak var groupNameOutlet: UILabel!
//    @IBOutlet weak var restaurantInfo: UILabel!
//    @IBOutlet weak var dateOutlet: UILabel!
//    @IBOutlet weak var priceRangeOutlet: UILabel!
//    @IBOutlet weak var maxPeopleOutlet: UILabel!
//    @IBOutlet weak var eventNameOutlet: UITextField!
//    @IBOutlet weak var eventRemarkOutlet: UITextView!
//    @IBOutlet weak var createEventButton: UIButton!
//    @IBOutlet weak var invitedPeopleList: UITableView!
//    @IBOutlet weak var restaurantImage: UIImageView!
    
    
    func setUpDatabaseReferences(){
        storageRef = Storage.storage().reference()
        ref = Database.database().reference()
        usersReference = Database.database().reference().child("Users")
        groupsReference = Database.database().reference().child("groups")
    }
    
    func getGroupContacts(){
        if(self.allFriendsPicked == nil && group?.contacts != nil){
            allFriendsPicked = group?.contacts!
        }
        
    }
//
//    func setUpOutlets(){
//        createEventButton.isEnabled = false
//        restaurantImage.image = image
//        restaurantInfo.numberOfLines = 0
//        restaurantInfo.text = textPickedRest
//
//        let formatter = DateFormatter()
//        formatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
//        let dateString = formatter.string(from: pickedDate!)
//        dateOutlet.numberOfLines = 0
//        dateOutlet.text = "This event will take place on:\n" + dateString
//
//        if(priceMin != -1){
//            priceRangeOutlet.text = "The price of this event is between " + String(priceMin!) + " and " + String(priceMax!) + " euro"
//        } else{
//            priceRangeOutlet.text = "The price of this event is unknown"
//        }
//        if(allFriendsPicked == nil){
//            groupOrNot = "group"
//            getGroupContacts()
//        } else{
//            groupOrNot = "not"
//            groupNameOutlet.text =  ""
//            groupOrContactIntro.text = "You will invite the following people:"
//        }
//        maxPeopleOutlet.text = "A maximum of " + String(maxPeople!) +  " people can join this event"
//    }
//
    /*** SET UP VIEWS ***/
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let originalY = ((self.navigationController?.navigationBar.frame.height)! + 20)
        if((scrollView.contentOffset.y + 64) < originalY){
            self.eventPic.frame.origin.y = originalY - (scrollView.contentOffset.y + 64)
        }
    }
    
    func setUpParentViewsAndImage(){
        theWidth = view.frame.width
        theViewHeight = view.frame.height
        
        theScrollHeight = 830
        theScrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: theWidth, height: theViewHeight))
        theScrollView.contentSize = CGSize(width: theWidth, height: theScrollHeight)
        theScrollView.delegate = self
        view.addSubview(theScrollView)
        
        let eventPicHeight = CGFloat(200.0)
        eventPic = UIImageView(frame: CGRect(x: 0, y: 0, width: theWidth, height: eventPicHeight))
        eventPic.image = image
        view.addSubview(eventPic)
        view.sendSubview(toBack: eventPic)
        
        viewForBackground = UIView(frame: CGRect(x: 0, y: eventPicHeight, width: theWidth, height: theScrollHeight))
        viewForBackground.backgroundColor = UIColor.white
        theScrollView.addSubview(viewForBackground)
    }
    
    func makeLocationButtons(eventInfoView : UIView){
        let totalWidth = viewForBackground.frame.width
        
        // add the full width view
        LocationButtonsView = UIView(frame: CGRect(x: 0, y: 0, width: totalWidth, height: 80))
        LocationButtonsView.backgroundColor = UIColor.white
        LocationButtonsView.layer.borderColor = UIColor.lightGray.cgColor
        LocationButtonsView.layer.borderWidth = 1
        
        let theViewsHeight = LocationButtonsView.frame.height
        // add the get route button
        let getRouteButton = UIButton(frame: CGRect(x: 0.0, y: 0.0, width: totalWidth/6, height: theViewsHeight))
        getRouteButton.addTarget(self, action: #selector(getRoute), for: .touchUpInside)
        getRouteButton.setImage(UIImage(named: "pin_icon.png"), for: .normal)
        getRouteButton.imageView?.contentMode = .scaleAspectFit
        getRouteButton.imageEdgeInsets = UIEdgeInsets(top: 30, left: 0, bottom: 30, right: 0)
        LocationButtonsView.addSubview(getRouteButton)
        
        // add the location image view
        showLocationImageView = UIImageView(frame: CGRect(x: totalWidth/6, y: (80-totalWidth/6)/2, width: totalWidth/6, height: totalWidth/6))
        showLocationImageView.image = self.eventPic.image
        showLocationImageView.contentMode = .scaleAspectFill
        showLocationImageView.makePortrait()
        LocationButtonsView.addSubview(showLocationImageView)
        
        // add the location info view
        showLocationInfoView = UITextView(frame: CGRect(x: totalWidth/3, y: 0, width: totalWidth/2, height: theViewsHeight))
        showLocationInfoView.text = (pickedRestaurant?.name)! + " \n" + "stars strimg" + "\nopen now?" + "\ndistance in km"
        showLocationInfoView.font = UIFont.systemFont(ofSize: 14)
        showLocationInfoView.isEditable = false
        showLocationInfoView.isScrollEnabled = false
        LocationButtonsView.addSubview(showLocationInfoView)
        
        eventInfoView.addSubview(LocationButtonsView)
    }
    
    func makeDateButtons(eventInfoView : UIView){
        let totalWidth = viewForBackground.frame.width
        
        dateButtonsView = UIView(frame: CGRect(x: 0.0, y: 80.0, width: totalWidth, height: 60.0))
        dateButtonsView.backgroundColor = UIColor.white
        dateButtonsView.layer.borderColor = UIColor.lightGray.cgColor
        dateButtonsView.layer.borderWidth = 1
        
        let theViewsHeight = dateButtonsView.frame.height
        
        // add icon
        let dateView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: totalWidth/6, height: theViewsHeight))
        dateView.backgroundColor = UIColor.white
        let dateIconImageView = UIImageView(frame: CGRect(x: (totalWidth/6-20)/2, y: 20, width: 20, height: 20))
        dateIconImageView.image = UIImage(named: "clock_icon.png")
        dateIconImageView.contentMode = .scaleAspectFill
        dateView.addSubview(dateIconImageView)
        dateButtonsView.addSubview(dateView)
        
        // add date text
        dateInfoView = UITextView(frame: CGRect(x: totalWidth/6, y: 0, width: (totalWidth/6)*4, height: theViewsHeight))
        dateInfoView.font = UIFont.systemFont(ofSize: 14)
        dateInfoView.isEditable = false
        dateInfoView.isScrollEnabled = false
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let dateString = formatter.string(from: pickedDate)
        formatter.dateFormat = "HH:mm"
        let timeString = formatter.string(from: pickedDate)
        dateInfoView.text = dateString + "\n" + timeString
        dateButtonsView.addSubview(dateInfoView)
        
        eventInfoView.addSubview(dateButtonsView)
    }
    
    func makeCostsButtons(eventInfoView : UIView){
        let totalWidth = viewForBackground.frame.width
        
        costButtonsView = UIView(frame: CGRect(x: 0.0, y: 140.0, width: totalWidth, height: 60.0))
        costButtonsView.backgroundColor = UIColor.white
        costButtonsView.layer.borderColor = UIColor.lightGray.cgColor
        costButtonsView.layer.borderWidth = 1
        costButtonsView.isUserInteractionEnabled = true
        
        let theViewsHeight = costButtonsView.frame.height
        
        // add icon
        let costView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: totalWidth/6, height: theViewsHeight))
        costView.backgroundColor = UIColor.white
        let dateIconImageView = UIImageView(frame: CGRect(x: (totalWidth/6-20)/2, y: 20, width: 20, height: 20))
        dateIconImageView.image = UIImage(named: "euro_icon.png")
        dateIconImageView.contentMode = .scaleAspectFill
        costView.addSubview(dateIconImageView)
        costButtonsView.addSubview(costView)
        
        // add cost text
        costInfoView = UITextView(frame: CGRect(x: totalWidth/6, y: 0, width: (totalWidth/6)*4, height: theViewsHeight))
        costInfoView.font = UIFont.systemFont(ofSize: 14)
        costInfoView.isEditable = false
        costInfoView.isScrollEnabled = false
        if(String(priceMin!) != "-1"){
            costInfoView.text = "€ " + String(priceMin!) + " - " + String(priceMax!) + "\nEstimated costs"
        } else{
            costInfoView.text = "€ ?? - ?? \nEstimated costs"
        }
        costButtonsView.addSubview(costInfoView)
        
        eventInfoView.addSubview(costButtonsView)
    }
    
    func makeCreatorPart(eventInfoView : UIView){
        let totalWidth = viewForBackground.frame.width
        
        creatorPartView = UIView(frame: CGRect(x: 0.0, y: 200.0, width: totalWidth, height: 60.0))
        creatorPartView.backgroundColor = UIColor.white
        creatorPartView.layer.borderColor = UIColor.lightGray.cgColor
        creatorPartView.layer.borderWidth = 1
        
        let theViewsHeight = creatorPartView.frame.height
        
        // add icon
        let creatorView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: totalWidth/6, height: theViewsHeight))
        creatorView.backgroundColor = UIColor.white
        let creatorImageView = UIImageView(frame: CGRect(x: (totalWidth/6-20)/2, y: 20, width: 20, height: 20))
        creatorImageView.image = UIImage(named: "profile_icon.png")
        creatorImageView.contentMode = .scaleAspectFill
        creatorView.addSubview(creatorImageView)
        creatorPartView.addSubview(creatorView)
        
        // add the creator image view
        showCreatorImageView = UIImageView(frame: CGRect(x: totalWidth/6, y: 10, width: theViewsHeight-20, height: theViewsHeight-20))
        showCreatorImageView.image = UIImage(named: "profile_icon.png")
        
        let contactObject = contactExists(contactId: self.userId!, entityName: "Contact", context: self.context)
        let contactName = contactObject?.value(forKey: "name") as! String
        let contactPicChangedAmount = contactObject?.value(forKey: "picChangedAmount") as! String
        getPicDataForUser(theUserId: self.userId!, picChangedAmount: contactPicChangedAmount, completionHandler: {picData in
            self.showCreatorImageView.image = UIImage(data: picData as Data)
        })
        showCreatorImageView.contentMode = .scaleAspectFill
        showCreatorImageView.makePortrait()
        creatorPartView.addSubview(showCreatorImageView)
        
        // add info text
        creatorInfoView = UITextView(frame: CGRect(x: totalWidth/3, y: 0, width: (totalWidth/6)*4, height: theViewsHeight))
        creatorInfoView.font = UIFont.systemFont(ofSize: 14)
        creatorInfoView.isEditable = false
        creatorInfoView.isScrollEnabled = false
        creatorInfoView.text = "Created by " + contactName
        creatorPartView.addSubview(creatorInfoView)
        
        eventInfoView.addSubview(creatorPartView)
    }
    
    func makeMaxPeoplePart(eventInfoView: UIView){
        let totalWidth = viewForBackground.frame.width
        
        let maxPeoplePartView = UIView(frame: CGRect(x: 0.0, y: 260.0, width: totalWidth, height: 60.0))
        maxPeoplePartView.backgroundColor = UIColor.white
        maxPeoplePartView.layer.borderColor = UIColor.lightGray.cgColor
        maxPeoplePartView.layer.borderWidth = 1
        
        let theViewsHeight = maxPeoplePartView.frame.height
        
        // add icon
        let creatorView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: totalWidth/6, height: theViewsHeight))
        creatorView.backgroundColor = UIColor.white
        let creatorImageView = UIImageView(frame: CGRect(x: (totalWidth/6-30)/2, y: 15, width: 30, height: 30))
        creatorImageView.image = UIImage(named: "group_icon.png")
        creatorImageView.contentMode = .scaleAspectFill
        creatorView.addSubview(creatorImageView)
        maxPeoplePartView.addSubview(creatorView)
        
        // add info text
        let maxPeopleInfoView = UITextView(frame: CGRect(x: totalWidth/3, y: 0, width: (totalWidth/6)*4, height: theViewsHeight))
        maxPeopleInfoView.font = UIFont.systemFont(ofSize: 14)
        maxPeopleInfoView.isEditable = false
        maxPeopleInfoView.isScrollEnabled = false
        maxPeopleInfoView.text = "Max attendees: " + String(maxPeople!)
        maxPeoplePartView.addSubview(maxPeopleInfoView)
        
        eventInfoView.addSubview(maxPeoplePartView)
    }
    
    func setEventInfoButtons(){
        if(eventInfoView != nil){
            eventInfoView.removeFromSuperview()
        }
        
        let totalWidth = viewForBackground.frame.width
        let xValue = viewForBackground.frame.origin.x
        let yValue = viewForBackground.frame.origin.y
        let theHeight = CGFloat(320)
        eventInfoView = UIView(frame: CGRect(x: xValue, y: yValue, width: totalWidth, height: theHeight))
        eventInfoView.backgroundColor = UIColor.white
        eventInfoView.addShadow()
        
        theScrollView.addSubview(eventInfoView)
        
        makeLocationButtons(eventInfoView: eventInfoView)
        makeDateButtons(eventInfoView: eventInfoView)
        makeCostsButtons(eventInfoView: eventInfoView)
        makeCreatorPart(eventInfoView: eventInfoView)
        makeMaxPeoplePart(eventInfoView: eventInfoView)
    }
    
    func addRemarksField(){
        let totalWidth = viewForBackground.frame.width
        let xValue = viewForBackground.frame.origin.x
        let yValue = viewForBackground.frame.origin.y
        let theHeight = CGFloat(100)
        let remarksView = UIView(frame: CGRect(x: xValue, y: yValue + 340, width: totalWidth, height: theHeight))
        remarksView.backgroundColor = UIColor.white
        remarksView.addShadow()
        
        
        let localLabel = UILabel(frame: CGRect(x: 10, y: 0, width: totalWidth - 10, height: 15))
        localLabel.text = "Remarks"
        localLabel.textColor = UIColor.red
        localLabel.font = UIFont.systemFont(ofSize: 12)
        remarksView.addSubview(localLabel)
        
        remarksTextView = UITextView(frame: CGRect(x: 10, y: 15, width: totalWidth - 10, height: 85))
        remarksView.addSubview(remarksTextView)
        
        theScrollView.addSubview(remarksView)
    }
    
    func makeEventNamePart(){
        let totalWidth = viewForBackground.frame.width
        let xValue = viewForBackground.frame.origin.x
        let yValue = viewForBackground.frame.origin.y
        let height = CGFloat(30)
        let eventNameView = UIView(frame: CGRect(x: xValue, y: yValue + 460, width: totalWidth, height: height))
        eventNameView.backgroundColor = UIColor.white
        eventNameView.addShadow()
        eventNameLabel = UITextField(frame: CGRect(x: 5, y: 0, width: totalWidth - 10, height: height))
        eventNameLabel.text = "fill in name for event"
        eventNameLabel.addTarget(self, action: #selector(changeEventName), for: .editingChanged)
        eventNameView.addSubview(eventNameLabel)
        theScrollView.addSubview(eventNameView)
    }
    
    func makeAttendeesPart(){
        if(attendeesView != nil){
            attendeesView.removeFromSuperview()
        }
        
        let totalWidth = viewForBackground.frame.width
        let xValue = viewForBackground.frame.origin.x
        let yValue = viewForBackground.frame.origin.y
        let height = CGFloat(30)
        attendeesView = UIView(frame: CGRect(x: xValue, y: yValue + 510, width: totalWidth, height: height))
        attendeesView.backgroundColor = UIColor.white
        attendeesView.addShadow()
        
        let localLabel = UILabel(frame: CGRect(x: 10, y: 0, width: totalWidth/2 - 10, height: height))
        localLabel.text = "Invited contacts"
        localLabel.textColor = UIColor.red
        localLabel.font = UIFont.systemFont(ofSize: 12)
        attendeesView.addSubview(localLabel)
        
        collexpAttendeesButton = UIButton(frame: CGRect(x: totalWidth/2, y: 0, width: totalWidth/2, height: height))
        collexpAttendeesButton.addTarget(self, action: #selector(expandInvitedContacts), for: .touchUpInside)
        collexpAttendeesButton.setImage(UIImage(named: "expand_icon.png"), for: .normal)
        collexpAttendeesButton.imageView?.contentMode = .scaleAspectFit
        collexpAttendeesButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -totalWidth/3)
        attendeesView.addSubview(collexpAttendeesButton)
        
        theScrollView.addSubview(attendeesView)
    }
    
    func makeCreateButtonPart(){
        let totalWidth = viewForBackground.frame.width
        let xValue = viewForBackground.frame.origin.x
        let yValue = viewForBackground.frame.origin.y
        let height = CGFloat(50)
        makeEventButton = UIButton(frame: CGRect(x: xValue, y: yValue + 560, width: totalWidth, height: height))
        makeEventButton.backgroundColor = UIColor.white
        makeEventButton.addTarget(self, action: #selector(createEvent), for: .touchUpInside)
        makeEventButton.addShadowBottom()
        let icon = UIImage(named: "pencil_icon.png")
        makeEventButton.setImage(icon, for: .normal)
        makeEventButton.imageView?.contentMode = .scaleAspectFit
        let toLeft = makeEventButton.frame.width/2
        makeEventButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: -toLeft, bottom: 10, right: 0)
        makeEventButton.setTitleColor(UIColor.black, for: .normal)
        makeEventButton.setTitle("Create event", for: .normal)
        makeEventButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: (-theWidth/2)*1.3 , bottom: 0, right: 0)
        
        theScrollView.addSubview(makeEventButton)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Overview"
        userId = Auth.auth().currentUser!.uid
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        
//        print(self.groupOrNot)
        
        setUpParentViewsAndImage()
        setEventInfoButtons()
        addRemarksField()
        makeEventNamePart()
        makeAttendeesPart()
        makeCreateButtonPart()
        
        addTopViewColor(colorNavBar: true)
        setUpDatabaseReferences()
        
        getGroupContacts();

//        setUpOutlets()
        self.navigationController?.hidesBarsOnSwipe = true
    }
    
    func getDirections(directionsURL : String){
        guard let url = URL(string: directionsURL) else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @objc func expandInvitedContacts(){
        var imageName = ""
        var valueChanged : CGFloat = 0
        let nrOfAttending = allFriendsPicked!.count
        if (invitedContactsExpanded){
            invitedContactsExpanded = false
            imageName = "expand_icon.png"
            valueChanged = CGFloat(nrOfAttending * 50 * -1)
            
            for view in invitedContactViews{
                view.removeFromSuperview()
            }
            
        } else {
            invitedContactsExpanded = true
            imageName = "collapse_icon.png"
            valueChanged = CGFloat(nrOfAttending * 50)
            
            var counter = 0;
            for friend in allFriendsPicked! {
                let localFriendView = UIView(frame: CGRect(x: 0, y: CGFloat(30 + counter * 50), width: theWidth, height: 50))
                attendeesView.addSubview(localFriendView)
                
                let friendImageView = UIImageView(frame: CGRect(x: 5, y: 5, width: 40, height: 40))
                friendImageView.image = UIImage(named: "profile_icon.png")
                
                let contactObject = contactExists(contactId: friend.id!, entityName: "Contact", context: self.context)
                let contactName = contactObject?.value(forKey: "name") as! String
                let contactFavCuis = contactObject?.value(forKey: "favCuis") as! String
                let contactPicChangedAmount = contactObject?.value(forKey: "picChangedAmount") as! String
                getPicDataForUser(theUserId: friend.id!, picChangedAmount: contactPicChangedAmount, completionHandler: {picData in
                    friendImageView.image = UIImage(data: picData as Data)
                })
                
                friendImageView.contentMode = .scaleAspectFill
                friendImageView.makePortrait()
                
                let nameLabel = UILabel(frame: CGRect(x: 65, y: 5, width: theWidth - 40, height: 25))
                nameLabel.font = UIFont.systemFont(ofSize: 16)
                nameLabel.text = contactName
                
                let favCuisLabel = UILabel(frame: CGRect(x: 65, y: 25, width: theWidth - 40, height: 25))
                favCuisLabel.font = UIFont.systemFont(ofSize: 12)
                favCuisLabel.text = "Pref. cuisine: " + contactFavCuis
                
                localFriendView.addSubview(friendImageView)
                localFriendView.addSubview(nameLabel)
                localFriendView.addSubview(favCuisLabel)
                attendeesView.addSubview(localFriendView)
                invitedContactViews.append(localFriendView)
                
                let tappy = MyTapGesture(target: self, action: #selector(self.tappedInvitedContacts))
                localFriendView.addGestureRecognizer(tappy)
                tappy.userId = friend.id!
                
                counter += 1
            }
        }
        
        
        collexpAttendeesButton.setImage(UIImage(named: imageName), for: .normal)
        attendeesView.frame = CGRect(x: attendeesView.frame.origin.x, y: attendeesView.frame.origin.y, width: CGFloat(theWidth), height: attendeesView.frame.height + valueChanged)
        viewForBackground.frame = CGRect(x: viewForBackground.frame.origin.x, y: viewForBackground.frame.origin.y, width: viewForBackground.frame.width, height: viewForBackground.frame.height + valueChanged)
        theScrollHeight = theScrollHeight + CGFloat(valueChanged)
        theScrollView.contentSize = CGSize(width: theWidth, height: theScrollHeight)
        makeEventButton.frame = CGRect(x: makeEventButton.frame.origin.x, y: makeEventButton.frame.origin.y + valueChanged, width: makeEventButton.frame.width, height: makeEventButton.frame.height)
    }
    
    @objc func tappedInvitedContacts(sender : MyTapGesture){
        let person = allFriendsPicked!.first(where: {$0.id == sender.userId})!
        let alert = UIAlertController(title: "", message: "view " + person.name! + "'s profile?" , preferredStyle: .alert)
        
        let action2 = UIAlertAction(title: "View Profile", style: .default, handler: { (action) -> Void in
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileOtherID") as! OpenOtherProfileViewController
            nextViewController.theContact = person
            let navController = UINavigationController(rootViewController: nextViewController)
            self.present(navController, animated: true, completion: nil)
        })
        alert.addAction(action2)
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        
        
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    @objc func getRoute(){
        self.latitude = pickedRestaurant?.lat
        self.longitude = pickedRestaurant?.lng
        
        var directionsURL = ""
        let alert = UIAlertController(title: "mode of transportation", message: "What way will you be travelling?", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Driving", style: .default, handler: { (action) -> Void in
            directionsURL = "https://www.google.com/maps/?daddr=" + self.latitude! + "," + self.longitude! + "&dirflg=d"
            self.getDirections(directionsURL: directionsURL)
        })
        let action2 = UIAlertAction(title: "Walking", style: .default, handler: { (action) -> Void in
            directionsURL = "https://www.google.com/maps/?daddr=" + self.latitude! + "," + self.longitude! + "&dirflg=w"
            self.getDirections(directionsURL: directionsURL)
        })
        let action3 = UIAlertAction(title: "Bicycling", style: .default, handler: { (action) -> Void in
            directionsURL = "https://www.google.com/maps/?daddr=" + self.latitude! + "," + self.longitude! + "&dirflg=b"
            self.getDirections(directionsURL: directionsURL)
        })
        let action4 = UIAlertAction(title: "Public transport", style: .default, handler: { (action) -> Void in
            directionsURL = "https://www.google.com/maps/?daddr=" + self.latitude! + "," + self.longitude! + "&dirflg=r"
            self.getDirections(directionsURL: directionsURL)
        })
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        
        alert.addAction(action1)
        alert.addAction(action2)
        alert.addAction(action3)
        alert.addAction(action4)
        alert.addAction(cancel)
        self.present(alert, animated: true, completion: nil)
    }
    
    func goBackToMainPage(){
        let alert = UIAlertController(title: "", message: "Are you sure you want cancel? all your changes will be lost.", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
        })
        let cancel = UIAlertAction(title: "No", style: .destructive, handler: { (action) -> Void in })
        
        alert.addAction(action1)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
//    @IBAction func cancelButton(_ sender: Any) {
//        goBackToMainPage()
//    }
    
    @objc func changeEventName(){
        if(eventNameLabel.text?.isEmpty)!{
            makeEventButton.isEnabled = false
        } else {
            makeEventButton.isEnabled = true
        }
    }
    
//    @IBAction func eventNameChanged(_ sender: UITextField) {
//        if(sender.text?.isEmpty)!{
//            createEventButton.isEnabled = false
//        } else{
//            createEventButton.isEnabled = true
//        }
//    }
    
    @objc func createEvent(){
        let alert = UIAlertController(title: "", message: "Create event and send out invites?", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            
            let newEventRef = self.ref!.child("Events").childByAutoId()
            let id = newEventRef.key
            let ADispatchGroup = DispatchGroup()
            ADispatchGroup.enter()
            if(self.image != nil){
                let eventPictureRef = self.storageRef?.child("pics/" + "Events/" + id + "/eventPic.jpg")
                let theUIImage = self.image!.resizedto150KB()
                let data = UIImageJPEGRepresentation(theUIImage!, 1.0)
                _ = eventPictureRef?.putData(data!, metadata: nil) { (metadata, error) in
                    ADispatchGroup.leave()
                    guard metadata != nil else {
                        return
                    }
                }
            } else { // probably unnecessary but might as well leave it in just in case I'm wrong
                let eventPictureRef = self.storageRef?.child("pics/" + "Events/" + id + "/eventPic.jpg")
                let theUIImage = UIImage(named: "noImageAvailable.png")
                let data = UIImageJPEGRepresentation(theUIImage!, 1.0)
                _ = eventPictureRef?.putData(data!, metadata: nil) { (metadata, error) in
                    ADispatchGroup.leave()
                    guard metadata != nil else {
                        return
                    }
                }
            }
            
            ADispatchGroup.notify(queue: .main){
                let miliseconds = "\(self.pickedDate.millisecondsSince1970)"
                let locationName = self.pickedRestaurant?.name
                let longitude = self.pickedRestaurant?.lng
                let latitude = self.pickedRestaurant?.lat
                let locationAddress = self.pickedRestaurant?.vicinity
                let locationId = self.pickedRestaurant?.placeId
                let timeMade = "\(Date().millisecondsSince1970)"
                let userId = Auth.auth().currentUser!.uid
                
                let newEvent = ["miliseconds": miliseconds, "typeOfEvent" : self.typeOfEvent, "groupOrNot": self.groupOrNot, "maxPeople": String(self.maxPeople!) as String, "priceBegin": String(self.priceMin!), "priceEnd": String(self.priceMax!), "eventId": id, "locationName": locationName, "longitude": longitude, "latitude": latitude, "locationAddress": locationAddress, "locationId" : locationId, "creator" : userId, "name" : self.eventNameLabel.text!, "remarks" : self.remarksTextView.text!, "picChangedAmount" : "0", "timeMade" : timeMade, "timeLastChanged" : timeMade, "rating" : self.rating]
                newEventRef.setValue(newEvent)
                self.ref!.child("Users").child(userId).child("myEvents").child(id).setValue(id) // put both in one database calls
                newEventRef.child("joinedPeople").child(userId).setValue(userId) //TODO add to newEvent to make it one databasecall.
                
                // new way of saving who is invited to what event
                let newDispatchGroup = DispatchGroup();
                for friend in self.allFriendsPicked!{
                    newDispatchGroup.enter()
                    self.ref!.child("publicProfiles").child(friend.id!).child("events").child(id).setValue(self.eventNameLabel.text!){(error, snapshot) in
                        newDispatchGroup.leave()
                    }
                }
                
                newDispatchGroup.enter()
                self.ref!.child("publicProfiles").child(self.userId!).child("events").child(id).setValue(self.eventNameLabel.text!){(error, snapshot) in
                    newDispatchGroup.leave()
                }
                
                let newEventRefInvitees = self.ref!.child("Events").child(id).child("eventInvitees")
                var theNewEventInvitees = [String : String]()
                for friend in self.allFriendsPicked!{
                    theNewEventInvitees[friend.id!] = friend.id
                }
                if (!theNewEventInvitees.keys.contains(userId)){
                    theNewEventInvitees[userId] = userId
                }
                newEventRefInvitees.setValue(theNewEventInvitees)
                
                // end new way
                
//                // old way of saving who is invited to what event
//                let newEventInviteesRef = self.ref!.child("EventInvitees").child(id)
//                var newEventInvitees = [String : String]()
//                for friend in self.allFriendsPicked!{
//                    newEventInvitees[friend.id!] = friend.id
//                }
//                if (!newEventInvitees.keys.contains(userId)){
//                    newEventInvitees[userId] = userId
//                }
//                newEventInviteesRef.setValue(newEventInvitees)
//                // end old way
                
//                // eventTimeStamp
//                let eventTimeStamps = ["timeMade" : timeMade, "timeLastChanged" : timeMade]
//                self.ref!.child("eventTimeStamps").child(id).setValue(eventTimeStamps)
                
                newDispatchGroup.notify(queue: .main){
                    self.dismiss(animated: true, completion: nil)
                }
                
//                self.dismiss(animated: true, completion: nil)
            }
        })
        let cancel = UIAlertAction(title: "No", style: .destructive, handler: { (action) -> Void in })
        
        alert.addAction(action1)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
//    @IBAction func createButton(_ sender: Any) {
//        let alert = UIAlertController(title: "", message: "Create event and send out invites?", preferredStyle: .alert)
//        let action1 = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
//
//            let newEventRef = self.ref!.child("Events").childByAutoId()
//            let id = newEventRef.key
//            let ADispatchGroup = DispatchGroup()
//            ADispatchGroup.enter()
//            if(self.image != nil){
//                let eventPictureRef = self.storageRef?.child("pics/" + "Events/" + id + "/eventPic.jpg")
//                let theUIImage = self.image!.resizedto150KB()
//                let data = UIImageJPEGRepresentation(theUIImage!, 1.0)
//                _ = eventPictureRef?.putData(data!, metadata: nil) { (metadata, error) in
//                    ADispatchGroup.leave()
//                    guard metadata != nil else {
//                        return
//                    }
//                }
//            } else { // probably unnecessary but might as well leave it in just in case I'm wrong
//                let eventPictureRef = self.storageRef?.child("pics/" + "Events/" + id + "/eventPic.jpg")
//                let theUIImage = UIImage(named: "noImageAvailable.png")
//                let data = UIImageJPEGRepresentation(theUIImage!, 1.0)
//                _ = eventPictureRef?.putData(data!, metadata: nil) { (metadata, error) in
//                    ADispatchGroup.leave()
//                    guard metadata != nil else {
//                        return
//                    }
//                }
//            }
//
//            ADispatchGroup.notify(queue: .main){
//                let miliseconds = "\(self.pickedDate.millisecondsSince1970)"
//                let locationName = self.pickedRestaurant?.name
//                let longitude = self.pickedRestaurant?.lng
//                let latitude = self.pickedRestaurant?.lat
//                let locationAddress = self.pickedRestaurant?.vicinity
//                let locationId = self.pickedRestaurant?.placeId
//                let timeMade = "\(Date().millisecondsSince1970)"
//                let userId = Auth.auth().currentUser!.uid
//
//                let newEvent = ["miliseconds": miliseconds, "typeOfEvent" : self.typeOfEvent, "groupOrNot": self.groupOrNot, "maxPeople": String(self.maxPeople!) as String, "priceBegin": String(self.priceMin!), "priceEnd": String(self.priceMax!), "eventId": id, "locationName": locationName, "longitude": longitude, "latitude": latitude, "locationAddress": locationAddress, "locationId" : locationId, "creator" : userId, "name" : self.eventNameLabel.text!, "remarks" : self.remarksTextView.text!, "picChangedAmount" : "0", "timeMade" : timeMade, "timeLastChanged" : timeMade, "rating" : self.rating]
//                newEventRef.setValue(newEvent)
//                self.ref!.child("Users").child(userId).child("myEvents").child(id).setValue(id) // put both in one database calls
//                newEventRef.child("joinedPeople").child(userId).setValue(userId) //TODO add to newEvent to make it one databasecall.
//
//                let newEventInviteesRef = self.ref!.child("EventInvitees").child(id)
//                var newEventInvitees = [String : String]()
//                for friend in self.allFriendsPicked!{
//                    newEventInvitees[friend.id!] = friend.id
//                }
//                if (!newEventInvitees.keys.contains(userId)){
//                    newEventInvitees[userId] = userId
//                }
//
//                newEventInviteesRef.setValue(newEventInvitees)
//
//
//                let eventTimeStamps = ["timeMade" : timeMade, "timeLastChanged" : timeMade]
//
//                self.ref!.child("eventTimeStamps").child(id).setValue(eventTimeStamps)
//
//                self.dismiss(animated: true, completion: nil)
//            }
//        })
//        let cancel = UIAlertAction(title: "No", style: .destructive, handler: { (action) -> Void in })
//
//        alert.addAction(action1)
//        alert.addAction(cancel)
//        present(alert, animated: true, completion: nil)
//    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let currentText = textView.text ?? ""
        guard let stringRange = Range(range, in: currentText) else { return false }
        let changedText = currentText.replacingCharacters(in: stringRange, with: text)
        return changedText.count <= 128
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(allFriendsPicked != nil){
            return (allFriendsPicked?.count)!
        } else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "groupCell")
        cell.textLabel?.text = allFriendsPicked![indexPath.row].name
        return cell
    }
}
