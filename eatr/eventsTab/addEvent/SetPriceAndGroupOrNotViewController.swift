//
//  SetPriceAndMaxPeopleViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 18/03/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import RangeSeekSlider

class SetPriceAndGroupOrNotViewController: UIViewController {

    var pickedRestaurant : RestaurantItem?
    var textPickedRest : String?
    var typeOfEvent : String!
    var pickedDate : Date?
    var image : UIImage?
    var rating : String!
    
    var topImage : UIImageView!
    
    @IBOutlet weak var priceRangeIntroLbl: UILabel!
    @IBOutlet weak var switchText: UILabel!
    @IBOutlet weak var disablePriceRangeOutlet: UIButton!
    @IBOutlet weak var switchOutlet: UISwitch!
    @IBOutlet weak var priceRangeSlider: RangeSeekSlider!
    @IBOutlet weak var switchInfo: UILabel!
    
    func setAllViews(){
        let theWidth = view.frame.width
        let topImageHeight = CGFloat(150)
        
        topImage = UIImageView(frame: CGRect(x: 0, y: 20, width: theWidth, height: topImageHeight))
        topImage.image = image
        topImage.contentMode = .scaleToFill
        topImage.addShadow()
        
        let placeNextButtonAt = view.frame.height - 40
        let nextButtonView = UIView(frame: CGRect(x: 0, y: placeNextButtonAt, width: theWidth, height: 40))
        nextButtonView.backgroundColor = UIColor.lightGray
        let nextButton = UIButton(frame: CGRect(x: theWidth/2, y: 0, width: theWidth/2, height: 40))
        nextButton.setTitleColor(UIColor.black, for: .normal)
        nextButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: theWidth/4, bottom: 0, right: 0)
        nextButton.addTarget(self, action: #selector(goToNext), for: .touchUpInside)
        nextButton.backgroundColor = UIColor.lightGray
        nextButton.setTitle("NEXT >", for: .normal)
        
        let discardButton = UIButton(frame: CGRect(x: 0, y: 0, width: theWidth/2, height: 40))
        discardButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: theWidth/4)
        discardButton.setTitle("< Discard", for: .normal)
        discardButton.setTitleColor(UIColor.red, for: .normal)
        discardButton.backgroundColor = UIColor.lightGray
        discardButton.addTarget(self, action: #selector(goBackToMainPage), for: .touchUpInside)
        
        let priceView = UIView(frame: CGRect(x: 5, y: 210, width: theWidth-10, height: 160))
        priceView.backgroundColor = UIColor.white
        priceView.addShadow()
        
        priceRangeIntroLbl.frame = CGRect(x: 0, y: 0, width: theWidth-10, height: 20)
        priceRangeIntroLbl.removeFromSuperview()
        priceView.addSubview(priceRangeIntroLbl)
        priceRangeSlider.frame = CGRect(x: 0, y: 30, width: theWidth-10 , height: 60)
        priceRangeSlider.removeFromSuperview()
        priceView.addSubview(priceRangeSlider)
        disablePriceRangeOutlet.frame = CGRect(x: 95, y: 100, width: theWidth-200, height: 30)
        disablePriceRangeOutlet.backgroundColor = UIColor.white
        disablePriceRangeOutlet.setTitle("I don't know", for: .normal)
        disablePriceRangeOutlet.removeFromSuperview()
        priceView.addSubview(disablePriceRangeOutlet)
        
        
        let switchView = UIView(frame: CGRect(x: 5, y: 410, width: theWidth-10, height: 100))
        switchView.backgroundColor = UIColor.white
        switchView.addShadow()
        switchInfo.frame = CGRect(x: 0, y: 10, width: theWidth-10, height: 20)
        switchInfo.removeFromSuperview()
        switchView.addSubview(switchInfo)
        switchOutlet.frame = CGRect(x: (switchView.frame.width-51)/2, y: 40, width: 51, height: 31)
        print(switchOutlet.frame.width)
        switchOutlet.removeFromSuperview()
        switchView.addSubview(switchOutlet)
        switchText.frame = CGRect(x: 0, y: 80, width: theWidth-10, height: 20)
        switchText.textAlignment = .center
        switchText.removeFromSuperview()
        switchView.addSubview(switchText)
        
        
        view.addSubview(priceView)
        view.addSubview(switchView)
        nextButtonView.addSubview(discardButton)
        nextButtonView.addSubview(nextButton)
        view.addSubview(nextButtonView)
        view.addSubview(topImage)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setAllViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.backgroundColor = UIColor(white: 1, alpha: 0.0)
    }
    
    @objc func goBackToMainPage(){
        let alert = UIAlertController(title: "", message: "Are you sure you want cancel? all your changes will be lost.", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
        })
        let cancel = UIAlertAction(title: "No", style: .destructive, handler: { (action) -> Void in })
        
        alert.addAction(action1)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    @objc func goToNext(){
        var priceMin = -1
        var priceMax = -1
        if(priceRangeSlider.isEnabled == true){
            priceMin = Int(priceRangeSlider.selectedMinValue)
            priceMax = Int(priceRangeSlider.selectedMaxValue)
        }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if(switchOutlet.isOn){
            let vc = storyboard.instantiateViewController(withIdentifier: "AddGroupID") as! AddGroupViewController
            vc.typeOfEvent = typeOfEvent
            vc.pickedRestaurant = pickedRestaurant
            vc.pickedDate = pickedDate
            vc.priceMin = priceMin
            vc.priceMax = priceMax
            vc.image = image
            vc.textPickedRest = textPickedRest
            vc.rating = rating
            self.navigationController?.pushViewController(vc, animated: true)
        } else{
            let vc = storyboard.instantiateViewController(withIdentifier: "AddContactsID") as! AddContactsViewController
            vc.typeOfEvent = typeOfEvent
            vc.pickedRestaurant = pickedRestaurant
            vc.pickedDate = pickedDate
            vc.priceMin = priceMin
            vc.priceMax = priceMax
            vc.image = image
            vc.textPickedRest = textPickedRest
            vc.rating = rating
        
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        if(sender.isOn){
            switchText.text = "Group"
        } else{
            switchText.text = "Contacts"
        }
    }
    
    @IBAction func disablePriceRangeButton(_ sender: Any) {
        if(priceRangeSlider.isEnabled == true){
            priceRangeSlider.alpha = 0.25
            priceRangeSlider.isEnabled = false
            priceRangeIntroLbl.text = "Cost of event is set as unknown"
            disablePriceRangeOutlet.setTitle("Set a cost range" , for: .normal)
        } else{
            priceRangeSlider.isEnabled = true
            priceRangeSlider.alpha = 1.0
            priceRangeIntroLbl.text = "How much will this cost people?"
            disablePriceRangeOutlet.setTitle("I don't know" , for: .normal)
        }
    }    
}
