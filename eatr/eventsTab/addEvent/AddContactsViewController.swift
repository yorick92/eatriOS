//
//  AddContactsViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 31/03/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import Firebase
import CoreData

class AddContactsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource {
    var pickedRestaurant : RestaurantItem?
    var localFacebookFriendsToPick = [localContactStruct]()
    var localFacebookFriendsPicked = [localContactStruct]()
    var localPresetsToPick = [localPresetStruct]()
    var localPresetsPicked = [localPresetStruct]()
    var textPickedRest : String?
    var userId : String?
    var typeOfEvent : String!
    var priceMin: Int?
    var priceMax: Int?
    var pickedDate : Date?
    var image : UIImage?
    var context : NSManagedObjectContext!
    var rating : String!
    
    var nextButton : UIButton!
    
    @IBOutlet weak var listToPickFrom: UITableView!
    @IBOutlet weak var chosenContactCollection: UICollectionView!
    
    func loadFriends(){
        self.localFacebookFriendsToPick = [localContactStruct]()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MyContact")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                for result in results as! [NSManagedObject]{
                    let theId = result.value(forKey: "userId") as! String
                    let theName = result.value(forKey: "name") as! String
                    let theFavCuis = result.value(forKey: "favCuis") as! String
                    let theFavRest = result.value(forKey: "favRest") as! String
                    let theGender = result.value(forKey: "gender") as! String
                    let picChangedAmount = result.value(forKey: "picChangedAmount") as! String
                    let timeLastChanged = result.value(forKey: "timeLastChanged") as! String
                    
                    self.localFacebookFriendsToPick.append(localContactStruct(id: theId, name: theName, favCuis: theFavCuis, favRest: theFavRest, gender: theGender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
                    
                }
            }
        } catch{
            print("error")
        }
    }
    
    func loadPresets(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Preset")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            print(results.count)
            if results.count > 0{
                for result in results as! [NSManagedObject]{
                    
                    let presetId = result.value(forKey: "id") as! String
                    let presetName = result.value(forKey: "name") as! String
                    let contactsSet = result.value(forKey: "contacts") as! NSSet
                    
                    var contacts = [localContactStruct]()
                    for personAny in contactsSet.allObjects{
                        if let person = personAny as? NSManagedObject{
                            let id = person.value(forKey: "userId") as! String
                            let name = person.value(forKey: "name") as! String
                            let favCuis = person.value(forKey: "favCuis") as! String
                            let favRest = person.value(forKey: "favRest") as! String
                            let gender = person.value(forKey: "gender") as! String
                            let picChangedAmount = person.value(forKey: "picChangedAmount") as! String
                            let timeLastChanged = person.value(forKey: "timeLastChanged") as! String
                            
                            contacts.append(localContactStruct(id: id, name: name, favCuis: favCuis, favRest: favRest, gender: gender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
                        }
                    }
                    
                    self.localPresetsToPick.append(localPresetStruct(name: presetName, id: presetId, contacts: contacts))
                }
            }
        } catch{
            print("error")
        }
        self.listToPickFrom.reloadData()
    }

    func setUpVariables(){
        userId = Auth.auth().currentUser!.uid
    }
    
    func setUpOutlets(){
//        nextOutlet.isEnabled = false
    }
    
    func setAllViews(){
        let theWidth = view.frame.width
        
        let placeNextButtonAt = view.frame.height - 40
        let nextButtonView = UIView(frame: CGRect(x: 0, y: placeNextButtonAt, width: theWidth, height: 40))
        nextButtonView.backgroundColor = UIColor.lightGray
        nextButton = UIButton(frame: CGRect(x: theWidth/2, y: 0, width: theWidth/2, height: 40))
        nextButton.setTitleColor(UIColor.black, for: .normal)
        nextButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: theWidth/4, bottom: 0, right: 0)
        nextButton.addTarget(self, action: #selector(goToNext), for: .touchUpInside)
        nextButton.backgroundColor = UIColor.lightGray
        nextButton.setTitle("NEXT >", for: .normal)
        nextButton.isEnabled = false
        
        let discardButton = UIButton(frame: CGRect(x: 0, y: 0, width: theWidth/2, height: 40))
        discardButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: theWidth/4)
        discardButton.setTitle("< Discard", for: .normal)
        discardButton.setTitleColor(UIColor.red, for: .normal)
        discardButton.backgroundColor = UIColor.lightGray
        discardButton.addTarget(self, action: #selector(goBackToMainPage), for: .touchUpInside)

        chosenContactCollection.frame = CGRect(x: 5, y: 20 + (navigationController?.navigationBar.frame.height)! + 10, width: theWidth-10, height: 60)
        chosenContactCollection.layer.borderWidth = 1
        
        listToPickFrom.frame = CGRect(x: 0, y: chosenContactCollection.frame.origin.y + 60 + 10, width: theWidth, height: view.frame.height-110-40)
        
        nextButtonView.addSubview(discardButton)
        nextButtonView.addSubview(nextButton)
        view.addSubview(nextButtonView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Add contacts"
        addTopViewColor(colorNavBar: true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext

        setAllViews()
        setUpVariables()
        setUpOutlets()
        loadFriends()
        loadPresets()
    }
    
    @objc func goBackToMainPage(){
        let alert = UIAlertController(title: "", message: "Are you sure you want cancel? all your changes will be lost.", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
        })
        let cancel = UIAlertAction(title: "No", style: .destructive, handler: { (action) -> Void in })
        
        alert.addAction(action1)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    @objc func goToNext(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MaxPeopleID") as! MaxPeopleViewController
        vc.typeOfEvent = typeOfEvent
        vc.pickedRestaurant = pickedRestaurant
        vc.pickedDate = pickedDate
        vc.priceMin = priceMin
        vc.priceMax = priceMax
        vc.image = image
        vc.textPickedRest = textPickedRest
        vc.allFriendsPicked = localFacebookFriendsPicked
        vc.rating = rating
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(section == 0){
            return "presets"
        } else{
            return "contacts"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            return localPresetsToPick.count
        } else{
            return localFacebookFriendsToPick.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "contactsCell")
            cell.textLabel?.text = localPresetsToPick[indexPath.row].name
            return cell
        } else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "contactCellID") as! ContactCellTableViewCell
            let contact = localFacebookFriendsToPick[indexPath.row]
            cell.nameLbl.text = contact.name
            cell.cuisineLbl.text = "Cuisine: " + localFacebookFriendsToPick[indexPath.row].favCuis!
            cell.profilePic.image = UIImage(named: "profile_icon.png")
            getPicDataForUser(theUserId: contact.id!, picChangedAmount: contact.picChangedAmount!, completionHandler: {picData in
                cell.profilePic.image = UIImage(data: picData as Data)
            })
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return localFacebookFriendsPicked.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContactCell", for: indexPath) as! ContactCollectionViewCell
        let contact = localFacebookFriendsPicked[indexPath.row]
        cell.contactName.text = contact.name
        cell.contactImage.image = UIImage(named: "profile_icon.png")
        getPicDataForUser(theUserId: contact.id!, picChangedAmount: contact.picChangedAmount!, completionHandler: {picData in
            cell.contactImage.image = UIImage(data: picData as Data)
        })
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        localFacebookFriendsToPick.append(localFacebookFriendsPicked[indexPath.row])
        localFacebookFriendsPicked.remove(at: indexPath.row)
        
        if(localFacebookFriendsPicked.count >= 2){
            nextButton.isEnabled = true
        } else{
            nextButton.isEnabled = false
        }
        
        listToPickFrom.reloadData()
        chosenContactCollection.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(tableView == listToPickFrom){
            if(indexPath.section == 0){
                for friend in localPresetsToPick[indexPath.row].contacts!{
                    if(!localFacebookFriendsPicked.contains(where: {x in x.id == friend.id})){
                        localFacebookFriendsPicked.append(friend)
                        localFacebookFriendsToPick.remove(at: localFacebookFriendsToPick.index(where: {$0.id == friend.id})!)
                    }
                }
            } else{
                localFacebookFriendsPicked.append(localFacebookFriendsToPick[indexPath.row])
                localFacebookFriendsToPick.remove(at: indexPath.row)
            }
        }
        
        if(localFacebookFriendsPicked.count > 0){
            nextButton.isEnabled = true
        } else{
            nextButton.isEnabled = false
        }
        
        listToPickFrom.reloadData()
        chosenContactCollection.reloadData()
    }
    
    
}
