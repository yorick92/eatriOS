//
//  PickEventTypeViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 13/04/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit

class PickEventTypeViewController: UIViewController {

    func setUpNavigationController(){
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "< Discard", style: .plain, target: self, action: #selector(goBack))
    }
    
    func setUpAllViews(){
        let theWidth = view.frame.width
        let topImageHeight = CGFloat(150)
        let topImage = UIImageView(frame: CGRect(x: 0, y: 20, width: theWidth, height: topImageHeight))
        topImage.image = UIImage(named: "noImageAvailable.png")
        topImage.contentMode = .scaleToFill
        topImage.addShadow()
        
        
        let strokeTextAttributes: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.strokeColor : UIColor.black,
            NSAttributedStringKey.foregroundColor : UIColor.white,
            NSAttributedStringKey.strokeWidth : -2.0,
        ]
        let infoText = UILabel(frame: CGRect(x: 10, y: topImageHeight, width: theWidth - 20, height: 20))
        infoText.attributedText = NSAttributedString(string: "What do you want to do?", attributes: strokeTextAttributes)
        infoText.font = UIFont.systemFont(ofSize: 22)
        
        
        let coffeeButton = UIButton(frame: CGRect(x: 10, y: topImageHeight + 50, width: theWidth - 20, height: 100))
        coffeeButton.setBackgroundImage(UIImage(named: "noImageAvailable.png"), for: .normal)
        coffeeButton.setTitle("Get coffee", for: .normal)
        coffeeButton.titleLabel?.attributedText = NSAttributedString(string: "Get coffee", attributes: strokeTextAttributes)
        coffeeButton.addTarget(self, action: #selector(getCoffee), for: .touchUpInside)
        coffeeButton.addShadow()
        
        let eatOutButton = UIButton(frame: CGRect(x: 10, y: topImageHeight + 200, width: theWidth - 20, height: 100))
        eatOutButton.setBackgroundImage(UIImage(named: "noImageAvailable.png"), for: .normal)
        eatOutButton.setTitle("Eat out", for: .normal)
        eatOutButton.titleLabel?.attributedText = NSAttributedString(string: "Eat out", attributes: strokeTextAttributes)
        eatOutButton.addTarget(self, action: #selector(eatOut), for: .touchUpInside)
        eatOutButton.addShadow()
        
        let getDrinksButton = UIButton(frame: CGRect(x: 10, y: topImageHeight + 350, width: theWidth - 20, height: 100))
        getDrinksButton.setBackgroundImage(UIImage(named: "noImageAvailable.png"), for: .normal)
        getDrinksButton.setTitle("Get drinks", for: .normal)
        getDrinksButton.titleLabel?.attributedText = NSAttributedString(string: "Get drinks", attributes: strokeTextAttributes)
        getDrinksButton.addTarget(self, action: #selector(getDrinks), for: .touchUpInside)
        getDrinksButton.addShadow()
        
        view.addSubview(topImage)
        view.addSubview(infoText)
        view.addSubview(coffeeButton)
        view.addSubview(eatOutButton)
        view.addSubview(getDrinksButton)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addTopViewColor(colorNavBar: false)
        
        setUpNavigationController()
        setUpAllViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.backgroundColor = UIColor(white: 1, alpha: 0.0)
    }
    
    @objc func getCoffee(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PickRestaurantID") as! PickLocationViewController
        vc.typeOfEvent = "cafe"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func eatOut(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PickRestaurantID") as! PickLocationViewController
        vc.typeOfEvent = "restaurant"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func getDrinks(){
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "PickRestaurantID") as! PickLocationViewController
        vc.typeOfEvent = "bar"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func goBack(){
        self.dismiss(animated: true, completion: nil)
    }
}
