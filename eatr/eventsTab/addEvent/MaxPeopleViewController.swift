//
//  MaxPeopleViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 31/03/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit

class MaxPeopleViewController: UIViewController {
    var allFriendsPicked = [localContactStruct]()
    var pickedRestaurant : RestaurantItem?
    var typeOfEvent : String!
    var textPickedRest : String?
    var priceMin: Int?
    var priceMax: Int?
    var maxPeople: Int?
    var pickedDate : Date?
    var image : UIImage?
    var rating : String!
    
    @IBOutlet weak var maxPeopleSliderOutlet: UISlider!
    @IBOutlet weak var maxPeopleLbl: UILabel!
    @IBOutlet weak var nrOfFriendInvitedOutlet: UILabel!
    
    func setUpOutlets(){
        maxPeopleLbl.text = "Max People: " + String(Int(maxPeopleSliderOutlet.value))
        maxPeopleSliderOutlet.maximumValue = Float(allFriendsPicked.count)
        maxPeople = Int(maxPeopleSliderOutlet.value)
        nrOfFriendInvitedOutlet.text = "You will invite " + String(allFriendsPicked.count) + " friends"
    }
    
    func setUpAllViews(){
        let theWidth = view.frame.width
        
        let placeNextButtonAt = view.frame.height - 40
        let nextButtonView = UIView(frame: CGRect(x: 0, y: placeNextButtonAt, width: theWidth, height: 40))
        nextButtonView.backgroundColor = UIColor.lightGray
        let nextButton = UIButton(frame: CGRect(x: theWidth/2, y: 0, width: theWidth/2, height: 40))
        nextButton.setTitleColor(UIColor.black, for: .normal)
        nextButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: theWidth/4, bottom: 0, right: 0)
        nextButton.addTarget(self, action: #selector(goToNext), for: .touchUpInside)
        nextButton.backgroundColor = UIColor.lightGray
        nextButton.setTitle("NEXT >", for: .normal)
        
        let discardButton = UIButton(frame: CGRect(x: 0, y: 0, width: theWidth/2, height: 40))
        discardButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: theWidth/4)
        discardButton.setTitle("< Discard", for: .normal)
        discardButton.setTitleColor(UIColor.red, for: .normal)
        discardButton.backgroundColor = UIColor.lightGray
        discardButton.addTarget(self, action: #selector(goBackToMainPage), for: .touchUpInside)

        nextButtonView.addSubview(nextButton)
        nextButtonView.addSubview(discardButton)
        view.addSubview(nextButtonView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Set max people for events"
        addTopViewColor(colorNavBar: true)
        setUpAllViews()
        setUpOutlets()
    }
    
    @objc func goToNext(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddEventOverviewID") as! AddEventOverviewViewController
        vc.typeOfEvent = typeOfEvent
        vc.pickedRestaurant = pickedRestaurant
        vc.pickedDate = pickedDate
        vc.priceMin = priceMin
        vc.priceMax = priceMax
        vc.maxPeople = maxPeople
        vc.image = image
        vc.textPickedRest = textPickedRest
        vc.allFriendsPicked = allFriendsPicked
        vc.rating = rating
        vc.groupOrNot = "not"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func goBackToMainPage(){
        let alert = UIAlertController(title: "", message: "Are you sure you want cancel? all your changes will be lost.", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
        })
        let cancel = UIAlertAction(title: "No", style: .destructive, handler: { (action) -> Void in })
        
        alert.addAction(action1)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        maxPeopleLbl.text = "Max People: " + String(Int(sender.value))
        maxPeople = Int(maxPeopleSliderOutlet.value)
    }
}
