//
//  PickLocationViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 18/03/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlacePicker
import GoogleMaps


class PickLocationViewController: UIViewController, CLLocationManagerDelegate, GMSPlacePickerViewControllerDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var allRestaurantOptions = [RestaurantItem]()
    var pickedRest : RestaurantItem?
    var typeOfEvent : String!
    var textPickedRest : String?
    var latitude : String = ""
    var longitude : String = ""
    var pickedLatitude : String = ""
    var pickedLongitude : String = ""
    var indexOfPickedRest = -1
    let locationManager = CLLocationManager()
    var useOwnLocation : Bool = true
    var gottenOwnLocation : Bool = false
    var locationDispatchGroup = DispatchGroup()
    
    var topImage : UIImageView!
    var checkItOutButton : UIButton!
    var checkItOutView : UIView!
    var locationButton : UIButton!
    var locationGottenOnce = false
    var spinnerView : UIView!
    var nextButton : UIButton!
    var switchView : UIView!
    var switchButton : UISwitch!
    var switchLabel : UILabel!
    var totalSize = 0.0
    
    @IBOutlet weak var restTableView: UITableView!
    
    func setUpLocationServices(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func setUpAllViews(){
        let theWidth = view.frame.width
        let topImageHeight = CGFloat(150)
        
        topImage = UIImageView(frame: CGRect(x: 0, y: 20, width: theWidth, height: topImageHeight))
        topImage.image = UIImage(named: "noImageAvailable.png")
        topImage.contentMode = .scaleToFill
        
        checkItOutButton = UIButton(frame: CGRect(x: theWidth-50, y: topImageHeight+20-50, width: 40, height: 40))
        checkItOutButton.backgroundColor = UIColor(red: 191, green: 42, blue: 42)
        checkItOutButton.setImage(UIImage(named: "goTo_icon.png"), for: .normal)
        checkItOutButton.addTarget(self, action: #selector(checkOutLocation), for: .touchUpInside)
        checkItOutButton.layer.cornerRadius = checkItOutButton.frame.width/2
        checkItOutButton.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        checkItOutButton.isHidden = true
        
        locationButton = UIButton(frame: CGRect(x: 5, y: 170, width: theWidth - 100, height: 50))
        locationButton.backgroundColor = UIColor.lightGray
        locationButton.addShadowBottom()
        locationButton.addTarget(self, action: #selector(setLocation), for: .touchUpInside)
        
        let icon = UIImage(named: "pin_icon.png")
        locationButton.setImage(icon, for: .normal)
        locationButton.imageView?.contentMode = .scaleAspectFit
        let toLeft = locationButton.frame.width/2
        locationButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: -toLeft, bottom: 10, right: 0)
        locationButton.setTitleColor(UIColor.black, for: .normal)
        locationButton.setTitle("Your location", for: .normal)
        locationButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: -toLeft-70, bottom: 0, right: 0)
        
        // TODO put switch and switchlabel in a view together.
        // TODO make the ordering of the list change by changing the switch
        switchView = UIView(frame: CGRect(x: theWidth-90, y: 170, width: 85, height: 50))
        switchView.backgroundColor = UIColor.lightGray
        switchView.addShadowBottom()
        
        switchLabel = UILabel(frame: CGRect(x: 0, y: 35, width: 90, height: 15))
        switchLabel.font = UIFont.systemFont(ofSize: 14)
        switchLabel.textColor = UIColor.red
        switchLabel.text = "By distance"
        switchLabel.textAlignment = .center
        
        switchButton = UISwitch(frame: CGRect(x: 19, y: 0, width: 52, height: 31))// width = 52
        switchButton.backgroundColor = UIColor.white
        switchButton.layer.cornerRadius = 15
        switchButton.addTarget(self, action: #selector(switchChanged), for: UIControlEvents.valueChanged)
        
        switchView.addSubview(switchLabel)
        switchView.addSubview(switchButton)
        
        restTableView.frame = CGRect(x: 0, y: 230, width: theWidth, height: view.frame.height - 280)
        
        let placeNextButtonAt = view.frame.height - 40
        
        let nextButtonView = UIView(frame: CGRect(x: 0, y: placeNextButtonAt, width: theWidth, height: 40))
        nextButtonView.backgroundColor = UIColor.lightGray
        nextButton = UIButton(frame: CGRect(x: theWidth/2, y: 0, width: theWidth/2, height: 40))
        nextButton.setTitleColor(UIColor.black, for: .normal)
        nextButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: theWidth/4, bottom: 0, right: 0)
        nextButton.addTarget(self, action: #selector(goToNext), for: .touchUpInside)
        nextButton.backgroundColor = UIColor.lightGray
        nextButton.isUserInteractionEnabled = false
        
        let discardButton = UIButton(frame: CGRect(x: 0, y: 0, width: theWidth/2, height: 40))
        discardButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: theWidth/4)
        discardButton.setTitle("< Discard", for: .normal)
        discardButton.setTitleColor(UIColor.red, for: .normal)
        discardButton.backgroundColor = UIColor.lightGray
        discardButton.addTarget(self, action: #selector(goBackToMainPage), for: .touchUpInside)
        
        nextButtonView.addSubview(discardButton)
        nextButtonView.addSubview(nextButton)
        view.addSubview(nextButtonView)
        view.addSubview(topImage)
        view.addSubview(locationButton)
        view.addSubview(switchView)
        view.addSubview(checkItOutButton)
    }
    
    @objc func goBackToMainPage(){
        let alert = UIAlertController(title: "", message: "Are you sure you want cancel? all your changes will be lost.", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            self.dismiss(animated: true, completion: nil)
        })
        let cancel = UIAlertAction(title: "No", style: .destructive, handler: { (action) -> Void in })
        
        alert.addAction(action1)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    @objc func switchChanged(theSwitch: UISwitch) {
        if(theSwitch.isOn){
            allRestaurantOptions = allRestaurantOptions.sorted(by: {Double($0.rating)! > Double($1.rating)!})
            switchLabel.text = "By rating"
        } else{
            allRestaurantOptions = allRestaurantOptions.sorted(by: {$0.distance < $1.distance})
            switchLabel.text = "By distance"
        }
        restTableView.reloadData()
    }
    
    @objc func checkOutLocation(){
        let restName = (pickedRest?.name)!.replacingOccurrences(of: " ", with: "+")
        var placeUrl = "https://www.google.nl/maps/search/" + "'" + restName + "'"
        placeUrl += "/@" + (pickedRest?.lng)! + "," + (pickedRest?.lat)!
        print(placeUrl)
        guard let url = URL(string: placeUrl) else {
            print("return")
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @objc func setLocation(){
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        present(placePicker, animated: true, completion: nil)
    }
    
    @objc func goToNext(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PickEventDateID") as! AddEventDateViewController
        vc.typeOfEvent = typeOfEvent
        vc.pickedRestaurant = pickedRest
        vc.image = (topImage.image)!
        vc.rating = pickedRest?.rating
        let ratingString = pickedRest?.rating
        let RestaurantString = "Restaurant: " + (pickedRest?.name)! + "\nnear: " + (pickedRest?.vicinity)!
        vc.textPickedRest = RestaurantString + "\nrating: " + ratingString!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinnerView = UIViewController.displaySpinner(onView: self.restTableView)
        setUpLocationServices()
        setUpAllViews()
        getListItems()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.backgroundColor = UIColor(white: 1, alpha: 0.0)
    }
    
    func getListItems(){
        locationDispatchGroup.enter()
        if(useOwnLocation){
            checkForLocationServices()
        } else {
            fillRestList()
        }
    }
    
    func giveSettingsAlert(path : String, message : String){
        let alert = UIAlertController(title: "", message: "You have not given permission for location services, would you like to go to your settings to change it?" , preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Go to settings", style: .default, handler: { (action) -> Void in
            guard let settingsUrl = URL(string: path) else {
                return
            }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                    print("Settings opened: \(success)") // Prints true
                    _ = self.navigationController?.popViewController(animated: true)
                })
            }
        })
        alert.addAction(action1)
        let cancel = UIAlertAction(title: "No, I'll just set a location on the map", style: .destructive, handler: { (action) -> Void in })
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    func checkForLocationServices(){
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                print("No access")
                giveSettingsAlert(path: UIApplicationOpenSettingsURLString, message: "You have not given permission for location services, would you like to go to your settings to give permission?")
            case .authorizedAlways, .authorizedWhenInUse:
                print("Access")
                locationDispatchGroup.notify(queue: .main){
                    self.fillRestList()
                }
            }
        } else {
            print("Location services are not enabled")
            giveSettingsAlert(path: "App-Prefs:root=Privacy&path=LOCATION", message: "Your location services are turned off, would you like to go to your settings to turn it on?")
            
        }
    }
    
    func fillRestList(){
        var urlString = ""
        if(useOwnLocation){
            urlString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude + "&type=" + typeOfEvent + "&rankby=distance&key=AIzaSyCeLVUpx-gIibTYo10URfS1oyKNf72XbmM"
        } else {
            urlString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + pickedLatitude + "," + pickedLongitude + "&type=" + typeOfEvent + "&rankby=distance&key=AIzaSyCeLVUpx-gIibTYo10URfS1oyKNf72XbmM"
        }
        print(urlString)
        let url = URL(string: urlString)
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error != nil{
                print("restaurantsListError")
                print(error ?? "error")
            }
            else{
                if let content = data {
                    do {
                        self.allRestaurantOptions.removeAll()
                        let restInfoList = try JSONDecoder().decode(restaurantInfoListStruct.self, from: content)
                        for object in restInfoList.results{
                            
                            var latToCalcDistanceFrom = ""
                            var longToCalcDistanceFrom = ""
                            if(self.useOwnLocation){
                                latToCalcDistanceFrom = self.latitude
                                longToCalcDistanceFrom = self.longitude
                            } else {
                                latToCalcDistanceFrom = self.pickedLatitude
                                longToCalcDistanceFrom = self.pickedLongitude
                            }
                            
                            self.allRestaurantOptions.append(RestaurantItem(
                                name: object.name ?? "no name",
                                placeId: object.place_id ?? "no place id",
                                rating:  NSString(format: "%.1f", object.rating ?? -1.0) as String,
                                vicinity: object.vicinity ?? "no vicinity",
                                photoReference: object.photos?[0].photo_reference ?? "no photos",
                                lat: NSString(format: "%.13f", object.geometry?.location?.lat ?? 0.0) as String,
                                lng: NSString(format: "%.13f", object.geometry?.location?.lng ?? 0.0) as String,
                                personLat: latToCalcDistanceFrom,
                                personLng: longToCalcDistanceFrom,
                                icon: object.icon ?? "no icon",
                                priceLevel: object.price_level ?? -1
                            ))
                        }
                        DispatchQueue.main.async {
                            self.restTableView.reloadData()
                            if(self.spinnerView != nil){
                                UIViewController.removeSpinner(spinner: self.spinnerView!)
                            }
                        }
                    } catch let jsonErr{
                        print("Error json: ", jsonErr)
                    }
                }
            }
        }
        task.resume()
    }
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        checkItOutButton.fadeOut()
        topImage.image = UIImage(named: "noImageAvailable.png")
        nextButton.isUserInteractionEnabled = false
        nextButton.setTitle("", for: .normal)
        viewController.dismiss(animated: true, completion: nil)
        pickedLatitude = NSString(format: "%.13f", place.coordinate.latitude) as String
        pickedLongitude = NSString(format: "%.13f", place.coordinate.longitude) as String
        useOwnLocation = false;
        locationButton.setTitle("picked location", for: .normal)
        fillRestList()
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        latitude = String(format:"%f", location.coordinate.latitude)
        longitude = String(format:"%f", location.coordinate.longitude)
        print("updated location")
        if(!locationGottenOnce){
            locationGottenOnce = true
            locationDispatchGroup.leave()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allRestaurantOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let restCell = tableView.dequeueReusableCell(withIdentifier: "restCell") as! RestTableViewCell
        let restOption = allRestaurantOptions[indexPath.row]
        restCell.nameLbl.text = restOption.name
        if(restOption.rating == "-1.0"){
            restCell.ratingLbl.text = "no rating"
        } else{
            restCell.ratingLbl.text = restOption.rating
        }
        restCell.distanceLbl.text = String(restOption.distance) + " km"
        
        restCell.restImage.image = UIImage(named: "noImageAvailable.png")
        
        if(restOption.priceLevel == -1){
            restCell.priceLevel.text = "Price level ?/3"
        } else{
            restCell.priceLevel.text = "Price level " + String(restOption.priceLevel) + "/3"
        }
        
        
        let session = URLSession(configuration: .default)
        if(restOption.icon != "no icon"){
            let urlString = restOption.icon
            let restImageFromUrl = session.dataTask(with: URL(string: urlString)!){ (data, response, error) in
                if let e = error{
                    print("an error occured: \(e)")
                } else{
                    if(response as? HTTPURLResponse) != nil{
                        if let imageData = data{
                            let theImage = UIImage(data: imageData)
                            DispatchQueue.main.async {
                                restCell.restImage.image = theImage
                            }
                            if let imageData = UIImagePNGRepresentation(theImage!) {
                                let bytes = imageData.count
                                let KB = Double(bytes) / 1024.0
                                self.totalSize += KB
                                print(self.totalSize)
                            }
                        } else{
                            print("no image found")
                        }
                    } else{
                        print("no server response")
                    }
                }
            }
            restImageFromUrl.resume()
        }else{
            // nothing
        }

        return restCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        pickedRest = allRestaurantOptions[indexPath.row]
        checkItOutButton.fadeIn()
        nextButton.isUserInteractionEnabled = true
        nextButton.setTitle("NEXT >", for: .normal)
        
        let session = URLSession(configuration: .default)
        if(pickedRest?.photoReference != "no photos"){
            let urlString = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + (pickedRest?.photoReference)! + "&key=AIzaSyCeLVUpx-gIibTYo10URfS1oyKNf72XbmM";
            let restImageFromUrl = session.dataTask(with: URL(string: urlString)!){ (data, response, error) in
                if let e = error{
                    print("an error occured: \(e)")
                } else{
                    if(response as? HTTPURLResponse) != nil{
                        if let imageData = data{
                            let theImage = UIImage(data: imageData)
                            DispatchQueue.main.async {
                                self.topImage.image = theImage
                            }
                            if let imageData = UIImagePNGRepresentation(theImage!) {
                                let bytes = imageData.count
                                let KB = Double(bytes) / 1024.0
                                print(KB)
                            }
                        } else{
                            DispatchQueue.main.async {
                                self.topImage.image = UIImage(named: "noImageAvailable.png")
                            }
                        }
                    } else{
                        DispatchQueue.main.async {
                            self.topImage.image = UIImage(named: "noImageAvailable.png")
                        }
                    }
                }
            }
            restImageFromUrl.resume()
        }else{
            DispatchQueue.main.async {
                self.topImage.image = UIImage(named: "noImageAvailable.png")
            }
        }
    }
    
    //    func presentThePopup(){
    //        var urlString = ""
    //        if(useOwnLocation){
    //            urlString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude + "&type=" + typeOfEvent + "&rankby=distance&key=AIzaSyCeLVUpx-gIibTYo10URfS1oyKNf72XbmM"
    //        } else {
    //            urlString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + pickedLatitude + "," + pickedLongitude + "&type=" + typeOfEvent + "&rankby=distance&key=AIzaSyCeLVUpx-gIibTYo10URfS1oyKNf72XbmM"
    //        }
    //        let url = URL(string: urlString)
    //        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
    //            if error != nil{
    //                print("restaurantsListError")
    //                print(error ?? "error")
    //            }
    //            else{
    //                if let content = data {
    //                    do {
    //                        self.allRestaurantOptions.removeAll()
    //                        let restInfoList = try JSONDecoder().decode(restaurantInfoListStruct.self, from: content)
    //                        for object in restInfoList.results{
    //                            var latToCalcDistanceFrom = ""
    //                            var longToCalcDistanceFrom = ""
    //                            if(self.useOwnLocation){
    //                                latToCalcDistanceFrom = self.latitude
    //                                longToCalcDistanceFrom = self.longitude
    //                            } else {
    //                                latToCalcDistanceFrom = self.pickedLatitude
    //                                longToCalcDistanceFrom = self.pickedLongitude
    //                            }
    //
    //                            self.allRestaurantOptions.append(RestaurantItem(
    //                                name: object.name ?? "no name",
    //                                placeId: object.place_id ?? "no place id",
    //                                rating:  NSString(format: "%.1f", object.rating ?? -1.0) as String,
    //                                vicinity: object.vicinity ?? "no vicinity",
    //                                photoReference: object.photos?[0].photo_reference ?? "no photos",
    //                                lat: NSString(format: "%.13f", object.geometry?.location?.lat ?? 0.0) as String,
    //                                lng: NSString(format: "%.13f", object.geometry?.location?.lng ?? 0.0) as String,
    //                                personLat: latToCalcDistanceFrom,
    //                                personLng: longToCalcDistanceFrom
    //                            ))
    //                        }
    //                        DispatchQueue.main.async {
    //                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
    //                            let vc = storyboard.instantiateViewController(withIdentifier: "restaurantsPopUp") as! RestaurantsPopUpViewController
    //                            vc.restaurantsList = self.allRestaurantOptions
    //                            vc.restProtocol = self
    //                            self.present(vc, animated: true, completion: nil)
    //                        }
    //                    } catch let jsonErr{
    //                        print("Error json: ", jsonErr)
    //                    }
    //                }
    //            }
    //        }
    //        task.resume()
    //    }

    
//    @IBAction func setLocationButton(_ sender: Any) {
//        let config = GMSPlacePickerConfig(viewport: nil)
//        let placePicker = GMSPlacePickerViewController(config: config)
//        placePicker.delegate = self
//        present(placePicker, animated: true, completion: nil)
//    }
//
//    @IBAction func presentPopupForLocations(_ sender: Any) {
//        if(useOwnLocation){
//            checkForLocationServices()
//        } else {
//            presentThePopup()
//        }
//    }
//
//    @IBAction func nextButton(_ sender: Any) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "PickEventDateID") as! AddEventDateViewController
//        vc.typeOfEvent = typeOfEvent
//        vc.pickedRestaurant = pickedRest
//        vc.image = image
//        vc.textPickedRest = textPickedRest
//        self.navigationController?.pushViewController(vc, animated: true)
//    }
    
//    func dismissPresentedView() {
//        self.presentedViewController?.dismiss(animated: true, completion: nil)
//        if(indexOfPickedRest != -1){
//            let thePickedRest = allRestaurantOptions[indexOfPickedRest]
//            pickedRest = thePickedRest
//            var ratingString = thePickedRest.rating
//            if(ratingString == "-1.0"){
//                ratingString = "no rating"
//            }
//            textPickedRest = "Restaurant: " + thePickedRest.name + "\nnear: " + thePickedRest.vicinity + "\nrating: " + ratingString
//            pickedRestInfo.numberOfLines = 0
//            pickedRestInfo.text = textPickedRest
//            nextButtonOutlet.isEnabled = true
//
//            let session = URLSession(configuration: .default)
//            if(thePickedRest.photoReference != "no photos"){
//                let urlString = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + thePickedRest.photoReference + "&key=AIzaSyDT0H-iqe2IW5cTCeIQs4nUpvo-51tqNHg";
//                let restImageFromUrl = session.dataTask(with: URL(string: urlString)!){ (data, response, error) in
//                    if let e = error{
//                        print("an error occured: \(e)")
//                    } else{
//                        if(response as? HTTPURLResponse) != nil{
//                            if let imageData = data{
//                                self.image = UIImage(data: imageData)
//                                DispatchQueue.main.async {
//                                    self.pickedRestImage.image = self.image
//                                }
//                            } else{
//                                print("here")
//                                self.image = UIImage(named: "noImageAvailable.png")
//                                DispatchQueue.main.async {
//                                    self.pickedRestImage.image = self.image
//                                }
//                                print("no image found")
//                            }
//                        } else{
//                            print("no server response")
//                        }
//                    }
//                }
//                restImageFromUrl.resume()
//            }else{
//                self.image = UIImage(named: "noImageAvailable.png")
//                DispatchQueue.main.async {
//                    self.pickedRestImage.image = self.image
//                }
//            }
//        }
//    }

}
