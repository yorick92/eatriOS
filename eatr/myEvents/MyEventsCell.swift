//
//  MyEventsCell.swift
//  eatr
//
//  Created by Yorick Bolster on 30/03/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit

class MyEventsCell: UITableViewCell {
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var maxPeopleLbl: UILabel!
    @IBOutlet weak var priceRangeLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var madeByLbl: UILabel!
    @IBOutlet weak var madeByImage: UIImageView!
    
    @IBOutlet weak var joinedImage1: UIImageView!
    @IBOutlet weak var joinedImage2: UIImageView!
    @IBOutlet weak var joinedImage3: UIImageView!
    @IBOutlet weak var andMoreImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
