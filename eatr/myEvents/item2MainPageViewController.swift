//
//  item2MainPageViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 04/02/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import Firebase
import CoreData
import Floaty

class item2MainPageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, reloadAllDataProtocol{
    var myCreatedEvents = [Event]()
    var myJoinedEvents = [Event]()
    var userId : String?
    var context : NSManagedObjectContext!
    var floaty : Floaty!
    
    var theWidth : CGFloat!
    var sortingView : UIView!
    var sortingButton : UIButton!
    var sortingViewText : UILabel!
    var sortingViews = [UIView]()
    var sortingExpanded = false
    
    var myEventsButton : UIButton!
    var myJoinedEventsButton : UIButton!
    
    var lastScrollPositionMyEvents = CGFloat(0)
    var lastScrollPositionMyJoinedEvents = CGFloat(0)

    @IBOutlet weak var myEventsList: UITableView!
    @IBOutlet weak var myJoinedEventsList: UITableView!
    @IBOutlet weak var myEventsListView: UIView!
    @IBOutlet weak var myJoinedEventsListView: UIView!
    func getString(object : Any?) -> String{
        if let a = object as? String{
            return a
        } else{
            return ""
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let theOffsetY = scrollView.contentOffset.y
        if(scrollView == myEventsList){
            if(lastScrollPositionMyEvents < theOffsetY){
                if(theOffsetY > 0){
                    floaty.fadeOut()
                }
            } else{
                if(theOffsetY < scrollView.contentSize.height - scrollView.frame.height){
                    floaty.fadeIn()
                }
            }
            lastScrollPositionMyEvents = theOffsetY
        } else{
            if(lastScrollPositionMyJoinedEvents < theOffsetY){
                if(theOffsetY > 0){
                    floaty.fadeOut()
                }
            } else{
                floaty.fadeIn()
            }
            lastScrollPositionMyJoinedEvents = theOffsetY
        }
    }
    
    func deleteCoreDataEvents(entityName : String){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                for result in results as! [NSManagedObject]{
                    self.context.delete(result)
                    if let anEventName = result.value(forKey: "eventName") as? String{
                        print(anEventName + " will be deleted")
                    }
                }
            }
        } catch{
            print("error")
        }
        
        do{
            try self.context.save()
            print("deleted CoreDataEvents")
        } catch{
            
        }
    }
    
    func deleteEventFromCoreData(eventId : String, entityName : String){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "eventId") as! String == eventId}){
                    self.context.delete(result as! NSManagedObject)
                }
            }
        } catch{
            print("error")
        }
        
        do{
            try self.context.save()
        } catch{
            
        }
    }
    
    func getMyEventsFromStorage(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "CoreDataEvent")
        request.returnsObjectsAsFaults = false
        self.myCreatedEvents = [Event]()
        self.myJoinedEvents = [Event]()
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                for result in results as! [NSManagedObject]{
                    var isCreator : Bool = false
                    var hasJoined : Bool = false
                    let creator = result.value(forKey: "creator") as! String
                    let joinedPeopleSet : NSSet = result.value(forKey: "joinedPeople") as! NSSet
                    
                    var joinedPeople = [localContactStruct]()
                    for personAny in joinedPeopleSet{
                        if let person = personAny as? NSManagedObject{
                            let id = person.value(forKey: "userId") as! String
                            let name = person.value(forKey: "name") as! String
                            let favCuis = person.value(forKey: "favCuis") as! String
                            let favRest = person.value(forKey: "favRest") as! String
                            let gender = person.value(forKey: "gender") as! String
                            let picChangedAmount = person.value(forKey: "picChangedAmount") as! String
                            let timeLastChanged = person.value(forKey: "timeLastChanged") as! String
                            
                            joinedPeople.append(localContactStruct(id: id, name: name, favCuis: favCuis, favRest: favRest, gender: gender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
                        }
                    }
                    
                    if(userId! == creator){
                        isCreator = true
                    }
                    if(joinedPeople.index(where: {$0.id == userId!}) != nil){
                        hasJoined = true
                    }
                    if(!isCreator && !hasJoined){
                        continue
                    }
                    
                    let dateMilliseconds = result.value(forKey: "dateStart") as! Int
                    let dateStart = Date(milliseconds: dateMilliseconds)
                    let eventId = result.value(forKey: "eventId") as! String
                    let eventName = result.value(forKey: "eventName") as! String
                    let isGroupEvent = result.value(forKey: "isGroupEvent") as! Bool
                    let latitude = result.value(forKey: "latitude") as! String
                    let locationAddress = result.value(forKey: "locationAddress") as! String
                    let locationId = result.value(forKey: "locationId") as! String
                    let locationName = result.value(forKey: "locationName") as! String
                    let longitude = result.value(forKey: "longitude") as! String
                    let maxPeople = result.value(forKey: "maxPeople") as! String
                    let nrOfPeopleInvited = result.value(forKey: "nrOfPeopleInvited") as! String
                    let picChangedAmount = result.value(forKey: "picChangedAmount") as! String
                    let priceBegin = result.value(forKey: "priceBegin") as! String
                    let priceEnd = result.value(forKey: "priceEnd") as! String
                    let remarks = result.value(forKey: "remarks") as! String
                    let timeLastChanged = result.value(forKey: "timeLastChanged") as! String
                    let timeMade = result.value(forKey: "timeMade") as! String
                    let typeOfEvent = result.value(forKey: "typeOfEvent") as! String
                    
                    let invitedPeopleSet : NSSet = result.value(forKey: "invitedPeople") as! NSSet
                    
                    var invitedPeople = [localContactStruct]()
                    for personAny in invitedPeopleSet{
                        if let person = personAny as? NSManagedObject{
                            let id = person.value(forKey: "userId") as! String
                            let name = person.value(forKey: "name") as! String
                            let favCuis = person.value(forKey: "favCuis") as! String
                            let favRest = person.value(forKey: "favRest") as! String
                            let gender = person.value(forKey: "gender") as! String
                            let picChangedAmount = person.value(forKey: "picChangedAmount") as! String
                            let timeLastChanged = person.value(forKey: "timeLastChanged") as! String
                            
                            invitedPeople.append(localContactStruct(id: id, name: name, favCuis: favCuis, favRest: favRest, gender: gender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
                        }
                    }
                    
                    if(isCreator){
                        self.myCreatedEvents.append(Event(creator: creator, eventId: eventId, isGroupEvent: isGroupEvent, latitude: latitude, locationAddress: locationAddress, locationId: locationId, locationName: locationName, longitude: longitude, maxPeople: maxPeople, dateStart: dateStart, eventName: eventName, picChangedAmount: picChangedAmount, priceBegin: priceBegin, priceEnd: priceEnd, remarks: remarks, typeOfEvent: typeOfEvent, timeMade: timeMade, timeLastChanged: timeLastChanged, nrOfPeopleInvited: nrOfPeopleInvited,joinedPeople: joinedPeople, invitedPeople: invitedPeople))
                    }
                    if(hasJoined){
                        self.myJoinedEvents.append(Event(creator: creator, eventId: eventId, isGroupEvent: isGroupEvent, latitude: latitude, locationAddress: locationAddress, locationId: locationId, locationName: locationName, longitude: longitude, maxPeople: maxPeople, dateStart: dateStart, eventName: eventName, picChangedAmount: picChangedAmount, priceBegin: priceBegin, priceEnd: priceEnd, remarks: remarks, typeOfEvent: typeOfEvent, timeMade: timeMade, timeLastChanged: timeLastChanged, nrOfPeopleInvited: nrOfPeopleInvited,joinedPeople: joinedPeople, invitedPeople: invitedPeople))
                    }
                    
                }
            }
        } catch{
            print("error")
        }
        
        do{
            try self.context.save()
        } catch{
            
        }
    }
    
    @objc func addEvent(gesture: UIGestureRecognizer) {
        if(Reachability.isConnectedToNetwork()){
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "PickEventTypeID") as! PickEventTypeViewController
            let navController = UINavigationController(rootViewController: nextViewController)
            self.present(navController, animated: true, completion: nil)
        }
    }
    
    func makeFloaty(){
        if(floaty != nil){
            floaty.removeFromSuperview()
        }
        floaty = Floaty(frame: CGRect(x: view.frame.width-70, y: view.frame.height-(tabBarController?.tabBar.frame.height)! - 70, width: 56, height: 56))
        floaty.buttonColor = UIColor(red: 191, green: 42, blue: 42)
        view.addSubview(floaty)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(item2MainPageViewController.addEvent(gesture:)))
        floaty.addGestureRecognizer(tapGesture)
        floaty.isUserInteractionEnabled = true
    }
    
    func addSortingView(){
        let theHeight = CGFloat(50)
        sortingView = UIView(frame: CGRect(x: 0, y: 20, width: theWidth, height: theHeight))
        sortingView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        let sortingIcon = UIImageView(frame: CGRect(x: theWidth/6, y: 10, width: theWidth/6-theWidth/24, height: theHeight-20))
        sortingIcon.image = UIImage(named: "sort_icon.png")?.withRenderingMode(.alwaysTemplate)
        sortingIcon.tintColor = UIColor.white
        
        sortingViewText = UILabel(frame: CGRect(x: theWidth/3, y: 0, width: theWidth/3, height: theHeight))
        sortingViewText.text = "by date"
        sortingViewText.textColor = UIColor.white
        
        sortingButton = UIButton(frame: CGRect(x: theWidth/3*2, y: 0, width: theWidth/3, height: theHeight))
        let tintedImage = UIImage(named: "expand_icon.png")?.withRenderingMode(.alwaysTemplate)
        sortingButton.setImage(tintedImage, for: .normal)
        sortingButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: theWidth/6, bottom: 10, right: theWidth/12)
        sortingButton.tintColor = UIColor.white
        sortingButton.addTarget(self, action: #selector(expandSorting), for: .touchUpInside)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(expandSorting))
        
        sortingView.addSubview(sortingIcon)
        sortingView.addSubview(sortingViewText)
        sortingView.addSubview(sortingButton)
        sortingView.addGestureRecognizer(tapGesture)
        
        view.addSubview(sortingView)
    }
    
    func addButtonForMyEventListView(){
        let localHeight = CGFloat(50)
        let theHeight = view.frame.height - (tabBarController?.tabBar.frame.height)! - 70 - 50
        myEventsListView.frame = CGRect(x: myEventsListView.frame.origin.x, y: 70, width: myEventsListView.frame.width, height: theHeight)
        myEventsListView.backgroundColor = UIColor.white
        myEventsList.frame = CGRect(x: 5, y: 45, width: theWidth - 10, height: theHeight - 45)
        
        let sortingViewText = UILabel(frame: CGRect(x: 10, y: 0, width: theWidth, height: localHeight))
        sortingViewText.text = "My created events"
        sortingViewText.font = UIFont.systemFont(ofSize: 18)
        sortingViewText.textColor = UIColor.red
        
        let borderView = UIView(frame: CGRect(x: 10, y: 49, width: theWidth-40, height: 1))
        borderView.backgroundColor = UIColor.black
        
        myEventsButton = UIButton(frame: CGRect(x: theWidth/3*2, y: 0, width: theWidth/3, height: localHeight))
        let tintedImage = UIImage(named: "collapse_icon.png")?.withRenderingMode(.alwaysTemplate)
        myEventsButton.setImage(tintedImage, for: .normal)
        myEventsButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: theWidth/6, bottom: 10, right: theWidth/12)
        myEventsButton.tintColor = UIColor.red
        myEventsButton.addTarget(self, action: #selector(setEventViews), for: .touchUpInside)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(setEventViews))
        sortingViewText.isUserInteractionEnabled = true
        sortingViewText.addGestureRecognizer(tapGesture)
        
        myEventsListView.addSubview(sortingViewText)
        myEventsListView.addSubview(myEventsButton)
        myEventsListView.addSubview(borderView)
    }
    
    func addButtonForMyJoinedEventListView(){
        let localHeight = CGFloat(50)
        let theHeight = view.frame.height - (tabBarController?.tabBar.frame.height)! - 70 - 50
        myJoinedEventsListView.frame = CGRect(x: myJoinedEventsListView.frame.origin.x, y: theHeight + 70, width: myJoinedEventsListView.frame.width, height: 50)
        myJoinedEventsListView.backgroundColor = UIColor.white
        myJoinedEventsList.frame = CGRect(x: 5, y: 45, width: theWidth - 10, height: theHeight - 45)
        myJoinedEventsList.isHidden = true
        
        let sortingViewText = UILabel(frame: CGRect(x: 10, y: 0, width: theWidth - 20, height: localHeight))
        sortingViewText.text = "My joined events"
        sortingViewText.font = UIFont.systemFont(ofSize: 18)
        sortingViewText.textColor = UIColor.red
        
        myJoinedEventsButton = UIButton(frame: CGRect(x: theWidth/3*2, y: 0, width: theWidth/3, height: localHeight))
        let tintedImage = UIImage(named: "expand_icon.png")?.withRenderingMode(.alwaysTemplate)
        myJoinedEventsButton.setImage(tintedImage, for: .normal)
        myJoinedEventsButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: theWidth/6, bottom: 10, right: theWidth/12)
        myJoinedEventsButton.tintColor = UIColor.red
        myJoinedEventsButton.addTarget(self, action: #selector(setEventViews), for: .touchUpInside)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(setEventViews))
        sortingViewText.isUserInteractionEnabled = true
        sortingViewText.addGestureRecognizer(tapGesture)
        
        myJoinedEventsListView.addSubview(sortingViewText)
        myJoinedEventsListView.addSubview(myJoinedEventsButton)
    }
    
    @objc func setEventViews(){
        var setMyEventsViewToHeight = CGFloat(0)
        var setMyJoinedEventsViewToHeight = CGFloat(0)
        var setMyJoinedEventsViewToY = CGFloat(0)
        var setMyEventsToImage = ""
        var setMyJoinedEventsToImage = ""
        if(myEventsList.isHidden){ // expand
            myEventsList.isHidden = false
            myJoinedEventsList.isHidden = true
            setMyEventsViewToHeight = view.frame.height - (tabBarController?.tabBar.frame.height)! - 70 - 50
            setMyJoinedEventsViewToY = setMyEventsViewToHeight + 70
            setMyJoinedEventsViewToHeight = CGFloat(50)
            setMyEventsToImage = "collapse_icon.png"
            setMyJoinedEventsToImage = "expand_icon.png"
        } else { // collapse
            myEventsList.isHidden = true
            myJoinedEventsList.isHidden = false
            setMyEventsViewToHeight = CGFloat(50)
            setMyJoinedEventsViewToY = setMyEventsViewToHeight + 70
            setMyJoinedEventsViewToHeight = view.frame.height - (tabBarController?.tabBar.frame.height)! - 70 - 50
            setMyEventsToImage = "expand_icon.png"
            setMyJoinedEventsToImage = "collapse_icon.png"
        }
        
        let tintedImageMyEvents = UIImage(named: setMyEventsToImage)?.withRenderingMode(.alwaysTemplate)
        let tintedImageMyJoinedEvents = UIImage(named: setMyJoinedEventsToImage)?.withRenderingMode(.alwaysTemplate)
        myEventsButton.setImage(tintedImageMyEvents, for: .normal)
        myJoinedEventsButton.setImage(tintedImageMyJoinedEvents, for: .normal)
        myEventsListView.frame = CGRect(x: myEventsListView.frame.origin.x, y: 70, width: myEventsListView.frame.width, height: setMyEventsViewToHeight)
        myJoinedEventsListView.frame = CGRect(x: myJoinedEventsListView.frame.origin.x, y: setMyJoinedEventsViewToY, width: myJoinedEventsListView.frame.width, height: setMyJoinedEventsViewToHeight)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        
        theWidth = view.frame.width
//        addTopViewColor(colorNavBar: true)
        userId = Auth.auth().currentUser?.uid
        makeFloaty()
        addSortingView()
        addButtonForMyEventListView()
        addButtonForMyJoinedEventListView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getMyEventsFromStorage()
        sortEvents()
    }
    
    @objc func expandSorting(){
        let theHeight = CGFloat(50)
        var valueChanged = CGFloat(0)
        var imageName = ""
        
        if(sortingExpanded){
            sortingExpanded = false
            valueChanged = CGFloat(sortingViews.count * 50 * -1)
            sortingView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            for view in sortingViews{
                view.removeFromSuperview()
            }
            sortingViews = [UIView]()
            imageName = "expand_icon.png"
        } else {
            sortingExpanded = true
            var optionAndPngName = [(optionName : String, pngName : String, selector : Selector)]()
            optionAndPngName.append(("by date", "clock_icon.png", #selector(sortByTime)))
            optionAndPngName.append(("by price", "distance_icon.png", #selector(sortByPrice)))
            
            valueChanged = CGFloat(50 * optionAndPngName.count)
            sortingView.backgroundColor = UIColor.gray
            
            var counter = 0
            for option in optionAndPngName{
                let sortOption = UIView(frame: CGRect(x: 0, y: CGFloat(50 + counter * 50), width: theWidth, height: theHeight))
                sortOption.addBottomBorderWithColor(color: UIColor.white, width: 0.5)
                sortOption.addTopBorderWithColor(color: UIColor.white, width: 0.5)
                let tapGesture = UITapGestureRecognizer(target: self, action: option.selector)
                sortOption.addGestureRecognizer(tapGesture)
                sortOption.isUserInteractionEnabled = true
                
                
                let optionIcon = UIImageView(frame: CGRect(x: theWidth/6, y: 10, width: theWidth/6-theWidth/24, height: theHeight-20))
                optionIcon.image = UIImage(named: option.pngName)?.withRenderingMode(.alwaysTemplate)
                optionIcon.tintColor = UIColor.white
                
                let sortOptionLabel = UILabel(frame: CGRect(x: theWidth/3, y: 0, width: theWidth/3, height: theHeight))
                sortOptionLabel.text = option.optionName
                sortOptionLabel.textColor = UIColor.white
                
                sortOption.addSubview(optionIcon)
                sortOption.addSubview(sortOptionLabel)
                
                sortingViews.append(sortOption)
                sortingView.addSubview(sortOption)
                counter += 1
            }
            imageName = "collapse_icon.png"
        }
        
        let tintedImageMyEvents = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
        sortingButton.setImage(tintedImageMyEvents, for: .normal)
        sortingView.frame = CGRect(x: sortingView.frame.origin.x, y: sortingView.frame.origin.y, width: sortingView.frame.width, height: sortingView.frame.height + valueChanged)
    }
    
    @objc func sortByTime(){
        myCreatedEvents = myCreatedEvents.sorted(by: {$0.dateStart < $1.dateStart})
        myEventsList.reloadData()
        
        myJoinedEvents = myJoinedEvents.sorted(by: {$0.dateStart < $1.dateStart})
        myJoinedEventsList.reloadData()
        
        sortingViewText.text = "By date"
        if(sortingExpanded){
            expandSorting()
        }
    }
    
    @objc func sortByPrice(){
        var length = myCreatedEvents.count
        if length > 0{
            for i in 0...(length-1){
                if(myCreatedEvents[i].priceBegin == "-1"){
                    myCreatedEvents[i].priceBegin = "999999"
                }
            }
            myCreatedEvents = myCreatedEvents.sorted(by: {Int($0.priceBegin)! < Int($1.priceBegin)!})
            for i in 0...(length-1){
                if(myCreatedEvents[i].priceBegin == "999999"){
                    myCreatedEvents[i].priceBegin = "-1"
                }
            }
        }
        myEventsList.reloadData()
        
        length = myJoinedEvents.count
        if length > 0{
            for i in 0...(length-1){
                if(myJoinedEvents[i].priceBegin == "-1"){
                    myJoinedEvents[i].priceBegin = "999999"
                }
            }
            myJoinedEvents = myJoinedEvents.sorted(by: {Int($0.priceBegin)! < Int($1.priceBegin)!})
            for i in 0...(length-1){
                if(myJoinedEvents[i].priceBegin == "999999"){
                    myJoinedEvents[i].priceBegin = "-1"
                }
            }
        }
        myJoinedEventsList.reloadData()
        
        sortingViewText.text = "By price"
        if(sortingExpanded){
            expandSorting()
        }
    }

    func reloadEverything() {
        getMyEventsFromStorage()
        sortEvents()
    }
    
    func sortEvents(){
        if(sortingViewText.text == "By date"){
            sortByTime()
        } else {
            sortByPrice()
        }
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(tableView == myEventsList){
            return myCreatedEvents.count
        } else {
            return myJoinedEvents.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        var event : Event!
        
        if(tableView == myEventsList){
            event = myCreatedEvents[indexPath.section]
        } else {
            event = myJoinedEvents[indexPath.section]
        }

        let cell = tableView.dequeueReusableCell(withIdentifier: "mainPageListCellID") as! MyEventsCell
        cell.selectionStyle = .none
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm"
        let dateString = formatter.string(from: event.dateStart)
        
        cell.dateLbl.text = dateString
        cell.maxPeopleLbl.text = "Max people: " + String(event.maxPeople)
        if(event.priceBegin == "-1"){
            cell.priceRangeLbl.text = "€ ?? - ??"
        } else{
            cell.priceRangeLbl.text = "€ " + String(event.priceBegin) + " - " + String(event.priceEnd)
        }
        cell.nameLbl.text = event.eventName
        cell.locationLbl.text = "at: " + event.locationName
        
        // get contact who made it info
        let contactObject = contactExists(contactId: event.creator, entityName: "Contact", context: self.context)
        let contactName = contactObject?.value(forKey: "name") as! String
        let contactPicChangedAmount = contactObject?.value(forKey: "picChangedAmount") as! String
        
        cell.madeByLbl.text = "by " + contactName
        
//        print("in the created by picture")
//        print(event.creator)
//        print(contactPicChangedAmount)
        getPicDataForUser(theUserId: event.creator, picChangedAmount: contactPicChangedAmount, completionHandler: {picData in
            cell.madeByImage.image = UIImage(data: picData as Data)
            cell.madeByImage.layer.borderWidth = 1.0
            cell.madeByImage.layer.masksToBounds = false
            cell.madeByImage.layer.borderColor = UIColor.white.cgColor
            cell.madeByImage.layer.cornerRadius = cell.madeByImage.frame.size.width / 2
            cell.madeByImage.clipsToBounds = true
        })
        
        
        cell.joinedImage1.layer.borderWidth = 1.0
        cell.joinedImage1.layer.masksToBounds = false
        cell.joinedImage1.layer.borderColor = UIColor.white.cgColor
        cell.joinedImage1.layer.cornerRadius = cell.madeByImage.frame.size.width / 2
        cell.joinedImage1.clipsToBounds = true
        cell.joinedImage1.layer.zPosition = 1
        cell.joinedImage1.image = UIImage(named: "profile_icon.png")
        
        cell.joinedImage2.layer.borderWidth = 1.0
        cell.joinedImage2.layer.masksToBounds = false
        cell.joinedImage2.layer.borderColor = UIColor.white.cgColor
        cell.joinedImage2.layer.cornerRadius = cell.madeByImage.frame.size.width / 2
        cell.joinedImage2.clipsToBounds = true
        cell.joinedImage2.layer.zPosition = 2
        cell.joinedImage2.image = UIImage(named: "profile_icon.png")
        
        cell.joinedImage3.layer.borderWidth = 1.0
        cell.joinedImage3.layer.masksToBounds = false
        cell.joinedImage3.layer.borderColor = UIColor.white.cgColor
        cell.joinedImage3.layer.cornerRadius = cell.madeByImage.frame.size.width / 2
        cell.joinedImage3.clipsToBounds = true
        cell.joinedImage3.layer.zPosition = 3
        cell.joinedImage3.image = UIImage(named: "profile_icon.png")
        
        if(event.joinedPeople.count > 0){
//            print("in the joined people picture")
//            print(event.joinedPeople[0].id!)
//            print(event.joinedPeople[0].picChangedAmount!)
            getPicDataForUser(theUserId: event.joinedPeople[0].id!, picChangedAmount: event.joinedPeople[0].picChangedAmount!, completionHandler: {picData in
                cell.joinedImage1.image = UIImage(data: picData as Data)
            })
        } else {
            
        }
        
        if(event.joinedPeople.count > 1){
            getPicDataForUser(theUserId: event.joinedPeople[1].id!, picChangedAmount: event.joinedPeople[1].picChangedAmount!, completionHandler: {picData in
                cell.joinedImage2.image = UIImage(data: picData as Data)
            })
        } else {
            
        }
        
        if(event.joinedPeople.count > 2){
            getPicDataForUser(theUserId: event.joinedPeople[2].id!, picChangedAmount: event.joinedPeople[2].picChangedAmount!, completionHandler: {picData in
                cell.joinedImage3.image = UIImage(data: picData as Data)
            })
        } else {
            
        }
        
        cell.andMoreImage.image = UIImage(named: "plus_icon.png")
        cell.andMoreImage.layer.borderWidth = 1.0
        cell.andMoreImage.layer.masksToBounds = false
        cell.andMoreImage.layer.borderColor = UIColor.white.cgColor
        cell.andMoreImage.layer.cornerRadius = cell.madeByImage.frame.size.width / 2
        cell.andMoreImage.clipsToBounds = true
        cell.andMoreImage.layer.zPosition = 4
        
        cell.dateLbl.textColor = UIColor.white
        cell.maxPeopleLbl.textColor = UIColor.white
        cell.priceRangeLbl.textColor = UIColor.white
        cell.nameLbl.textColor = UIColor.white
        cell.locationLbl.textColor = UIColor.white
        cell.madeByLbl.textColor = UIColor.white
        
        //        cell.backgroundView = UIImageView(image: UIImage(named: "profile_icon.png"))
        
        var nrOfTimesPicChanged = "0"
        if event.picChangedAmount != ""{
            nrOfTimesPicChanged = event.picChangedAmount
        }
        
        if(UserDefaults.standard.object(forKey: event.eventId + "/" + nrOfTimesPicChanged ) != nil){
            let picData = UserDefaults.standard.object(forKey: event.eventId + "/" + nrOfTimesPicChanged) as! NSData
            cell.backgroundView = UIImageView(image: UIImage(data: picData as Data))
            cell.backgroundView?.addBlurDark()
        } else {
            // remove old one from local database
            var amountChanged : Int = Int(nrOfTimesPicChanged)!
            amountChanged -= 1
            let lastEventChangedAmount = String(amountChanged)
            UserDefaults.standard.removeObject(forKey: event.eventId + "/" + lastEventChangedAmount)
            
            
            let eventPicRef = Storage.storage().reference(withPath: "pics/Events/" + event.eventId + "/eventPic.jpg")
            eventPicRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    print(error)
                    DispatchQueue.main.async {
                        let image = UIImage(named: "noImageAvailable.png")
                        let imageData : NSData = UIImagePNGRepresentation(image!)! as NSData
                        UserDefaults.standard.set(imageData, forKey: event.eventId + "/" + nrOfTimesPicChanged )
                        cell.backgroundView = UIImageView(image: UIImage(data: imageData as Data))
                        cell.backgroundView?.addBlurDark()
                    }
                } else {
                    let image = UIImage(data: data!)
                    let imageData : NSData = UIImagePNGRepresentation(image!)! as NSData
                    UserDefaults.standard.set(imageData, forKey: event.eventId + "/" + nrOfTimesPicChanged )
                    cell.backgroundView = UIImageView(image: UIImage(data: imageData as Data))
                    cell.backgroundView?.addBlurDark()
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var event : Event!
        if(tableView == myEventsList){
            event = myCreatedEvents[indexPath.section]
        } else {
            event = myJoinedEvents[indexPath.section]
        }

        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "OpenEventID") as! OpenEventViewControllerTry
        nextViewController.theEvent = event
        nextViewController.reloadProtocol = self
        let navController = UINavigationController(rootViewController: nextViewController)
        self.present(navController, animated: true, completion: nil)
    }
}
