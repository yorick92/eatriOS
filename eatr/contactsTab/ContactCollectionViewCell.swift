//
//  ContactCollectionViewCell.swift
//  eatr
//
//  Created by Yorick Bolster on 06/04/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit

class ContactCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var contactName: UILabel!
}
