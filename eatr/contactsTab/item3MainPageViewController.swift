//
//  item3MainPageViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 11/02/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Firebase
import FirebaseDatabase
import CoreData
import Floaty

class item3MainPageViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var facebookIdReference : DatabaseReference?
    var usersReference : DatabaseReference?
    var localContacts = [localContactStruct]()
    var localPresets = [localPresetStruct]()
    var localGroups = [localGroupStruct]()
    var userId : String?
    var context : NSManagedObjectContext!
    var refreshControl = UIRefreshControl()
    
    var theWidth : CGFloat!
    var heightUsed : CGFloat!
    var realViewHeight : CGFloat!
    var groupsExpanded = false
    var presetsExpanded = false
    var contactsExpanded = false
    
    var theScrollView : UIScrollView!
    
    var groupsView : UIView!
    var collexpGroupsButton : UIButton!
    var presetsView : UIView!
    var collexpPresetsButton : UIButton!
    var contactsView : UIView!
    var collexpContactsButton : UIButton!
    var findFriendsButton : UIButton!
    
    var groupViews = [UIView]()
    var presetViews = [UIView]()
    var contactViews = [UIView]()
    
    var spinnerView : UIView?
    var floaty : Floaty!
    
    var theLastScrollPosition = CGFloat(0)
    
    /*** HELPER FUNCTIONS ***/
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let theOffsetY = scrollView.contentOffset.y
        if(theLastScrollPosition < theOffsetY){
            if(theOffsetY > 0){
                floaty.fadeOut()
            }
        } else{
            if(theOffsetY < scrollView.contentSize.height - scrollView.frame.height){
                floaty.fadeIn()
            }
        }
        theLastScrollPosition = theOffsetY
    }
    
    func getContactsFromStorage(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Contact")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                for result in results as! [NSManagedObject]{
                    
                    let id = result.value(forKey: "userId") as! String
                    let name = result.value(forKey: "name") as! String
                    let favCuis = result.value(forKey: "favCuis") as! String
                    let favRest = result.value(forKey: "favRest") as! String
                    let gender = result.value(forKey: "gender") as! String
                    let picChangedAmount = result.value(forKey: "picChangedAmount") as! String
                    let timeLastChanged = result.value(forKey: "timeLastChanged") as! String
                    
                    self.localContacts.append(localContactStruct(id: id, name: name, favCuis: favCuis, favRest: favRest, gender: gender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
                }
            }
        } catch{
            print("error")
        }
    }
    
    func loadGroups(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Group")
        request.returnsObjectsAsFaults = false
        
        self.localGroups = [localGroupStruct]()
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                for result in results as! [NSManagedObject]{
                    let groupId = result.value(forKey: "id") as! String
                    let groupName = result.value(forKey: "name") as! String
                    let admin = result.value(forKey: "admin") as! String
                    let contactsSet = result.value(forKey: "contacts") as! NSSet
                    
                    var contacts = [localContactStruct]()
                    for personAny in contactsSet.allObjects{
                        if let person = personAny as? NSManagedObject{
                            let id = person.value(forKey: "userId") as! String
                            let name = person.value(forKey: "name") as! String
                            let favCuis = person.value(forKey: "favCuis") as! String
                            let favRest = person.value(forKey: "favRest") as! String
                            let gender = person.value(forKey: "gender") as! String
                            let picChangedAmount = person.value(forKey: "picChangedAmount") as! String
                            let timeLastChanged = person.value(forKey: "timeLastChanged") as! String
                            
                            contacts.append(localContactStruct(id: id, name: name, favCuis: favCuis, favRest: favRest, gender: gender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
                        }
                    }
                    self.localGroups.append(localGroupStruct(name: groupName, id: groupId, admin: admin, contacts: contacts))
                }
            }
        } catch{
            print("error")
        }
    }
    
    func loadPresets(){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Preset")
        request.returnsObjectsAsFaults = false
        
        self.localPresets = [localPresetStruct]()
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                for result in results as! [NSManagedObject]{
                    
                    let presetId = result.value(forKey: "id") as! String
                    let presetName = result.value(forKey: "name") as! String
                    let contactsSet = result.value(forKey: "contacts") as! NSSet
                    
                    var contacts = [localContactStruct]()
                    for personAny in contactsSet.allObjects{
                        if let person = personAny as? NSManagedObject{
                            let id = person.value(forKey: "userId") as! String
                            let name = person.value(forKey: "name") as! String
                            let favCuis = person.value(forKey: "favCuis") as! String
                            let favRest = person.value(forKey: "favRest") as! String
                            let gender = person.value(forKey: "gender") as! String
                            let picChangedAmount = person.value(forKey: "picChangedAmount") as! String
                            let timeLastChanged = person.value(forKey: "timeLastChanged") as! String
                            
                            contacts.append(localContactStruct(id: id, name: name, favCuis: favCuis, favRest: favRest, gender: gender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
                        }
                    }
                    
                    self.localPresets.append(localPresetStruct(name: presetName, id: presetId, contacts: contacts))
                }
            }
        } catch{
            print("error")
        }
    }
    
    func loadFriends(){
        self.localContacts = [localContactStruct]()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MyContact")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                for result in results as! [NSManagedObject]{
                    let theId = result.value(forKey: "userId") as! String
                    let theName = result.value(forKey: "name") as! String
                    let theFavCuis = result.value(forKey: "favCuis") as! String
                    let theFavRest = result.value(forKey: "favRest") as! String
                    let theGender = result.value(forKey: "gender") as! String
                    let picChangedAmount = result.value(forKey: "picChangedAmount") as! String
                    let timeLastChanged = result.value(forKey: "timeLastChanged") as! String
                    
                    self.localContacts.append(localContactStruct(id: theId, name: theName, favCuis: theFavCuis, favRest: theFavRest, gender: theGender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
                    
                }
            }
        } catch{
            print("error")
        }
    }
    
    func setUpDatabaseReferences(){
        facebookIdReference = Database.database().reference().child("facebookIds")
        usersReference = Database.database().reference().child("Users")
    }
    
    func setUpVariables(){
        userId = Auth.auth().currentUser!.uid
    }
    
    func disableIfNoInternet(){
        if(!Reachability.isConnectedToNetwork()){
            //TODO
        }
    }
    
    /*** CREATE VIEW FUNCTIONS ***/
    
    func setUpScrollView(){
        realViewHeight = view.frame.height - (self.tabBarController?.tabBar.frame.size.height)! - 20
        theScrollView = UIScrollView(frame: CGRect(x: 0, y: 20, width: theWidth, height: realViewHeight))
        theScrollView.backgroundColor = UIColor.white
        theScrollView.contentSize = CGSize(width: theWidth, height: realViewHeight)
        theScrollView.delegate = self
        view.addSubview(theScrollView)
    }
    
    func makeGroupsPart(){
        if(groupsView != nil){
            groupsView.removeFromSuperview()
        }
        
        let totalWidth = theScrollView.frame.width
        let height = CGFloat(60)
        groupsView = UIView(frame: CGRect(x: 0, y: 0, width: totalWidth, height: height))
        groupsView.backgroundColor = UIColor.white
        groupsView.addShadow()
        
        let localLabel = UILabel(frame: CGRect(x: 10, y: 0, width: totalWidth/2 - 10, height: height))
        localLabel.text = "Groups (" + String(localGroups.count) + ")"
        localLabel.textColor = UIColor.red
        localLabel.font = UIFont.systemFont(ofSize: 18)
        groupsView.addSubview(localLabel)
        
        collexpGroupsButton = UIButton(frame: CGRect(x: totalWidth/2, y: 0, width: totalWidth/2, height: height))
        collexpGroupsButton.addTarget(self, action: #selector(expandGroups), for: .touchUpInside)
        let tintedImage = UIImage(named: "expand_icon.png")?.withRenderingMode(.alwaysTemplate)
        collexpGroupsButton.setImage(tintedImage, for: .normal)
        collexpGroupsButton.imageView?.contentMode = .scaleAspectFit
        collexpGroupsButton.tintColor = UIColor.red
        collexpGroupsButton.imageEdgeInsets = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: -totalWidth/3)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(expandGroups))
        localLabel.isUserInteractionEnabled = true
        localLabel.addGestureRecognizer(tapGesture)
        
        groupsView.addSubview(collexpGroupsButton)
        theScrollView.addSubview(groupsView)
    }
    
    func makePresetsPart(){
        if(presetsView != nil){
            presetsView.removeFromSuperview()
        }
        
        let totalWidth = theScrollView.frame.width
        let height = CGFloat(60)
        presetsView = UIView(frame: CGRect(x: 0, y: 80, width: totalWidth, height: height))
        presetsView.backgroundColor = UIColor.white
        presetsView.addShadow()
        
        let localLabel = UILabel(frame: CGRect(x: 10, y: 0, width: totalWidth/2 - 10, height: height))
        localLabel.text = "Presets (" + String(localPresets.count) + ")"
        localLabel.textColor = UIColor.red
        localLabel.font = UIFont.systemFont(ofSize: 18)
        presetsView.addSubview(localLabel)
        
        collexpPresetsButton = UIButton(frame: CGRect(x: totalWidth/2, y: 0, width: totalWidth/2, height: height))
        collexpPresetsButton.addTarget(self, action: #selector(expandPresets), for: .touchUpInside)
        let tintedImage = UIImage(named: "expand_icon.png")?.withRenderingMode(.alwaysTemplate)
        collexpPresetsButton.setImage(tintedImage, for: .normal)
        collexpPresetsButton.imageView?.contentMode = .scaleAspectFit
        collexpPresetsButton.tintColor = UIColor.red
        collexpPresetsButton.imageEdgeInsets = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: -totalWidth/3)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(expandPresets))
        localLabel.isUserInteractionEnabled = true
        localLabel.addGestureRecognizer(tapGesture)
        
        presetsView.addSubview(collexpPresetsButton)
        theScrollView.addSubview(presetsView)
    }
    
    func makeContactsPart(){
        if(contactsView != nil){
            contactsView.removeFromSuperview()
        }
        
        let totalWidth = theScrollView.frame.width
        let height = CGFloat(60)
        contactsView = UIView(frame: CGRect(x: 0, y: 160, width: totalWidth, height: height))
        contactsView.backgroundColor = UIColor.white
        contactsView.addShadow()
        
        let localLabel = UILabel(frame: CGRect(x: 10, y: 0, width: totalWidth/2 - 10, height: height))
        localLabel.text = "Contacts (" + String(localContacts.count) + ")"
        localLabel.textColor = UIColor.red
        localLabel.font = UIFont.systemFont(ofSize: 18)
        contactsView.addSubview(localLabel)
        
        collexpContactsButton = UIButton(frame: CGRect(x: totalWidth/2, y: 0, width: totalWidth/2, height: height))
        collexpContactsButton.addTarget(self, action: #selector(expandContacts), for: .touchUpInside)
        let tintedImage = UIImage(named: "expand_icon.png")?.withRenderingMode(.alwaysTemplate)
        collexpContactsButton.setImage(tintedImage, for: .normal)
        collexpContactsButton.imageView?.contentMode = .scaleAspectFit
        collexpContactsButton.tintColor = UIColor.red
        collexpContactsButton.imageEdgeInsets = UIEdgeInsets(top: 15, left: 0, bottom: 15, right: -totalWidth/3)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(expandContacts))
        localLabel.isUserInteractionEnabled = true
        localLabel.addGestureRecognizer(tapGesture)
        
        contactsView.addSubview(collexpContactsButton)
        theScrollView.addSubview(contactsView)
    }
    
    func makeFindFriendsPart(){
        if(findFriendsButton != nil){
            findFriendsButton.removeFromSuperview()
        }
        
        let totalWidth = theScrollView.frame.width
        let height = CGFloat(60)
        findFriendsButton = UIButton(frame: CGRect(x: 0, y: 240, width: totalWidth, height: height))
        findFriendsButton.backgroundColor = UIColor.white
        findFriendsButton.setTitle("Connect to friends", for: .normal)
        findFriendsButton.setTitleColor(UIColor.red, for: .normal)
        findFriendsButton.addTarget(self, action: #selector(findFriends), for: .touchUpInside)
        findFriendsButton.addShadow()
        theScrollView.addSubview(findFriendsButton)
    }
    
    /*** VIEWDIDLOAD ***/
    
    func makeFloaty(){
        if(floaty != nil){
            floaty.removeFromSuperview()
        }
        floaty = Floaty(frame: CGRect(x: view.frame.width-70, y: view.frame.height-(tabBarController?.tabBar.frame.height)! - 70, width: 56, height: 56))
        floaty.buttonColor = UIColor(red: 191, green: 42, blue: 42)
        view.addSubview(floaty)
        
        floaty.addItem("Make group", icon: UIImage(named: "group_icon.png")!, handler: { item in
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MakeGroupID") as! MakeGroupViewController
            let navController = UINavigationController(rootViewController: nextViewController)
            self.present(navController, animated: true, completion: nil)
        })
        
        floaty.addItem("Make preset", icon: UIImage(named: "plus_icon.png")!, handler: { item in
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MakePresetID") as! MakePresetViewController
            let navController = UINavigationController(rootViewController: nextViewController)
            self.present(navController, animated: true, completion: nil)
        })
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        theWidth = view.frame.width
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        
//        addTopViewColor(colorNavBar: true)
        setUpScrollView()
        
        setUpDatabaseReferences()
        setUpVariables()
        disableIfNoInternet()
        makeFloaty()
        
        refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: .valueChanged)
        theScrollView.refreshControl = refreshControl
    }
    
    func reload(){
        loadGroups()
        loadPresets()
        loadFriends()
        
        groupsExpanded = false
        presetsExpanded = false
        contactsExpanded = false
        heightUsed = 300
        
        makeGroupsPart()
        makePresetsPart()
        makeContactsPart()
        makeFindFriendsPart()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        reload()
    }
    
    /*** ACTIONS ***/
    
    @objc func refresh(sender:AnyObject) {
        reload()
        self.refreshControl.endRefreshing()
    }
    
    @objc func expandGroups(){
        var imageName = ""
        var valueChanged : CGFloat = 0
        let nrOfGroups = localGroups.count
        if (groupsExpanded){
            groupsExpanded = false
            imageName = "expand_icon.png"
            valueChanged = CGFloat(nrOfGroups * 50 * -1)
            
            for view in groupViews{
                view.removeFromSuperview()
            }
            
        } else {
            groupsExpanded = true
            imageName = "collapse_icon.png"
            valueChanged = CGFloat(nrOfGroups * 50)
            
            var counter = 0;
            for group in localGroups {
                let localGroupView = UIView(frame: CGRect(x: 0, y: 60 + counter * 50, width: Int(theWidth), height: 50))
                
                let groupImageView = UIImageView(frame: CGRect(x: 5, y: 5, width: 40, height: 40))
                groupImageView.image = UIImage(named: "group_icon.png")
                groupImageView.contentMode = .scaleAspectFill
                groupImageView.makePortrait()
                
                let nameLabel = UILabel(frame: CGRect(x: 65, y: 5, width: theWidth - 40, height: 30))
                nameLabel.font = UIFont.systemFont(ofSize: 16)
                nameLabel.text = group.name
                
                let groupMembersLabel = UILabel(frame: CGRect(x: 65, y: 25, width: theWidth - 100, height: 20))
                groupMembersLabel.font = UIFont.systemFont(ofSize: 12)
                
                var groupMembersString = ""
                for friend in group.contacts!{
                    groupMembersString += friend.name! + ", "
                }
                
                groupMembersLabel.text = groupMembersString
                
                localGroupView.addSubview(groupImageView)
                localGroupView.addSubview(nameLabel)
                localGroupView.addSubview(groupMembersLabel)
                groupsView.addSubview(localGroupView)
                groupViews.append(localGroupView)
                
                let tappy = MyTapGesture(target: self, action: #selector(self.tappedGroup))
                localGroupView.addGestureRecognizer(tappy)
                tappy.userId = group.id!
                
                counter += 1
            }
        }
        
        let tintedImage = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
        collexpGroupsButton.setImage(tintedImage, for: .normal)
        groupsView.frame = CGRect(x: groupsView.frame.origin.x, y: groupsView.frame.origin.y, width: groupsView.frame.width, height: groupsView.frame.height + valueChanged)
        heightUsed = heightUsed + valueChanged
        theScrollView.contentSize = CGSize(width: theWidth, height: max(heightUsed, realViewHeight))
        presetsView.frame = CGRect(x: presetsView.frame.origin.x, y: presetsView.frame.origin.y + valueChanged, width: presetsView.frame.width, height: presetsView.frame.height)
        contactsView.frame = CGRect(x: contactsView.frame.origin.x, y: contactsView.frame.origin.y + valueChanged, width: contactsView.frame.width, height: contactsView.frame.height)
        findFriendsButton.frame = CGRect(x: findFriendsButton.frame.origin.x, y: findFriendsButton.frame.origin.y + valueChanged, width: findFriendsButton.frame.width, height: findFriendsButton.frame.height)
    }
    
    @objc func expandPresets(){
        var imageName = ""
        var valueChanged : CGFloat = 0
        let nrOfPresets = localPresets.count
        if (presetsExpanded){
            presetsExpanded = false
            imageName = "expand_icon.png"
            valueChanged = CGFloat(nrOfPresets * 50 * -1)
            
            for view in presetViews{
                view.removeFromSuperview()
            }
            
        } else {
            presetsExpanded = true
            imageName = "collapse_icon.png"
            valueChanged = CGFloat(nrOfPresets * 50)
            
            var counter = 0;
            for preset in localPresets {
                let localPresetView = UIView(frame: CGRect(x: 0, y: 60 + counter * 50, width: Int(theWidth), height: 50))
                
                let presetImageView = UIImageView(frame: CGRect(x: 5, y: 5, width: 40, height: 40))
                presetImageView.image = UIImage(named: "group_icon.png")
                presetImageView.contentMode = .scaleAspectFill
                presetImageView.makePortrait()
                
                let nameLabel = UILabel(frame: CGRect(x: 65, y: 5, width: theWidth - 40, height: 30))
                nameLabel.font = UIFont.systemFont(ofSize: 16)
                nameLabel.text = preset.name
                
                let presetMembersLabel = UILabel(frame: CGRect(x: 65, y: 25, width: theWidth - 100, height: 20))
                presetMembersLabel.font = UIFont.systemFont(ofSize: 12)
                
                var presetMembersString = ""
                for friend in preset.contacts!{
                    presetMembersString += friend.name! + ", "
                }
                
                presetMembersLabel.text = presetMembersString
                
                localPresetView.addSubview(presetImageView)
                localPresetView.addSubview(nameLabel)
                localPresetView.addSubview(presetMembersLabel)
                presetsView.addSubview(localPresetView)
                presetViews.append(localPresetView)
                
                let tappy = MyTapGesture(target: self, action: #selector(self.tappedPreset))
                localPresetView.addGestureRecognizer(tappy)
                tappy.userId = preset.id!
                
                counter += 1
            }
        }
        
        let tintedImage = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
        collexpPresetsButton.setImage(tintedImage, for: .normal)
        presetsView.frame = CGRect(x: presetsView.frame.origin.x, y: presetsView.frame.origin.y, width: presetsView.frame.width, height: presetsView.frame.height + valueChanged)
        heightUsed = heightUsed + valueChanged
        theScrollView.contentSize = CGSize(width: theWidth, height: max(heightUsed, realViewHeight))
        contactsView.frame = CGRect(x: contactsView.frame.origin.x, y: contactsView.frame.origin.y + valueChanged, width: contactsView.frame.width, height: contactsView.frame.height)
        findFriendsButton.frame = CGRect(x: findFriendsButton.frame.origin.x, y: findFriendsButton.frame.origin.y + valueChanged, width: findFriendsButton.frame.width, height: findFriendsButton.frame.height)
    }
    
    @objc func expandContacts(){
        var imageName = ""
        var valueChanged : CGFloat = 0
        let nrOfContacts = localContacts.count
        if (contactsExpanded){
            contactsExpanded = false
            imageName = "expand_icon.png"
            valueChanged = CGFloat(nrOfContacts * 50 * -1)
            
            for view in contactViews{
                view.removeFromSuperview()
            }
            
        } else {
            contactsExpanded = true
            imageName = "collapse_icon.png"
            valueChanged = CGFloat(nrOfContacts * 50)
            
            var counter = 0;
            for contact in localContacts {
                let localContactView = UIView(frame: CGRect(x: 0, y: 60 + counter * 50, width: Int(theWidth), height: 50))
                
                let contactImageView = UIImageView(frame: CGRect(x: 5, y: 5, width: 40, height: 40))
                contactImageView.image = UIImage(named: "profile_icon.png")
                contactImageView.contentMode = .scaleAspectFill
                contactImageView.makePortrait()
                
                getPicDataForUser(theUserId: contact.id!, picChangedAmount: contact.picChangedAmount!, completionHandler: {picData in
                    contactImageView.image = UIImage(data: picData as Data)
                })
                
                let nameLabel = UILabel(frame: CGRect(x: 65, y: 5, width: theWidth - 40, height: 30))
                nameLabel.font = UIFont.systemFont(ofSize: 16)
                nameLabel.text = contact.name
                
                let favCuisLabel = UILabel(frame: CGRect(x: 65, y: 25, width: theWidth - 100, height: 20))
                favCuisLabel.font = UIFont.systemFont(ofSize: 12)
                
                let favCuisString = "Fav. Cuisine: " + contact.favCuis!
                favCuisLabel.text = favCuisString
                
                localContactView.addSubview(contactImageView)
                localContactView.addSubview(nameLabel)
                localContactView.addSubview(favCuisLabel)
                contactsView.addSubview(localContactView)
                contactViews.append(localContactView)
                
                let tappy = MyTapGesture(target: self, action: #selector(self.tappedContact))
                localContactView.addGestureRecognizer(tappy)
                tappy.userId = contact.id!
                
                counter += 1
            }
        }
        
        let tintedImage = UIImage(named: imageName)?.withRenderingMode(.alwaysTemplate)
        collexpContactsButton.setImage(tintedImage, for: .normal)
        contactsView.frame = CGRect(x: contactsView.frame.origin.x, y: contactsView.frame.origin.y, width: contactsView.frame.width, height: contactsView.frame.height + valueChanged)
        heightUsed = heightUsed + valueChanged
        theScrollView.contentSize = CGSize(width: theWidth, height: max(heightUsed, realViewHeight))
        findFriendsButton.frame = CGRect(x: findFriendsButton.frame.origin.x, y: findFriendsButton.frame.origin.y + valueChanged, width: findFriendsButton.frame.width, height: findFriendsButton.frame.height)
    }
    
    @objc func tappedGroup(sender: MyTapGesture){
        let theGroup = localGroups[localGroups.index(where: {$0.id == sender.userId})!]
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditGroupID") as! EditGroupViewController
        nextViewController.group = theGroup
        let navController = UINavigationController(rootViewController: nextViewController)
        self.present(navController, animated: true, completion: nil)
    }

    @objc func tappedPreset(sender: MyTapGesture){
        let thePreset = localPresets[localPresets.index(where: {$0.id == sender.userId})!]
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditPresetID") as! EditPresetViewController
        nextViewController.preset = thePreset
        let navController = UINavigationController(rootViewController: nextViewController)
        self.present(navController, animated: true, completion: nil)
    }
    
    @objc func tappedContact(sender: MyTapGesture){
        let theContact = localContacts[localContacts.index(where: {$0.id == sender.userId})!]
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileOtherID") as! OpenOtherProfileViewController
        nextViewController.theContact = theContact
        let navController = UINavigationController(rootViewController: nextViewController)
        self.present(navController, animated: true, completion: nil)
    }
    
    @objc func findFriends(){
        spinnerView = UIViewController.displaySpinner(onView: self.view)
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "friends"]).start { (connection, result, error) in
            do{
                let jsonData = try JSONSerialization.data(withJSONObject: result!, options: .prettyPrinted)
                let facebookFriends = try JSONDecoder().decode(facebookUserInfo.self, from: jsonData)
                
                var friendlist = [String : String]()
                if(facebookFriends.friends != nil){
                    let myGroup = DispatchGroup()
                    for friend in (facebookFriends.friends?.data)!{
                        myGroup.enter()
                        self.facebookIdReference?.child(friend.id!).observeSingleEvent(of: .value, with: { (snapshot) in
                            if let firebaseId = snapshot.value as? String{
                                friendlist[firebaseId] = friend.name!
                            }
                            myGroup.leave()
                        })
                    }
                    myGroup.notify(queue: .main){
                        print("goes here")
                        self.usersReference?.child(self.userId!).child("facebookFriends").setValue(friendlist){ (error, snapshot) in
                            if(self.spinnerView != nil){
                                UIViewController.removeSpinner(spinner: self.spinnerView!)
                            }
                            self.showToast(message: "Your friend list has been updated!")
                            self.reload()
                        }
                    }
                }
                
//                var friendslist = [[String : String]]()
//                if(facebookFriends.friends != nil){
//                    let myGroup = DispatchGroup()
//                    for friend in (facebookFriends.friends?.data)!{
//                        myGroup.enter()
//                        self.facebookIdReference?.child(friend.id!).observeSingleEvent(of: .value, with: { (snapshot) in
//                            if let firebaseId = snapshot.value as? String{
//                                friendslist.append(["name": friend.name!, "id" : firebaseId])
//                            }
//                            myGroup.leave()
//                        })
//                    }
//                    myGroup.notify(queue: .main){
//                        self.usersReference?.child(self.userId!).child("facebookFriends").setValue(friendslist){ (error, snapshot) in
//                            if(self.spinnerView != nil){
//                                UIViewController.removeSpinner(spinner: self.spinnerView!)
//                            }
//                            self.showToast(message: "Your friend list has been updated!")
//                            self.reload()
//                        }
//                    }
//                }
            } catch{
                print("facebookgraphrequesterror at json decoder at get friends")
            }
        }
    }
    
    @IBAction func makeGroup(_ sender: Any) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MakeGroupID") as! MakeGroupViewController
        let navController = UINavigationController(rootViewController: nextViewController)
        self.present(navController, animated: true, completion: nil)
    }
    
    @IBAction func makePreset(_ sender: Any) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MakePresetID") as! MakePresetViewController
        let navController = UINavigationController(rootViewController: nextViewController)
        self.present(navController, animated: true, completion: nil)
    }
    
    @IBAction func getFriendsButton(_ sender: Any) { // TODO set a max per timeframe on this
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "friends"]).start { (connection, result, error) in
            do{
                let jsonData = try JSONSerialization.data(withJSONObject: result!, options: .prettyPrinted)
                let facebookFriends = try JSONDecoder().decode(facebookUserInfo.self, from: jsonData)
                
                var friendlist = [String : String]()
                if(facebookFriends.friends != nil){
                    let myGroup = DispatchGroup()
                    for friend in (facebookFriends.friends?.data)!{
                        myGroup.enter()
                        self.facebookIdReference?.child(friend.id!).observeSingleEvent(of: .value, with: { (snapshot) in
                            if let firebaseId = snapshot.value as? String{
                                friendlist[firebaseId] = friend.name!
                            }
                            myGroup.leave()
                        })
                    }
                    print(friendlist)
                    myGroup.notify(queue: .main){
                        print("goes here")
                        self.usersReference?.child(self.userId!).child("facebookFriends").setValue(friendlist)
                    }
                }
            } catch{
                print("facebookgraphrequesterror at json decoder at get friends")
            }
        }
    }
    
    /*** TABLEVIEWS ***/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            return localContacts.count
        }
        else if(section == 1){
            return localPresets.count
        } else{
            return localGroups.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "contactCellID") as! ContactCellTableViewCell
            cell.nameLbl.text = localContacts[indexPath.row].name
            cell.profilePic.image = UIImage(named: "profile_icon.png")
            cell.cuisineLbl.text = "Cuisine: " + localContacts[indexPath.row].favCuis!
            return cell
        }
        else if(indexPath.section == 1){
            let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "contactsCell")
            cell.textLabel?.text = localPresets[indexPath.row].name
            return cell
        } else{
            let cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "contactsCell")
            cell.textLabel?.text = localGroups[indexPath.row].name
            return cell
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if(section == 0){
            return "contacts"
        } else if(section == 1){
            return "presets"
        } else{
            return "groups"
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 0){
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileOtherID") as! OpenOtherProfileViewController
            nextViewController.theContact = localContacts[indexPath.row]
            let navController = UINavigationController(rootViewController: nextViewController)
            self.present(navController, animated: true, completion: nil)
        } else if (indexPath.section == 1){
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditPresetID") as! EditPresetViewController
            nextViewController.preset = localPresets[indexPath.row]
            let navController = UINavigationController(rootViewController: nextViewController)
            self.present(navController, animated: true, completion: nil)
        } else if (indexPath.section == 2){
            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "EditGroupID") as! EditGroupViewController
            nextViewController.group = localGroups[indexPath.row]
            let navController = UINavigationController(rootViewController: nextViewController)
            self.present(navController, animated: true, completion: nil)
        }
    }
}
