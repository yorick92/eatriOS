//
//  MakePresetViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 26/03/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import FirebaseDatabase
import CoreData
import Floaty

class MakePresetViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UICollectionViewDataSource, UICollectionViewDelegate {
    var usersReference : DatabaseReference?
    var localFacebookFriends = [localContactStruct]()
    var localFacebookFriendsCurrent = [localContactStruct]()
    var friendsForPreset = [localContactStruct]()
    var userId : String?
    var moreThanOneContact = false
    var hasAName = false
    var context : NSManagedObjectContext!
    var floaty : Floaty!
    

    @IBOutlet weak var contactsList: UITableView!
    @IBOutlet weak var addedContactsCollection: UICollectionView!
    @IBOutlet weak var searchBarAddContacts: UISearchBar!
    
    func setUpNavigationController(){
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "< back", style: .plain, target: self, action: #selector(goBack))
    }
    
    func setUpDatabaseReferences(){
        usersReference = Database.database().reference().child("Users")
    }
    
    func setUpVariables(){
        userId = Auth.auth().currentUser!.uid
    }
    
    func setUpViews(){
        let theWidth = view.frame.width
        let startOfArea = 20 + (navigationController?.navigationBar.frame.height)! + 10
        let addedContactsCollectionView = UIView(frame: CGRect(x: 5, y: startOfArea, width: theWidth-10, height: 60))
        addedContactsCollectionView.addShadowBottom()
        addedContactsCollection.frame = CGRect(x: 0, y: 0, width: theWidth-10, height: 60)
        addedContactsCollection.removeFromSuperview()
        addedContactsCollectionView.addSubview(addedContactsCollection)
        view.addSubview(addedContactsCollectionView)
        
        searchBarAddContacts.frame = CGRect(x: 5, y: startOfArea+80, width: theWidth-10, height: 50)
        contactsList.frame = CGRect(x: 5, y: startOfArea + 130, width: theWidth-10, height: view.frame.height - startOfArea - 80)
    }
    
    func loadFriends(){
        self.localFacebookFriends = [localContactStruct]()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MyContact")
        request.returnsObjectsAsFaults = false
        do {
            let results = try self.context.fetch(request)
            print(results.count)
            if results.count > 0{
                for result in results as! [NSManagedObject]{
                    let theId = result.value(forKey: "userId") as! String
                    let theName = result.value(forKey: "name") as! String
                    let theFavCuis = result.value(forKey: "favCuis") as! String
                    let theFavRest = result.value(forKey: "favRest") as! String
                    let theGender = result.value(forKey: "gender") as! String
                    let picChangedAmount = result.value(forKey: "picChangedAmount") as! String
                    let timeLastChanged = result.value(forKey: "timeLastChanged") as! String
                    
                    self.localFacebookFriends.append(localContactStruct(id: theId, name: theName, favCuis: theFavCuis, favRest: theFavRest, gender: theGender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
                    
                }
            }
        } catch{
            print("error")
        }
        self.localFacebookFriendsCurrent = self.localFacebookFriends
        self.contactsList.reloadData()
    }
    
    func makeFloaty(){
        if floaty != nil{
            floaty.removeFromSuperview()
        }
        floaty = Floaty(frame: CGRect(x: view.frame.width-70, y: view.frame.height - 70, width: 56, height: 56))
        floaty.buttonColor = UIColor(red: 191, green: 42, blue: 42)
        view.addSubview(floaty)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(goToNaming))
        floaty.addGestureRecognizer(tapGesture)
        floaty.isUserInteractionEnabled = true
        floaty.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
        self.title = "Pick contacts for preset"
        addTopViewColor(colorNavBar: true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        makeFloaty()
        setUpViews()
        setUpNavigationController()
        setUpDatabaseReferences()
        setUpVariables()
        loadFriends()
    }
    
    @objc func goToNaming(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "NamePresetID") as! NamePresetViewController
        vc.friendsForPreset = friendsForPreset
        vc.userId = userId
        vc.usersReference = usersReference
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return localFacebookFriendsCurrent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCellID") as! ContactCellTableViewCell
        let contact = localFacebookFriendsCurrent[indexPath.row]
        cell.nameLbl.text = contact.name
        cell.cuisineLbl.text = "Cuisine: " + localFacebookFriendsCurrent[indexPath.row].favCuis!
        cell.profilePic.image = UIImage(named: "profile_icon.png")
        cell.profilePic.makePortrait()
        getPicDataForUser(theUserId: contact.id!, picChangedAmount: contact.picChangedAmount!, completionHandler: {picData in
            cell.profilePic.image = UIImage(data: picData as Data)
        })
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        friendsForPreset.append(localFacebookFriendsCurrent[indexPath.row])
        localFacebookFriendsCurrent.remove(at: indexPath.row)
        localFacebookFriends.remove(at: indexPath.row)
        
        if(friendsForPreset.count >= 2){
            floaty.isHidden = false
        } else{
            floaty.isHidden = true
        }

        contactsList.reloadData()
        addedContactsCollection.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return friendsForPreset.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContactCell", for: indexPath) as! ContactCollectionViewCell
        let contact = friendsForPreset[indexPath.row]
        cell.contactName.text = contact.name
        cell.contactImage.image = UIImage(named: "profile_icon.png")
        cell.contactImage.makePortrait()
        getPicDataForUser(theUserId: contact.id!, picChangedAmount: contact.picChangedAmount!, completionHandler: {picData in
            cell.contactImage.image = UIImage(data: picData as Data)
        })
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        localFacebookFriendsCurrent.append(friendsForPreset[indexPath.row])
        localFacebookFriends.append(friendsForPreset[indexPath.row])
        friendsForPreset.remove(at: indexPath.row)
        
        if(friendsForPreset.count >= 2){
            floaty.isHidden = false
        } else{
            floaty.isHidden = true
        }
        
        contactsList.reloadData()
        addedContactsCollection.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            localFacebookFriendsCurrent = localFacebookFriends
            contactsList.reloadData()
            return}
        
        localFacebookFriendsCurrent = localFacebookFriends.filter({ (localFriend) -> Bool in
            guard let text = searchBar.text else {return false}
            return (localFriend.name?.lowercased().contains(text.lowercased()))!
        })
        
        contactsList.reloadData()
    }
    
    @objc func goBack(){
        self.dismiss(animated: true, completion: nil)
    }
}

