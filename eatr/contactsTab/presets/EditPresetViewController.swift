//
//  EditPresetViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 26/03/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import FirebaseDatabase
import CoreData

class EditPresetViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var usersReference : DatabaseReference?
    var preset : localPresetStruct!
    var friendsForPreset = [localContactStruct]()
    var userId : String?
    var context : NSManagedObjectContext!

    var changeNameButton : UIButton!
    var deletePresetButton : UIButton!
    
    @IBOutlet weak var presetList: UITableView!
    @IBOutlet weak var presetNameTextField: UITextField!
    @IBOutlet weak var editPresetButton: UIButton!
    
    func setUpNavigationController(){
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "< back", style: .plain, target: self, action: #selector(goBack))
    }
    
    func setUpDatabaseReferences(){
        usersReference = Database.database().reference().child("Users")
    }
    
    func setUpOutlets(){
        presetNameTextField.text = preset.name!
        changeNameButton.isEnabled = true
    }
    
    func setUpVariables(){
        userId = Auth.auth().currentUser!.uid
    }
    
    func presetExists(presetId : String)->NSManagedObject?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Preset")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "id") as! String == presetId}){
                    return result as? NSManagedObject;
                } else {
                    return nil
                }
            }
        } catch{
            return nil
        }
        return nil
    }
    
    func loadPreset(){
        let presetObject = presetExists(presetId: preset.id!)
        
        let presetId = presetObject?.value(forKey: "id") as! String
        let presetName = presetObject?.value(forKey: "name") as! String
        let contactsSet = presetObject?.value(forKey: "contacts") as! NSSet
        
        var contacts = [localContactStruct]()
        for personAny in contactsSet.allObjects{
            if let person = personAny as? NSManagedObject{
                let id = person.value(forKey: "userId") as! String
                let name = person.value(forKey: "name") as! String
                let favCuis = person.value(forKey: "favCuis") as! String
                let favRest = person.value(forKey: "favRest") as! String
                let gender = person.value(forKey: "gender") as! String
                let picChangedAmount = person.value(forKey: "picChangedAmount") as! String
                let timeLastChanged = person.value(forKey: "picChangedAmount") as! String
                
                contacts.append(localContactStruct(id: id, name: name, favCuis: favCuis, favRest: favRest, gender: gender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
            }
        }
        
        self.friendsForPreset = contacts
        
        preset = localPresetStruct(name: presetName, id: presetId, contacts: contacts)
        self.presetList.reloadData()
    }
    
    func disableIfNoInternet(){
        if(!Reachability.isConnectedToNetwork()){
            changeNameButton.isEnabled = false
            editPresetButton.isEnabled = false
//            deletePresetButton.isEnabled = false
        }
    }
    
    func addViews(){
        let theWidth = view.frame.width
        
        presetList.frame = CGRect(x: presetList.frame.origin.x, y: presetList.frame.origin.y, width: presetList.frame.width, height: presetList.frame.height - 40)
        
        let placeButtonAt = presetList.frame.origin.y + presetList.frame.height + 10
        
        changeNameButton = UIButton(frame: CGRect(x: 10, y: placeButtonAt, width: theWidth-20, height: 30))
        changeNameButton.backgroundColor = UIColor.white
        changeNameButton.addTarget(self, action: #selector(changeName), for: .touchUpInside)
        changeNameButton.addShadowBottom()
        let icon = UIImage(named: "pencil_icon.png")
        changeNameButton.setImage(icon, for: .normal)
        changeNameButton.imageView?.contentMode = .scaleAspectFit
        let toLeft = changeNameButton.frame.width/2
        changeNameButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: -toLeft, bottom: 10, right: 0)
        changeNameButton.setTitleColor(UIColor.black, for: .normal)
        changeNameButton.setTitle("Change name", for: .normal)
        changeNameButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: (-theWidth/2)*1.3 , bottom: 0, right: 0)
        
        deletePresetButton = UIButton(frame: CGRect(x: 10, y: placeButtonAt + 40, width: theWidth-20, height: 30))
        deletePresetButton.backgroundColor = UIColor.white
        deletePresetButton.addTarget(self, action: #selector(deletePreset), for: .touchUpInside)
        deletePresetButton.addShadowBottom()
        let icon2 = UIImage(named: "cross_icon.png")
        deletePresetButton.setImage(icon2, for: .normal)
        deletePresetButton.imageView?.contentMode = .scaleAspectFit
        let toLeft2 = deletePresetButton.frame.width/2
        deletePresetButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: -toLeft2, bottom: 10, right: 0)
        deletePresetButton.setTitleColor(UIColor.red, for: .normal)
        deletePresetButton.setTitle("Delete preset", for: .normal)
        deletePresetButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: (-theWidth/2)*1.2 , bottom: 0, right: 0)

        view.addSubview(changeNameButton)
        view.addSubview(deletePresetButton)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
        self.title = preset.name!
        addTopViewColor(colorNavBar: true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        
        addViews()
        setUpNavigationController()
        setUpDatabaseReferences()
        setUpOutlets()
        setUpVariables()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadPreset()
    }
    
    func deleteAllData(entity: String)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        fetchRequest.returnsObjectsAsFaults = false
        
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            for managedObject in results
            {
                managedContext.delete(managedObject as! NSManagedObject)
            }
        } catch let error as NSError {
            print("Detele all data in \(entity) error : \(error) \(error.userInfo)")
        }
        
        do{
            try managedContext.save()
        } catch{
            print("could not save deletions into local storage")
        }
    }
    
    func deletePresetLocally(presetId : String)
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Preset")
        fetchRequest.returnsObjectsAsFaults = false
        
        do
        {
            let results = try managedContext.fetch(fetchRequest)
            for managedObject in results
            {
                if (managedObject as! NSManagedObject).value(forKey: "id") as! String == presetId{
                    print(presetId)
                    print((managedObject as! NSManagedObject).value(forKey: "id") as! String)
                    managedContext.delete(managedObject as! NSManagedObject)
                }
            }
        } catch _ as NSError {
            print("delete preset locally went wrong")
        }
        
        
        do{
            try managedContext.save()
        } catch{
            print("could not save deletions into local storage")
        }
    }
    
    @objc func deletePreset(){
        let alert = UIAlertController(title: "", message: "Delete this preset?" , preferredStyle: .alert)
        let dateInMs = Date().millisecondsSince1970
        let action1 = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            self.usersReference?.child(self.userId!).child("presets").child(self.preset.id!).removeValue(){ (snapshot, error) in
                self.usersReference?.child(self.userId!).child("timeLastChanged").setValue(dateInMs){ (snapshot, error) in
                    self.deletePresetLocally(presetId: self.preset.id!)
                    self.dismiss(animated: true, completion: nil)
                }
            }
        })
        alert.addAction(action1)
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func addContact(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddMoreContactsPresetID") as! AddMoreToPresetViewController
        vc.friendsForPresetAdded = friendsForPreset
        vc.userId = userId
        vc.usersReference = usersReference
        vc.preset = preset
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func textFieldChanged(_ sender: UITextField) {
        if(sender.text?.isEmpty)!{
            changeNameButton.isEnabled = false
        } else {
            changeNameButton.isEnabled = true
        }
    }
    
    @objc func changeName(){
        self.usersReference?.child(self.userId!).child("presets").child(self.preset.id!).child("name").setValue(presetNameTextField.text!)
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func updatePreset(_ sender: Any) {
        self.usersReference?.child(self.userId!).child("presets").child(self.preset.id!).child("name").setValue(presetNameTextField.text!)
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friendsForPreset.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCellID") as! ContactCellTableViewCell
        let contact = friendsForPreset[indexPath.row]
        cell.nameLbl.text = contact.name
        cell.cuisineLbl.text = "Cuisine: " + friendsForPreset[indexPath.row].favCuis!
        cell.profilePic.image = UIImage(named: "profile_icon.png")
        cell.profilePic.makePortrait()
        getPicDataForUser(theUserId: contact.id!, picChangedAmount: contact.picChangedAmount!, completionHandler: {picData in
            cell.profilePic.image = UIImage(data: picData as Data)
        })
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var alert : UIAlertController!
        if(friendsForPreset.count > 2){
            alert = UIAlertController(title: "", message: "Delete " + friendsForPreset[indexPath.row].name! + " from preset or view profile?" , preferredStyle: .alert)
        } else{
            alert = UIAlertController(title: "", message: "Cannot have less than 2 people in a preset.\n View profile " + friendsForPreset[indexPath.row].name! + "'s profile?" , preferredStyle: .alert)
        }
        
        let action1 = UIAlertAction(title: "View profile", style: .default, handler: { (action) -> Void in
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileOtherID") as! OpenOtherProfileViewController
            vc.theContact = self.friendsForPreset[indexPath.row]
            self.navigationController?.pushViewController(vc, animated: true)
        })
        alert.addAction(action1)
        
        if(friendsForPreset.count > 2){
            let action2 = UIAlertAction(title: "Delete", style: .default, handler: { (action) -> Void in
                if let presetRef = self.usersReference?.child(self.userId!).child("presets").child(self.preset.id!){
                    presetRef.child("contacts").child(self.friendsForPreset[indexPath.row].id!).removeValue(){ (error, snapshot) in
                        if error != nil{
                            print(error ?? "error at delete friend from preset")
                        } else {
                            self.friendsForPreset.remove(at: indexPath.row)
                            self.presetList.reloadData()
                        }
                        
                    }
                }
            })
            alert.addAction(action2)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    @objc func goBack(){
        self.dismiss(animated: true, completion: nil)
    }
}
