//
//  AddMoreToPresetViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 09/04/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import FirebaseDatabase
import CoreData

class AddMoreToPresetViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UICollectionViewDataSource, UICollectionViewDelegate {
    var usersReference : DatabaseReference?
    var localFacebookFriends = [localContactStruct]()
    var localFacebookFriendsCurrent = [localContactStruct]()
    var friendsForPresetAdded = [localContactStruct]()
    var friendsForPresetToAdd = [localContactStruct]()
    var preset : localPresetStruct!
    var userId : String?
    var context : NSManagedObjectContext!
    
    @IBOutlet weak var searchBarAddContacts: UISearchBar!
    @IBOutlet weak var addedContactsCollection: UICollectionView!
    @IBOutlet weak var contactsList: UITableView!
    @IBOutlet weak var doneButton: UIButton!
    
    
    func loadFriends(){
        self.localFacebookFriends = [localContactStruct]()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MyContact")
        request.returnsObjectsAsFaults = false
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                for result in results as! [NSManagedObject]{
                    let theId = result.value(forKey: "userId") as! String
                    let theName = result.value(forKey: "name") as! String
                    let theFavCuis = result.value(forKey: "favCuis") as! String
                    let theFavRest = result.value(forKey: "favRest") as! String
                    let theGender = result.value(forKey: "gender") as! String
                    let picChangedAmount = result.value(forKey: "picChangedAmount") as! String
                    let timeLastChanged = result.value(forKey: "timeLastChanged") as! String
                    
                    if(!self.friendsForPresetAdded.contains(where: {x in x.id == String(describing: theId)})){
                        self.localFacebookFriends.append(localContactStruct(id: theId, name: theName, favCuis: theFavCuis, favRest: theFavRest, gender: theGender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged))
                    }
                }
            }
        } catch{
            print("error")
        }
        self.localFacebookFriendsCurrent = self.localFacebookFriends
        self.contactsList.reloadData()
    }
    
    func disableIfNoInternet(){
        if(!Reachability.isConnectedToNetwork()){
            doneButton.isEnabled = false
        }
    }
    
    func setUpViews(){
        let theWidth = view.frame.width
        let startOfArea = 20 + (navigationController?.navigationBar.frame.height)! + 10
        let addedContactsCollectionView = UIView(frame: CGRect(x: 5, y: startOfArea, width: theWidth-10, height: 60))
        addedContactsCollectionView.addShadowBottom()
        addedContactsCollection.frame = CGRect(x: 0, y: 0, width: theWidth-10, height: 60)
        addedContactsCollection.removeFromSuperview()
        addedContactsCollectionView.addSubview(addedContactsCollection)
        view.addSubview(addedContactsCollectionView)
        
        searchBarAddContacts.frame = CGRect(x: 5, y: startOfArea+80, width: theWidth-10, height: 50)
        contactsList.frame = CGRect(x: 5, y: startOfArea + 130, width: theWidth-10, height: view.frame.height - startOfArea - 130 - 50)
        
        let saveChangesButton = UIButton(frame: CGRect(x: 10, y: view.frame.height - 40, width: theWidth-20, height: 30))
        saveChangesButton.backgroundColor = UIColor.white
        saveChangesButton.addTarget(self, action: #selector(saveChanges), for: .touchUpInside)
        saveChangesButton.addShadowBottom()
        let icon = UIImage(named: "pencil_icon.png")
        saveChangesButton.setImage(icon, for: .normal)
        saveChangesButton.imageView?.contentMode = .scaleAspectFit
        let toLeft = saveChangesButton.frame.width/2
        saveChangesButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: -toLeft, bottom: 10, right: 0)
        saveChangesButton.setTitleColor(UIColor.black, for: .normal)
        saveChangesButton.setTitle("Save changes", for: .normal)
        saveChangesButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: (-theWidth/2)*1.3 , bottom: 0, right: 0)
        
        view.addSubview(saveChangesButton)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
        self.title = "Add more to preset"
        addTopViewColor(colorNavBar: true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        setUpViews()
        loadFriends()
    }
    
    @objc func saveChanges(){
        let presetRef = self.usersReference?.child(self.userId!).child("presets").child(self.preset.id!)
        let myGroup = DispatchGroup()
        for friend in (friendsForPresetToAdd){
            myGroup.enter()
            presetRef?.child("contacts").child(friend.id!).setValue(["name" : friend.name!, "id" : friend.id!]){ (error, snapshot) in
                myGroup.leave()
            }
        }
        myGroup.notify(queue: .main){
            _ = self.navigationController?.popViewController(animated: true)
        }
    }
    
//    @IBAction func done(_ sender: UIButton) {
//        let presetRef = self.usersReference?.child(self.userId!).child("presets").child(self.preset.id!)
//        let myGroup = DispatchGroup()
//        for friend in (friendsForPresetToAdd){
//            myGroup.enter()
//            presetRef?.child("contacts").child(friend.id!).setValue(["name" : friend.name!, "id" : friend.id!]){ (error, snapshot) in
//                myGroup.leave()
//            }
//        }
//        myGroup.notify(queue: .main){
//            _ = self.navigationController?.popViewController(animated: true)
//        }
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return localFacebookFriendsCurrent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCellID") as! ContactCellTableViewCell
        let contact = localFacebookFriendsCurrent[indexPath.row]
        cell.nameLbl.text = contact.name
        cell.cuisineLbl.text = "Cuisine: " + localFacebookFriendsCurrent[indexPath.row].favCuis!
        cell.profilePic.image = UIImage(named: "profile_icon.png")
        cell.profilePic.makePortrait()
        getPicDataForUser(theUserId: contact.id!, picChangedAmount: contact.picChangedAmount!, completionHandler: {picData in
            cell.profilePic.image = UIImage(data: picData as Data)
        })
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        friendsForPresetToAdd.append(localFacebookFriendsCurrent[indexPath.row])
        localFacebookFriendsCurrent.remove(at: indexPath.row)
        localFacebookFriends.remove(at: indexPath.row)
        
        contactsList.reloadData()
        addedContactsCollection.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return friendsForPresetToAdd.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContactCell", for: indexPath) as! ContactCollectionViewCell
        let contact = friendsForPresetToAdd[indexPath.row]
        cell.contactName.text = contact.name
        cell.contactImage.image = UIImage(named: "profile_icon.png")
        getPicDataForUser(theUserId: contact.id!, picChangedAmount: contact.picChangedAmount!, completionHandler: {picData in
            cell.contactImage.image = UIImage(data: picData as Data)
        })
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        localFacebookFriendsCurrent.append(friendsForPresetAdded[indexPath.row])
        localFacebookFriends.append(friendsForPresetAdded[indexPath.row])
        friendsForPresetAdded.remove(at: indexPath.row)
        
        contactsList.reloadData()
        addedContactsCollection.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            localFacebookFriendsCurrent = localFacebookFriends
            contactsList.reloadData()
            return}
        
        localFacebookFriendsCurrent = localFacebookFriends.filter({ (localFriend) -> Bool in
            guard let text = searchBar.text else {return false}
            return (localFriend.name?.lowercased().contains(text.lowercased()))!
        })
        contactsList.reloadData()
    }
}
