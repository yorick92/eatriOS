//
//  NamePresetViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 09/04/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import FirebaseDatabase
import CoreData

class NamePresetViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    var usersReference : DatabaseReference?
    var friendsForPreset = [localContactStruct]()
    var userId : String?
    var context : NSManagedObjectContext!
    
    var makePresetButton : UIButton!
    
    @IBOutlet weak var contactsAmountLbl: UILabel!
    @IBOutlet weak var presetNameTextField: UITextField!
    @IBOutlet weak var addedContactsCollection: UICollectionView!
    
    func setUpOutlets(){
        makePresetButton.isEnabled = false
        presetNameTextField.placeholder = "preset name"
        contactsAmountLbl.text = "Contacts: " + String(friendsForPreset.count)
    }
    
    func addView(){
        let theWidth = view.frame.width
        
        makePresetButton = UIButton(frame: CGRect(x: 10, y: view.frame.height - 70, width: theWidth-20, height: 50))
        makePresetButton.backgroundColor = UIColor.white
        makePresetButton.addTarget(self, action: #selector(savePreset), for: .touchUpInside)
        makePresetButton.addShadowBottom()
        let icon = UIImage(named: "pencil_icon.png")
        makePresetButton.setImage(icon, for: .normal)
        makePresetButton.imageView?.contentMode = .scaleAspectFit
        let toLeft = makePresetButton.frame.width/2
        makePresetButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: -toLeft, bottom: 10, right: 0)
        makePresetButton.setTitleColor(UIColor.black, for: .normal)
        makePresetButton.setTitle("Save preset", for: .normal)
        makePresetButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: (-theWidth/2)*1.3 , bottom: 0, right: 0)
        makePresetButton.isEnabled = false
        
        view.addSubview(makePresetButton)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
        self.title = "Name preset"
        addTopViewColor(colorNavBar: true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        
        addView()
        setUpOutlets()
    }
    
    @objc func savePreset(){
        var presetStringList = [String : [String: String]]()
        
        for friend in (friendsForPreset){
            presetStringList[friend.id!] = ["name" : friend.name!, "id" : friend.id!]
        }
        let newPresetRef = self.usersReference?.child(self.userId!).child("presets").childByAutoId()
        let preset = ["name" : presetNameTextField.text!, "id" : newPresetRef?.key as Any, "contacts" : presetStringList] as [String : Any]
        newPresetRef?.setValue(preset)
        self.usersReference?.child(self.userId!).child("presets").child("timeLastChanged").setValue(String(Date().millisecondsSince1970))
        
        let presetName = presetNameTextField.text!
        let presetId = newPresetRef?.key
        var contactsForPreset = NSSet()
        for contact in friendsForPreset{
            let myContact = self.contactExists(contactId: contact.id!, entityName: "MyContact")
            if(myContact != nil){
                contactsForPreset = contactsForPreset.adding(myContact as Any) as NSSet
            } else {
                print("contact not available for preset, this should be impossible")
            }
        }
        
        let newPreset = NSEntityDescription.insertNewObject(forEntityName: "Preset" , into: self.context)
        newPreset.setValue(presetId, forKey: "id")
        newPreset.setValue(presetName, forKey: "name")
        newPreset.setValue(contactsForPreset, forKey: "contacts")
        
        do{
            try self.context.save()
        } catch{
            print("could not save event into local storage")
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func presetNameChanged(_ sender: UITextField) {
        if(sender.text?.isEmpty)!{
            makePresetButton.isEnabled = false
        } else{
            makePresetButton.isEnabled = true
        }
    }
    
    func contactExists(contactId : String, entityName : String)->NSManagedObject?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "userId") as! String == contactId}){
                    return result as? NSManagedObject;
                } else {
                    return nil
                }
            }
        } catch{
            return nil
        }
        return nil
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return friendsForPreset.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ProfileOtherID") as! OpenOtherProfileViewController
        nextViewController.theContact = friendsForPreset[indexPath.row]
        let navController = UINavigationController(rootViewController: nextViewController)
        self.present(navController, animated: true, completion: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContactCell", for: indexPath) as! ContactCollectionViewCell
        let contact = friendsForPreset[indexPath.row]
        cell.contactName.text = contact.name
        cell.contactImage.image = UIImage(named: "profile_icon.png")
        cell.contactImage.makePortrait()
        getPicDataForUser(theUserId: contact.id!, picChangedAmount: contact.picChangedAmount!, completionHandler: {picData in
            cell.contactImage.image = UIImage(data: picData as Data)
        })
        return cell
    }
}

