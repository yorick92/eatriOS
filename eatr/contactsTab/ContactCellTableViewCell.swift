//
//  ContactCellTableViewCell.swift
//  eatr
//
//  Created by Yorick Bolster on 09/04/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit

class ContactCellTableViewCell: UITableViewCell {

    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var cuisineLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
