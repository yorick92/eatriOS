//
//  NameGroupViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 09/04/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import FirebaseDatabase
import CoreData

class NameGroupViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    var ref : DatabaseReference?
    var usersReference : DatabaseReference?
    var groupsReference : DatabaseReference?
    var friendsForGroup = [localContactStruct]()
    var context : NSManagedObjectContext!
    var userId : String?

    var makeGroupButton : UIButton!
    
    @IBOutlet weak var addedContactsCollectionView: UICollectionView!
    @IBOutlet weak var groupNameTextField: UITextField!
    @IBOutlet weak var contactsAmountLbl: UILabel!
    
    func setUpOutlets(){
        groupNameTextField.placeholder = "group name"
        contactsAmountLbl.text = "Contacts: " + String(friendsForGroup.count)
    }
    
    func setUpDatabaseReferences(){
        ref = Database.database().reference()
    }
    
    func addView(){
        let theWidth = view.frame.width
        
        makeGroupButton = UIButton(frame: CGRect(x: 10, y: view.frame.height - 70, width: theWidth-20, height: 50))
        makeGroupButton.backgroundColor = UIColor.white
        makeGroupButton.addTarget(self, action: #selector(makeGroup), for: .touchUpInside)
        makeGroupButton.addShadowBottom()
        let icon = UIImage(named: "pencil_icon.png")
        makeGroupButton.setImage(icon, for: .normal)
        makeGroupButton.imageView?.contentMode = .scaleAspectFit
        let toLeft = makeGroupButton.frame.width/2
        makeGroupButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: -toLeft, bottom: 10, right: 0)
        makeGroupButton.setTitleColor(UIColor.black, for: .normal)
        makeGroupButton.setTitle("Create group", for: .normal)
        makeGroupButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: (-theWidth/2)*1.3 , bottom: 0, right: 0)
        makeGroupButton.isEnabled = false
        
        view.addSubview(makeGroupButton)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
        self.title = "name your group"
        addView()
        addTopViewColor(colorNavBar: true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        setUpOutlets()
        setUpDatabaseReferences()
    }
    
    @IBAction func changedGroupName(_ sender: UITextField) {
        if(sender.text?.isEmpty)!{
            makeGroupButton.isEnabled = false
        } else{
            makeGroupButton.isEnabled = true
        }
    }
    
    func contactExists(contactId : String, entityName : String)->NSManagedObject?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "userId") as! String == contactId}){
                    return result as? NSManagedObject;
                } else {
                    return nil
                }
            }
        } catch{
            return nil
        }
        return nil
    }
    
    @objc func makeGroup(){
        let newDispatchGroup = DispatchGroup()
        
        var groupStringList = [String : [String: String]]()
        let newGroupRef = self.groupsReference?.childByAutoId()
        
        let theUser = contactExists(contactId: userId!, entityName: "User")
        print(theUser)
        let usersName = theUser?.value(forKey: "name") as! String
        groupStringList[userId!] = ["name" : usersName, "id" : userId!]
        for friend in (friendsForGroup){
            groupStringList[friend.id!] = ["name" : friend.name!, "id" : friend.id!]
            newDispatchGroup.enter()
            // new way of saving who belongs to what group
            self.ref?.child("publicProfiles").child(friend.id!).child("groups").child((newGroupRef?.key)!).setValue(self.groupNameTextField.text!){(error, snapshot) in
                newDispatchGroup.leave()
            }
            // end new
            
            newDispatchGroup.enter()
            // old way of saving who belongs to what group
//            self.usersReference?.child(friend.id!).child("groups").child((newGroupRef?.key)!).setValue(self.groupNameTextField.text!){(error, snapshot) in
//                newDispatchGroup.leave()
//            }
            // end old
        }
        
        let group = ["name" : self.groupNameTextField.text!, "id" : newGroupRef?.key as Any, "contacts" : groupStringList, "admin": self.userId as Any, "timeLastChanged": String(Date().millisecondsSince1970)] as [String : Any]
        newDispatchGroup.enter()
        newGroupRef?.setValue(group){(error, snapshot) in
            newDispatchGroup.leave()
        }
        
        // old way of saving yourself to a group
//        newDispatchGroup.enter()
//        self.usersReference?.child(self.userId!).child("groups").child((newGroupRef?.key)!).setValue(self.groupNameTextField.text!){(error, snapshot) in
//            newDispatchGroup.leave()
//        }
        // end old
        
        // new way of saving yourself to a group
        newDispatchGroup.enter()
        self.ref?.child("publicProfiles").child(self.userId!).child("groups").child((newGroupRef?.key)!).setValue(self.groupNameTextField.text!){(error, snapshot) in
            newDispatchGroup.leave()
        }
        // end new
        
        
        newDispatchGroup.notify(queue: .main){
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return friendsForGroup.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContactCell", for: indexPath) as! ContactCollectionViewCell
        let contact = friendsForGroup[indexPath.row]
        cell.contactName.text = contact.name
        cell.contactImage.makePortrait()
        cell.contactImage.image = UIImage(named: "profile_icon.png")
        getPicDataForUser(theUserId: contact.id!, picChangedAmount: contact.picChangedAmount!, completionHandler: {picData in
            cell.contactImage.image = UIImage(data: picData as Data)
        })
        return cell
    }
}
