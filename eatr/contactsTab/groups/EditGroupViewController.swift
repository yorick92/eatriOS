//
//  EditGroupViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 28/03/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import FirebaseDatabase
import CoreData

class EditGroupViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var usersReference : DatabaseReference?
    var groupsReference : DatabaseReference?
    var Db : DatabaseReference?
    var friendsForGroup = [localContactStruct]()
    var group : localGroupStruct!
    var userId : String?
    var isAdmin = false
    var context : NSManagedObjectContext!
    let myGroup = DispatchGroup()
    
    var changeNameButton : UIButton!
    var leaveGroupButton : UIButton!
    
    @IBOutlet weak var groupList: UITableView!
    @IBOutlet weak var groupNameTextField: UITextField!
    @IBOutlet weak var editGroupButton: UIButton!
    
    func setUpNavigationController(){
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "< back", style: .plain, target: self, action: #selector(goBack))
    }
    
    func setUpDatabaseReferences(){
        Db = Database.database().reference()
        usersReference = Database.database().reference().child("Users")
        groupsReference = Database.database().reference().child("groups")
    }
    
    func setUpVariables(){
        userId = Auth.auth().currentUser!.uid
        friendsForGroup = group.contacts!
        if(group.admin == userId){
            isAdmin = true
        }
        groupNameTextField.text = group.name!
    }
    
    func deleteGroupFromCoreData(groupId : String){
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Group")
        request.returnsObjectsAsFaults = false

        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "id") as! String == groupId}){
                    self.context.delete(result as! NSManagedObject)
                }
            }
        } catch{
            print("error")
        }

        do{
            try self.context.save()
        } catch{

        }
    }
    
    func disableIfNoInternet(){
        if(!Reachability.isConnectedToNetwork()){
            changeNameButton.isEnabled = false
            editGroupButton.isEnabled = false
            leaveGroupButton.isEnabled = false
        }
    }
    
    func addViews(){
        let theWidth = view.frame.width
        
        groupList.frame = CGRect(x: groupList.frame.origin.x, y: groupList.frame.origin.y, width: groupList.frame.width, height: groupList.frame.height - 40)
        
        let placeButtonAt = groupList.frame.origin.y + groupList.frame.height + 10
        
        changeNameButton = UIButton(frame: CGRect(x: 10, y: placeButtonAt, width: theWidth-20, height: 30))
        changeNameButton.backgroundColor = UIColor.white
        changeNameButton.addTarget(self, action: #selector(changeName), for: .touchUpInside)
        changeNameButton.addShadowBottom()
        let icon = UIImage(named: "pencil_icon.png")
        changeNameButton.setImage(icon, for: .normal)
        changeNameButton.imageView?.contentMode = .scaleAspectFit
        let toLeft = changeNameButton.frame.width/2
        changeNameButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: -toLeft, bottom: 10, right: 0)
        changeNameButton.setTitleColor(UIColor.black, for: .normal)
        changeNameButton.setTitle("Change name", for: .normal)
        changeNameButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: (-theWidth/2)*1.3 , bottom: 0, right: 0)
        
        leaveGroupButton = UIButton(frame: CGRect(x: 10, y: placeButtonAt + 40, width: theWidth-20, height: 30))
        leaveGroupButton.backgroundColor = UIColor.white
        leaveGroupButton.addTarget(self, action: #selector(leaveGroup), for: .touchUpInside)
        leaveGroupButton.addShadowBottom()
        let icon2 = UIImage(named: "cross_icon.png")
        leaveGroupButton.setImage(icon2, for: .normal)
        leaveGroupButton.imageView?.contentMode = .scaleAspectFit
        let toLeft2 = leaveGroupButton.frame.width/2
        leaveGroupButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: -toLeft2, bottom: 10, right: 0)
        leaveGroupButton.setTitleColor(UIColor.red, for: .normal)
        leaveGroupButton.setTitle("Leave group", for: .normal)
        leaveGroupButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: (-theWidth/2)*1.2 , bottom: 0, right: 0)
        
        view.addSubview(changeNameButton)
        view.addSubview(leaveGroupButton)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
        self.title = group.name!
        addTopViewColor(colorNavBar: true)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        addViews()
        setUpNavigationController()
        setUpDatabaseReferences()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpVariables()
        disableIfNoInternet()
    }
    
    @objc func leaveGroup(){
        let alert = UIAlertController(title: "", message: "Are you sure you want to leave this group?" , preferredStyle: .alert)
        
        let action1 = UIAlertAction(title: "Yes", style: .default, handler: { (action) -> Void in
            
            if(self.friendsForGroup.count > 1){
                let localDispGroup = DispatchGroup()
                localDispGroup.enter()
                if(!self.isAdmin){
                    self.Db?.child("publicProfiles").child(self.userId!).child("groups").child(self.group.id!).removeValue() { (error, snapshot) in
                        self.groupsReference?.child(self.group.id!).child("contacts").child(self.userId!).removeValue(){ (error, snapshot) in
                            localDispGroup.leave()
                        }
                    }
                } else {
                    self.friendsForGroup.remove(at: self.friendsForGroup.index(where: {$0.id == self.userId!})!)
                    self.groupsReference?.child(self.group.id!).child("admin").setValue(self.friendsForGroup[0].id!){ (error, snapshot) in
                        self.Db?.child("publicProfiles").child(self.userId!).child("groups").child(self.group.id!).removeValue(){ (error, snapshot) in
                            self.groupsReference?.child(self.group.id!).child("contacts").child(self.userId!).removeValue(){ (error, snapshot) in
                                localDispGroup.leave()
                            }
                        }
                    }
                }
                localDispGroup.notify(queue: .main){
                    self.groupsReference?.child(self.group.id!).child("timeLastChanged").removeAllObservers()
                    self.groupsReference?.child(self.group.id!).child("timeLastChanged").setValue(String(Date().millisecondsSince1970)){ (error, snapshot) in
                        self.dismiss(animated: true, completion: nil)
                    }
                }
            } else {
                self.groupsReference?.child(self.group.id!).removeValue()
                self.Db?.child("publicProfiles").child(self.userId!).child("groups").child(self.group.id!).removeValue()
            }
            self.deleteGroupFromCoreData(groupId: self.group.id!)
        })
        
        alert.addAction(action1)
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func addContact(_ sender: UIButton) {
        if(isAdmin){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "AddMoreContactsGroupID") as! AddMoreToGroupViewController
            vc.friendsForGroupAdded = friendsForGroup
            vc.userId = userId
            vc.group = group
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func groupNameTextChanged(_ sender: UITextField) {
        if(sender.text?.isEmpty)!{
            changeNameButton.isEnabled = false
        } else {
            changeNameButton.isEnabled = true
        }
    }
    
    @objc func changeName(){
        let localGroup = DispatchGroup()
        
        localGroup.enter()
        self.groupsReference?.child(self.group.id!).child("name").setValue(self.groupNameTextField.text!){ (error, snapshot) in
            self.groupsReference?.child(self.group.id!).child("timeLastChanged").setValue(String(Date().millisecondsSince1970)){ (error, snapshot) in
                localGroup.leave()
            }
        }
        
        for groupContact in group.contacts!{
            localGroup.enter()
            self.Db?.child("publicProfiles").child(groupContact.id!).child("groups").child(group.id!).setValue(groupNameTextField.text!){ (error, snapshot) in
                localGroup.leave()
            }
        }
        
        localGroup.notify(queue: .main){
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friendsForGroup.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCellID") as! ContactCellTableViewCell
        let contact = friendsForGroup[indexPath.row]
        cell.nameLbl.text = contact.name
        cell.profilePic.image = UIImage(named: "profile_icon.png")
        cell.cuisineLbl.text = "Cuisine: " + contact.favCuis!
        cell.profilePic.makePortrait()
        getPicDataForUser(theUserId: contact.id!, picChangedAmount: contact.picChangedAmount!, completionHandler: {picData in
            cell.profilePic.image = UIImage(data: picData as Data)
        })
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(isAdmin && Reachability.isConnectedToNetwork()){
            var alert : UIAlertController!
            if(friendsForGroup[indexPath.row].id == userId){
                alert = UIAlertController(title: "", message: "View your own profile?" , preferredStyle: .alert)
            } else{
                alert = UIAlertController(title: "", message: "Delete " + friendsForGroup[indexPath.row].name! + " from group or view profile?" , preferredStyle: .alert)
            }
            
            let action1 = UIAlertAction(title: "View profile", style: .default, handler: { (action) -> Void in
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileOtherID") as! OpenOtherProfileViewController
                vc.theContact = self.friendsForGroup[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            })
            alert.addAction(action1)
            
            
            if(friendsForGroup[indexPath.row].id != userId){
                let action2 = UIAlertAction(title: "Delete", style: .default, handler: { (action) -> Void in
                    if let groupRef = self.groupsReference?.child(self.group.id!){
                        print(self.friendsForGroup[indexPath.row].id!)
                        groupRef.child("contacts").child(self.friendsForGroup[indexPath.row].id!).removeValue(){ (error, snapshot) in
                            if error != nil{
                                print(error ?? "error at delete friend from group")
                            } else {
                                self.friendsForGroup.remove(at: indexPath.row)
                                self.groupList.reloadData()
                                self.Db?.child("publicProfiles").child(self.friendsForGroup[indexPath.row].id!).child("groups").child(self.group.id!).removeValue(){ (error, snapshot) in
                                    groupRef.child("timeLastChanged").setValue(String(Date().millisecondsSince1970))
                                }
                            }
                        }
                    }
                })
                alert.addAction(action2)
            }
            
            let cancel = UIAlertAction(title: "No", style: .destructive, handler: { (action) -> Void in })
            
            alert.addAction(cancel)
            present(alert, animated: true, completion: nil)
        } else{
            var alert : UIAlertController!
            alert = UIAlertController(title: "", message: "View " + friendsForGroup[indexPath.row].name! +  "'s profile?" , preferredStyle: .alert)
            
            let action1 = UIAlertAction(title: "View profile", style: .default, handler: { (action) -> Void in
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "ProfileOtherID") as! OpenOtherProfileViewController
                vc.theContact = self.friendsForGroup[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            })
            alert.addAction(action1)
            
            let cancel = UIAlertAction(title: "No", style: .destructive, handler: { (action) -> Void in })
            
            alert.addAction(cancel)
            present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func goBack(){
        self.dismiss(animated: true, completion: nil)
    }
}
