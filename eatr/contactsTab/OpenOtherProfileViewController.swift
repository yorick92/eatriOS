//
//  OpenOtherProfileViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 25/03/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Firebase
import FirebaseStorage

extension UIViewController{
    func isModal() -> Bool {
        if let navigationController = self.navigationController{
            if navigationController.viewControllers.first != self{
                return false
            }
        }
        
        if self.presentingViewController != nil {
            return true
        }
        
        if self.navigationController?.presentingViewController?.presentedViewController == self.navigationController  {
            return true
        }
        
        if self.tabBarController?.presentingViewController is UITabBarController {
            return true
        }
        
        return false
    }
}

class OpenOtherProfileViewController: UIViewController {
    var storageRef : StorageReference?
    var facebookIdRef : DatabaseReference?
    var theContact: localContactStruct?
    var profileUserId : String!
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var genderLbl: UILabel!
    @IBOutlet weak var cuisineLbl: UILabel!
    @IBOutlet weak var restLbl: UILabel!
    
    func setUpDatabaseReferences(){
        self.storageRef = Storage.storage().reference(withPath: "pics/Users/" + self.profileUserId + "/profile.jpg")
    }
    
    func setUpProfileInfo(){
        self.nameLbl.text = theContact?.name!
        self.genderLbl.text = theContact?.gender!
        self.cuisineLbl.text = theContact?.favCuis!
        self.restLbl.text = theContact?.favRest!
        let picChangedAmount = theContact?.picChangedAmount!
        
        if(UserDefaults.standard.object(forKey: self.profileUserId! + "/" + picChangedAmount! ) != nil){
            print("from storage")
            let picData = UserDefaults.standard.object(forKey: self.profileUserId! + "/" + picChangedAmount!) as! NSData
            self.profileImage.image = UIImage(data: picData as Data)
        } else {
            // remove old one from local database
            var amountChanged : Int = Int(picChangedAmount!)!
            amountChanged -= 1
            let lastEventChangedAmount = String(amountChanged)
            UserDefaults.standard.removeObject(forKey: self.profileUserId! + "/" + lastEventChangedAmount)
            
            print("from database")
            
            self.storageRef!.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    DispatchQueue.main.async {
                        let image = UIImage(named: "profile_icon.png")
                        let imageData : NSData = UIImagePNGRepresentation(image!)! as NSData
                        UserDefaults.standard.set(imageData, forKey: self.profileUserId! + "/" + picChangedAmount! )
                        self.profileImage.image = image
                    }
                    print(error)
                } else {
                    DispatchQueue.main.async {
                        let image = UIImage(data: data!)
                        let imageData : NSData = UIImagePNGRepresentation(image!)! as NSData
                        UserDefaults.standard.set(imageData, forKey: self.profileUserId! + "/" + picChangedAmount! )
                        self.profileImage.image = image
                    }
                }
            }
        }
    }
    
    func setUpNavigationController(){
        if(self.isModal()){
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "< back", style: .plain, target: self, action: #selector(goBack))
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = (theContact?.name)! + "'s profile"
        profileUserId = theContact?.id!
        addTopViewColor(colorNavBar: true)
        setUpDatabaseReferences()
        setUpNavigationController()
        setUpProfileInfo()
    }
    
    @objc func goBack(){
        self.dismiss(animated: true, completion: nil)
    }
}
