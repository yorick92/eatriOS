//
//  item4MainPageViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 11/02/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Firebase
import FirebaseStorage
import FirebaseDatabase
import CoreData

class item4MainPageViewController: UIViewController, FBSDKLoginButtonDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIScrollViewDelegate{
    var storageRef : StorageReference?
    var userDataReference : DatabaseReference?
    var Db : DatabaseReference?
    var userId : String?
    var context : NSManagedObjectContext!
    var theUser : localContactStruct!
    var spinnerView : UIView?
    
    var theWidth : CGFloat!
    var realViewHeight : CGFloat!
    var theScrollHeight : CGFloat!
    
    var theScrollView : UIScrollView!
    var profilePic : UIImageView!
    var smallProfilePic : UIImageView!
    var viewForBackground : UIView!
    var nameAtImage : UILabel!
    
    var preferencesView : UIView!
    var favRestInfoLabel = UILabel()
    var favCuisInfoLabel = UILabel()
    
    var personalInfoView : UIView!
    var nameInfoLabel = UILabel()
    var genderInfoLabel = UILabel()
    
    func getUserObject(userId : String)->NSManagedObject?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try self.context.fetch(request)
            if results.count > 0{
                if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "userId") as! String == userId}){
                    return result as? NSManagedObject;
                } else {
                    return nil
                }
            }
        } catch{
            return nil
        }
        return nil
    }
    
    func setUpOutlets(){
        let logoutButton = FBSDKLoginButton(frame: CGRect(x: 16, y: theScrollView.contentSize.height - CGFloat(50), width: view.frame.width-32, height: 50))
        theScrollView.addSubview(logoutButton)
        logoutButton.delegate = self
    }
    
    func setUpDatabaseReferences(){
        storageRef = Storage.storage().reference(withPath: "pics/Users/" + userId! + "/profile.jpg")
        userDataReference = Database.database().reference().child("Users").child(self.userId!)
        Db = Database.database().reference()
    }
    
    func setUpVariables(){
        userId = Auth.auth().currentUser!.uid
    }
    
    func setUpUserInfoAndPic(){
        let theUserObject = getUserObject(userId: userId!)
        if(theUserObject == nil){
            return
        }
        
        let name = theUserObject?.value(forKey: "name") as? String
        let gender = theUserObject?.value(forKey: "gender") as? String
        let favCuis = theUserObject?.value(forKey: "favCuis") as? String
        let favRest = theUserObject?.value(forKey: "favRest") as? String
        let picChangedAmount = theUserObject?.value(forKey: "picChangedAmount") as? String
        let id = theUserObject?.value(forKey: "userId") as? String
        let timeLastChanged = theUserObject?.value(forKey: "timeLastChanged") as? String

        favCuisInfoLabel.text = favCuis
        favRestInfoLabel.text = favRest
        nameInfoLabel.text = name
        nameAtImage.text = name
        genderInfoLabel.text = gender
        
        theUser = localContactStruct(id: id, name: name, favCuis: favCuis, favRest: favRest, gender: gender, picChangedAmount: picChangedAmount, timeLastChanged: timeLastChanged)
        if(UserDefaults.standard.object(forKey: self.userId! + "/" + picChangedAmount! ) != nil){
            print(picChangedAmount!)
            print(self.userId!)
            let picData = UserDefaults.standard.object(forKey: self.userId! + "/" + picChangedAmount!) as! NSData
            self.profilePic.image = UIImage(data: picData as Data)
            self.smallProfilePic.image = UIImage(data: picData as Data)
        } else {
            // remove old one from local database
            var amountChanged : Int = Int(picChangedAmount!)!
            amountChanged -= 1
            let lastEventChangedAmount = String(amountChanged)
            print(lastEventChangedAmount)
            UserDefaults.standard.removeObject(forKey: self.userId! + "/" + lastEventChangedAmount)
            
            self.storageRef!.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    DispatchQueue.main.async {
                        let image = UIImage(named: "profile_icon.png")
                        let imageData : NSData = UIImagePNGRepresentation(image!)! as NSData
                        UserDefaults.standard.set(imageData, forKey: self.userId! + "/" + picChangedAmount! )
                        self.profilePic.image = image
                        self.smallProfilePic.image = image
                        
                        if(self.spinnerView != nil){
                            UIViewController.removeSpinner(spinner: self.spinnerView!)
                        }
                    }
                    print(error)
                } else {
                    DispatchQueue.main.async {
                        let image = UIImage(data: data!)
                        let imageData : NSData = UIImagePNGRepresentation(image!)! as NSData
                        UserDefaults.standard.set(imageData, forKey: self.userId! + "/" + picChangedAmount! )
                        self.profilePic.image = image
                        self.smallProfilePic.image = image
                        
                        if(self.spinnerView != nil){
                            UIViewController.removeSpinner(spinner: self.spinnerView!)
                        }
                    }
                }
            }
        }
    }
    
    func disableIfNoInternet(){
        if(!Reachability.isConnectedToNetwork()){
            // TODO
        }
    }
    
    /*** CREATE VIEW FUNCTIONS ***/
    
    func setUpParentViews(){
        theWidth = view.frame.width
        realViewHeight = view.frame.height - (self.tabBarController?.tabBar.frame.size.height)!
        theScrollHeight = 770
        theScrollView = UIScrollView(frame: CGRect(x: 0, y: 0, width: theWidth, height: realViewHeight))
        theScrollView.contentSize = CGSize(width: theWidth, height: theScrollHeight)
        theScrollView.delegate = self
        view.addSubview(theScrollView)

        let profilePicHeight = CGFloat(300.0)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(item4MainPageViewController.tappedProfilePic(gesture:)))
        
        smallProfilePic = UIImageView(frame: CGRect(x: theWidth/2 - 100, y: profilePicHeight/2 - 100, width: 200, height: 200))
        smallProfilePic.makePortrait()
        smallProfilePic.contentMode = .scaleAspectFit
        smallProfilePic.image = UIImage(named: "profile_icon.png")
        theScrollView.addSubview(smallProfilePic)
        
        smallProfilePic.addGestureRecognizer(tapGesture)
        smallProfilePic.isUserInteractionEnabled = true
        
        nameAtImage = UILabel(frame: CGRect(x: theWidth/2 - 100, y: profilePicHeight/2 + 110, width: 200, height: 20))
        nameAtImage.text = "placeholder"
        nameAtImage.textColor = UIColor.white
        nameAtImage.font = UIFont.systemFont(ofSize: 14)
        nameAtImage.textAlignment = .center
        theScrollView.addSubview(nameAtImage)
        
        profilePic = UIImageView(frame: CGRect(x: 0, y: 0, width: theWidth, height: profilePicHeight))
        profilePic.image = UIImage(named: "profile_icon.png")
        profilePic.contentMode = .scaleAspectFit
        profilePic.addBlurLight()
        profilePic.addBlurDark()
        
        let InfoButton = UIButton(frame: CGRect(x: theWidth - CGFloat(20), y: profilePicHeight - (20), width: 15, height: 15))
        InfoButton.setImage(UIImage(named: "info_icon.png"), for: .normal)
        
        InfoButton.addTarget(self, action: #selector(showInfoAboutProfile), for: .touchUpInside)
        theScrollView.addSubview(InfoButton)
        
        theScrollView.addSubview(profilePic)
        theScrollView.sendSubview(toBack: profilePic)
        profilePic.isUserInteractionEnabled = true

        theScrollView.backgroundColor = UIColor.white
    }
    
    func changeValue(title : String, message : String, infoLabel : UILabel, databaseString : String, changeNameToo : Bool){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addTextField { (textField: UITextField) in
            textField.text = infoLabel.text
        }
        
        let changeFavRestAction = UIAlertAction(title: "Save Changes", style: .default, handler: { (action) -> Void in
            let timeLastChanged = "\(Date().millisecondsSince1970)"
            self.userDataReference?.child(databaseString).setValue(alert.textFields![0].text)
            
            self.Db?.child("publicProfiles").child(self.userId!).child(databaseString).setValue(alert.textFields![0].text)
            self.Db?.child("publicProfiles").child(self.userId!).child("timeLastChanged").setValue(timeLastChanged)
            
            infoLabel.text = alert.textFields![0].text
            if changeNameToo {
                self.nameAtImage.text = alert.textFields![0].text
            }
        })
        let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
        alert.addAction(changeFavRestAction)
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    @objc func changeFavRest(){
        let title = "what is your favorite restaurant?"
        let message = "Fill in the name so others can see!"
        changeValue(title: title, message: message, infoLabel: favRestInfoLabel, databaseString: "favRest", changeNameToo: false)
    }
    
    @objc func changeFavCuis(){
        let title = "what is your favorite cuisine?"
        let message = "Fill it in so others can see!"
        changeValue(title: title, message: message, infoLabel: favCuisInfoLabel, databaseString: "favCuis", changeNameToo: false)
    }
    
    @objc func changeName(){
        let title = "what is your name?"
        let message = "Fill it in so others can see!"
        changeValue(title: title, message: message, infoLabel: nameInfoLabel, databaseString: "name", changeNameToo: true)
    }
    
    @objc func changeGender(){
        let title = "what is your gender?"
        let message = "Fill it in so others can see!"
        changeValue(title: title, message: message, infoLabel: genderInfoLabel, databaseString: "gender", changeNameToo: false)
    }
    
    @objc func showInfoAboutProfile(){
        let message = "The profile information on this page can be seen by anyone that has you as a contact on the app, and anyone that is invited to the same event as you. \n\n That means your profile could be visible to a friend of a friend of your friend invites you two to the same event, it is however not possible for people to browse through random profiles."
        let alert = UIAlertController(title: "Who can see my profile?", message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Okay", style: .destructive, handler: { (action) -> Void in })
        alert.addAction(cancel)
        present(alert, animated: true, completion: nil)
    }
    
    func makeAnItem(superView : UIView, selector : Selector?, iconName : String, TitleText : String, buttonImageName : String, infoLabel : UILabel, yValue : CGFloat, theHeight : CGFloat, addBottomBorder : Bool){
        
        let substractFromWidth = CGFloat(20)
        let localViewWidth = theWidth - substractFromWidth
        let viewToAdd = UIView(frame: CGRect(x: substractFromWidth/2, y: yValue, width: localViewWidth, height: theHeight))
        viewToAdd.backgroundColor = UIColor.white
        
        let theViewsHeight = viewToAdd.frame.height
        
        // add icon
        let viewForIcon = UIView(frame: CGRect(x: 0.0, y: 0.0, width: localViewWidth/6, height: theViewsHeight))
        viewForIcon.backgroundColor = UIColor.white
        let favCuisIconImageView = UIImageView(frame: CGRect(x: (localViewWidth/6-20)/2, y: 20, width: 20, height: 20))
        favCuisIconImageView.image = UIImage(named: iconName)
        favCuisIconImageView.contentMode = .scaleAspectFill
        viewForIcon.addSubview(favCuisIconImageView)
        
        if(addBottomBorder){
            viewToAdd.addBottomBorderWithColor(color: UIColor.lightGray, width: 1)
            viewForIcon.addBottomBorderWithColor(color: UIColor.lightGray, width: 1)
        }
        
        // add labels
        let favCuisTitleLabel = UILabel(frame: CGRect(x: localViewWidth/6, y: 0, width: (localViewWidth/6)*4, height: theViewsHeight/3*1.5))
        favCuisTitleLabel.text = TitleText
        favCuisTitleLabel.font = UIFont.systemFont(ofSize: 20)
        
        infoLabel.frame = CGRect(x: localViewWidth/6, y: theViewsHeight/3*1.5, width: (localViewWidth/6)*4, height: theViewsHeight/3)
        infoLabel.text = ""
        infoLabel.font = UIFont.systemFont(ofSize: 14)
        
        if selector != nil{
            let changeInfoButton = UIButton(frame: CGRect(x: (localViewWidth/6)*5, y: 0, width: localViewWidth/6, height: theViewsHeight))
            changeInfoButton.addTarget(self, action: selector!, for: .touchUpInside)
            changeInfoButton.setImage(UIImage(named: buttonImageName), for: .normal)
            changeInfoButton.imageView?.contentMode = .scaleAspectFit
            changeInfoButton.imageEdgeInsets = UIEdgeInsets(top: (theViewsHeight/2)-10, left: 0, bottom: (theViewsHeight/2)-10, right: 0)
            viewToAdd.addSubview(changeInfoButton)
        }
        
        viewToAdd.addSubview(viewForIcon)
        viewToAdd.addSubview(favCuisTitleLabel)
        viewToAdd.addSubview(infoLabel)
        
        superView.addSubview(viewToAdd)
    }
    
    func makePreferencesView(){
        preferencesView = UIView(frame: CGRect(x: 0, y: 300, width: theWidth, height: 190))
        preferencesView.backgroundColor = UIColor.white
        preferencesView.addShadow()
        
        let height = CGFloat(30)
        
        let localLabel = UILabel(frame: CGRect(x: 10, y: 0, width: theWidth, height: height))
        localLabel.set(image: UIImage(named: "check_icon.png")!, with: " Preferences")
        localLabel.textColor = UIColor.red
        preferencesView.addSubview(localLabel)
        
        makeAnItem(superView: preferencesView, selector: #selector(changeFavRest), iconName: "clock_icon.png", TitleText: "Favorite restaurant", buttonImageName: "greaterThan_icon.png", infoLabel: favRestInfoLabel, yValue: 30, theHeight: 80, addBottomBorder: true)
        makeAnItem(superView: preferencesView, selector: #selector(changeFavCuis), iconName: "clock_icon.png", TitleText: "Favorite cuisine", buttonImageName: "greaterThan_icon.png", infoLabel: favCuisInfoLabel, yValue: 110, theHeight: 80, addBottomBorder: false)
        
        theScrollView.addSubview(preferencesView)
    }
    
    func makePersonalInfoView(){
        personalInfoView = UIView(frame: CGRect(x: 0, y: 510, width: theWidth, height: 190))
        personalInfoView.backgroundColor = UIColor.white
        personalInfoView.addShadow()
        
        let height = CGFloat(30)
        
        let localLabel = UILabel(frame: CGRect(x: 10, y: 0, width: theWidth, height: height))
        localLabel.set(image: UIImage(named: "check_icon.png")!, with: " Personal info")
        localLabel.textColor = UIColor.red
        personalInfoView.addSubview(localLabel)
        
        makeAnItem(superView: personalInfoView, selector: #selector(changeName), iconName: "clock_icon.png", TitleText: "Name", buttonImageName: "greaterThan_icon.png", infoLabel: nameInfoLabel, yValue: 30, theHeight: 80, addBottomBorder: true)
        makeAnItem(superView: personalInfoView, selector: #selector(changeGender), iconName: "clock_icon.png", TitleText: "Gender", buttonImageName: "greaterThan_icon.png", infoLabel: genderInfoLabel, yValue: 110, theHeight: 80, addBottomBorder: false)
        
        theScrollView.addSubview(personalInfoView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboard()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        context = appDelegate.persistentContainer.viewContext
        addTopViewColor(colorNavBar: true)
        setUpParentViews()
        makePreferencesView()
        makePersonalInfoView()
        setUpOutlets()
        setUpVariables()
        setUpDatabaseReferences()
        
        disableIfNoInternet()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpUserInfoAndPic()
    }

    func pickFromPhotoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func pickFromCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func getFacebookPicture(){
        self.userDataReference?.child("facebookPicUrl").observeSingleEvent(of: .value, with: { (snapshot) in
            let facebookPicUrlString = snapshot.value as? String
            if(facebookPicUrlString != nil){
                let session = URLSession(configuration: .default)
                let facebookPicUrl = URL(string: facebookPicUrlString!)
                let getImageFromUrl = session.dataTask(with: facebookPicUrl!) { (data, response, error) in
                    if let e = error {
                        print("Error Occurred: \(e)")
                    } else {
                        if (response as? HTTPURLResponse) != nil {
                            self.userDataReference?.child("picChangedAmount").observeSingleEvent(of: .value, with: { (snapshot) in
                                var changedString = ""
                                if let a = snapshot.value as? String{
                                    changedString = a
                                } else{
                                    changedString = "0"
                                }
                                
                                var newChangedAmount : Int = Int(changedString)!
                                newChangedAmount += 1
                                let newChangedAmountString = String(newChangedAmount)
                                let timeLastChanged = "\(Date().millisecondsSince1970)"
                                
                                self.userDataReference?.child("picChangedAmount").setValue(newChangedAmountString) { (snapshot, error) in
                                    self.userDataReference?.child("timeLastChanged").setValue(timeLastChanged)
                                }
                                self.Db?.child("publicProfiles").child(self.userId!).child("picChangedAmount").setValue(newChangedAmountString) { (snapshot, error) in
                                    self.Db?.child("publicProfiles").child(self.userId!).child("timeLastChanged").setValue(timeLastChanged)
                                }
                                
                                _ = self.storageRef?.putData(data!, metadata: nil) { (metadata, error) in
                                    guard metadata != nil else {
                                        return
                                    }
                                    self.setUpUserInfoAndPic()
                                }
                            })
                        } else {
                            print("No response from server")
                        }
                    }
                }
                getImageFromUrl.resume()
            }
        })
    }
    
    func deleteProfilePic(){
        
        let changedString = theUser.picChangedAmount
        var newChangedAmount : Int = Int(changedString!)!
        newChangedAmount += 1
        let newChangedAmountString = String(newChangedAmount)
        let timeLastChanged = "\(Date().millisecondsSince1970)"
        
        self.userDataReference?.child("picChangedAmount").setValue(newChangedAmountString) { (snapshot, error) in
            self.userDataReference?.child("timeLastChanged").setValue(timeLastChanged)
        }
        self.Db?.child("publicProfiles").child(self.userId!).child("picChangedAmount").setValue(newChangedAmountString) { (snapshot, error) in
            self.Db?.child("publicProfiles").child(self.userId!).child("timeLastChanged").setValue(timeLastChanged)
        }
        
        let image = UIImage(named: "profile_icon.png")
        let imageData = UIImagePNGRepresentation(image!)
        _ = self.storageRef?.putData(imageData!, metadata: nil) { (metadata, error) in
            guard metadata != nil else {
                return
            }
            self.setUpUserInfoAndPic()
        }
    }
    
    @objc func tappedProfilePic(gesture: UIGestureRecognizer) {
        if(!Reachability.isConnectedToNetwork()){
            let alert = UIAlertController(title: "", message: "you cannot change you profile picture unless you are connected to the internet", preferredStyle: .alert)
            let cancel = UIAlertAction(title: "Ok", style: .destructive, handler: { (action) -> Void in })
            alert.addAction(cancel)
            present(alert, animated: true, completion: nil)
        } else {
            if (gesture.view as? UIImageView) != nil {
                let alert = UIAlertController(title: "", message: "", preferredStyle: .alert)
                let action1 = UIAlertAction(title: "Take picture with phone", style: .default, handler: { (action) -> Void in
                    self.spinnerView = UIViewController.displaySpinner(onView: self.view)
                    self.pickFromCamera()
                })
                let action2 = UIAlertAction(title: "Choose picture from library", style: .default, handler: { (action) -> Void in
                    self.spinnerView = UIViewController.displaySpinner(onView: self.view)
                    self.pickFromPhotoLibrary()
                })
                let action3 = UIAlertAction(title: "Use my facebook profile picture", style: .default, handler: { (action) -> Void in
                    self.spinnerView = UIViewController.displaySpinner(onView: self.view)
                    self.getFacebookPicture()
                })
                let action4 = UIAlertAction(title: "Delete profile pic", style: .default, handler: { (action) -> Void in
                    self.spinnerView = UIViewController.displaySpinner(onView: self.view)
                    self.deleteProfilePic()
                })
                let cancel = UIAlertAction(title: "Cancel", style: .destructive, handler: { (action) -> Void in })
                
                alert.addAction(action1)
                alert.addAction(action2)
                alert.addAction(action3)
                alert.addAction(action4)
                alert.addAction(cancel)
                present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        let resizedImage = image.resizedto150KB()
        let imageData = UIImagePNGRepresentation(resizedImage!)
        if(imageData != nil){
            
            self.userDataReference?.child("picChangedAmount").observeSingleEvent(of: .value, with: { (snapshot) in
                var changedString = ""
                if let a = snapshot.value as? String{
                    changedString = a
                } else{
                    changedString = "0"
                }
                
                var newChangedAmount : Int = Int(changedString)!
                newChangedAmount += 1
                let newChangedAmountString = String(newChangedAmount)
                let timeLastChanged = "\(Date().millisecondsSince1970)"
                
                self.userDataReference?.child("picChangedAmount").setValue(newChangedAmountString) { (snapshot, error) in
                    self.userDataReference?.child("timeLastChanged").setValue(timeLastChanged)
                }
                self.Db?.child("publicProfiles").child(self.userId!).child("picChangedAmount").setValue(newChangedAmountString) { (snapshot, error) in
                    self.Db?.child("publicProfiles").child(self.userId!).child("timeLastChanged").setValue(timeLastChanged)
                }
                
                _ = self.storageRef?.putData(imageData!, metadata: nil) { (metadata, error) in
                    guard metadata != nil else {
                        return
                    }
                    self.setUpUserInfoAndPic()
                }
            })
            
        }
        
        dismiss(animated:true, completion: nil)
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil{
            print(error)
            return
        }
        print("succesfully logged in with facebook")
    }
    
    func goToLoginPage(){
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginPageID") as! ViewController
        self.present(nextViewController, animated: true, completion: nil)
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("logged out of facebook")
        try! Auth.auth().signOut()
        goToLoginPage()
    }
}
