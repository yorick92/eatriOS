//
//  AddCircleEventViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 12/02/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import RangeSeekSlider
import FirebaseDatabase
import FirebaseStorage
import Firebase
import CoreLocation
import GooglePlacePicker
import GoogleMaps

struct restaurantInfoListStruct : Decodable {
    let results : [restaurantInfoStruct]
}

struct restaurantInfoStruct : Decodable {
    let name : String?
    let place_id: String?
    let rating : Float?
    let vicinity: String?
    let photos : [photosStruct]?
    let geometry : locationStruct?
}

struct photosStruct : Decodable {
    let photo_reference : String?
}

struct locationStruct : Decodable {
    let location : latLngStruct?
}

struct latLngStruct : Decodable {
    let lat : Float?
    let lng : Float?
}

class AddEventViewController: UIViewController, CLLocationManagerDelegate, pickedRestProtocol, GMSPlacePickerViewControllerDelegate{
    var indexOfPickedRest = -1
    var allRestaurantOptions = [RestaurantItem]()
    var image : UIImage?
    var storageRef : StorageReference?
    
    var latitude : String = ""
    var longitude : String = ""
    
    let locationManager = CLLocationManager()
    var ref:DatabaseReference?
    
    @IBOutlet weak var maxPeopleLbl: UILabel!
    @IBOutlet weak var datePickerWheel: UIDatePicker!
    @IBOutlet weak var maxPeopleSlider: UISlider!
    @IBAction func maxPeopleSliderAction(_ sender: UISlider) {
        maxPeopleLbl.text = "Max People: " + String(Int(sender.value))
    }
    @IBOutlet weak var priceRangeSlider: RangeSeekSlider!
    @IBOutlet weak var pickedRestLbl: UILabel!
    @IBOutlet weak var pickedRestPic: UIImageView!
    
    @IBAction func setLocationButton(_ sender: Any) {
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        present(placePicker, animated: true, completion: nil)
    }
    
    @IBAction func getRestaurantsList(_ sender: Any) {
        let urlString = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + latitude + "," + longitude + "&type=restaurant&rankby=distance&key=AIzaSyCeLVUpx-gIibTYo10URfS1oyKNf72XbmM";
        let url = URL(string: urlString)
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error != nil{
                print("restaurantsListError")
                print(error ?? "error")
            }
            else{
                if let content = data {
                    do {
                        self.allRestaurantOptions.removeAll()
                        let restInfoList = try JSONDecoder().decode(restaurantInfoListStruct.self, from: content)
                        for object in restInfoList.results{
                            self.allRestaurantOptions.append(RestaurantItem(
                                name: object.name ?? "no name",
                                placeId: object.place_id ?? "no place id",
                                rating:  NSString(format: "%.1f", object.rating ?? -1.0) as String,
                                vicinity: object.vicinity ?? "no vicinity",
                                photoReference: object.photos?[0].photo_reference ?? "no photos",
                                lat: NSString(format: "%.13f", object.geometry?.location?.lat ?? 0.0) as String,
                                lng: NSString(format: "%.13f", object.geometry?.location?.lng ?? 0.0) as String
                            ))
                        }
                        DispatchQueue.main.async {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "restaurantsPopUp") as! RestaurantsPopUpViewController
                            vc.restaurantsList = self.allRestaurantOptions
                            vc.restProtocol = self
                            self.present(vc, animated: true, completion: nil)
                        }
                    } catch let jsonErr{
                        print("Error json: ", jsonErr)
                    }
                }
            }
        }
        task.resume()
    }
    
    @IBAction func addEvent(_ sender: Any) {
        if(indexOfPickedRest != -1){
            let dateStart = datePickerWheel.date
            let calendar = Calendar.current
            let date = String(calendar.component(.day, from: dateStart))
            let month = String(calendar.component(.month, from: dateStart))
            let year = String(calendar.component(.year, from: dateStart))
            let hours = String(calendar.component(.hour, from: dateStart))
            let minutes = String(calendar.component(.minute, from: dateStart))
            
            let thePickedRest = allRestaurantOptions[indexOfPickedRest]
            let locationName = thePickedRest.name
            let longitude = thePickedRest.lng
            let latitude = thePickedRest.lat
            let locationAddress = thePickedRest.vicinity
            let locationId = thePickedRest.placeId
            
            let newEventRef = ref!.child("Events").childByAutoId()
            let id = newEventRef.key
            let priceBegin = Int(priceRangeSlider.selectedMinValue)
            let priceEnd = Int(priceRangeSlider.selectedMaxValue)
            let newEvent = ["date": date, "month": month, "year": year, "hours": hours, "minutes": minutes, "maxPeople": NSString(format: "%.0f", maxPeopleSlider.value) as String, "priceBegin": String(describing: priceBegin), "priceEnd": String(describing: priceEnd), "eventId": id, "locationName": locationName, "longitude": longitude, "latitude": latitude, "locationAddress": locationAddress, "locationId" : locationId]
            newEventRef.setValue(newEvent)
            let userId = Auth.auth().currentUser!.uid
            newEventRef.child("joinedPeople").child(userId).setValue(userId)
            
            if(self.image != nil){
                let eventPictureRef = storageRef?.child("pics/" + "Events/" + id + "/eventPic.jpg")
                let data = UIImageJPEGRepresentation(self.image!, 1.0)
                _ = eventPictureRef?.putData(data!, metadata: nil) { (metadata, error) in
                    guard metadata != nil else {
                        return
                    }
                }
            }
            goBackToItem1MainPage()
        }
        showToast(message: "Pick a restaurant first")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Get a reference to the storage service using the default Firebase App
        storageRef = Storage.storage().reference()
        ref = Database.database().reference()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func showToast(message : String) {
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 100, y: self.view.frame.size.height-100, width: 200, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "IranSansMobile", size: 19)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        viewController.dismiss(animated: true, completion: nil)
        latitude = NSString(format: "%.13f", place.coordinate.latitude) as String
        longitude = NSString(format: "%.13f", place.coordinate.longitude) as String
        print("Place name \(place.name)")
        print("Place address \(String(describing: place.formattedAddress))")
        print("Place attributions \(String(describing: place.attributions))")
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        viewController.dismiss(animated: true, completion: nil)
        
        print("No place selected")
    }
    
    func dismissPresentedView() {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
        print(indexOfPickedRest)
        if(indexOfPickedRest != -1){
            let thePickedRest = allRestaurantOptions[indexOfPickedRest]
            let textPickedRest = "Restaurant: " + thePickedRest.name + "\nnear: " + thePickedRest.vicinity + "\nrating: " + thePickedRest.rating
            pickedRestLbl.numberOfLines = 0
            pickedRestLbl.text = textPickedRest
            print(thePickedRest.name)
            print(thePickedRest.photoReference)
            let session = URLSession(configuration: .default)
            
            if(thePickedRest.photoReference != "no photos"){
                let urlString = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + thePickedRest.photoReference + "&key=AIzaSyCeLVUpx-gIibTYo10URfS1oyKNf72XbmM";
                let restImageFromUrl = session.dataTask(with: URL(string: urlString)!){ (data, response, error) in
                    if let e = error{
                        print("an error occured: \(e)")
                    } else{
                        if(response as? HTTPURLResponse) != nil{
                            if let imageData = data{
                                self.image = UIImage(data: imageData)
                                DispatchQueue.main.async {
                                    self.pickedRestPic.image = self.image
                                }
                            } else{
                                DispatchQueue.main.async {
                                    self.pickedRestPic.image = UIImage(named: "noImageAvailable.png")
                                }
                                print("no image found")
                            }
                        } else{
                            print("no server response")
                        }
                    }
                }
                restImageFromUrl.resume()
            } else{
                DispatchQueue.main.async {
                    self.pickedRestPic.image = UIImage(named: "noImageAvailable.png")
                }
            }
        }
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[0]
        latitude = String(format:"%f", location.coordinate.latitude)
        longitude = String(format:"%f", location.coordinate.longitude)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func goBackToItem1MainPage(){
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "item1MainPageID") as! item1MainPageViewController
        self.present(nextViewController, animated: true, completion: nil)
    }
    
}
