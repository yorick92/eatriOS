//
//  ViewController.swift
//  eatr
//
//  Created by Yorick Bolster on 04/02/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import Firebase
import FirebaseDatabase
import FirebaseStorage


class ViewController: UIViewController, FBSDKLoginButtonDelegate {
    var usersReference : DatabaseReference?
    var facebookIdReference : DatabaseReference?
    var databaseReference : DatabaseReference?
    var storageRef : StorageReference?
    var waitForLoginGroup = DispatchGroup()
    
    func setUpDatabaseReferences(){
        usersReference = Database.database().reference().child("Users")
        facebookIdReference = Database.database().reference().child("facebookIds")
        databaseReference = Database.database().reference()
        storageRef = Storage.storage().reference()
    }
    
    func setUpOutlets(){
        let loginButton = FBSDKLoginButton()
        view.addSubview(loginButton)
        // frame is outdated, use constraints later
        loginButton.frame = CGRect(x: 16, y: 50, width: view.frame.width-32, height: 50)
        loginButton.delegate = self
        loginButton.readPermissions = ["email", "public_profile", "user_friends"]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpDatabaseReferences()
        setUpOutlets()
    }
    
    func getFacebookUserInfo(){
        let accessToken = FBSDKAccessToken.current()
        guard let accessTokenString = accessToken?.tokenString else {return}
        let credentials = FacebookAuthProvider.credential(withAccessToken: accessTokenString)
        print("hier")
        print(credentials)
        Auth.auth().signInAndRetrieveData(with: credentials) { (user, error) in
            if error != nil {
                print("something when wrong with our FB user: ", error ?? "")
                return
            }
            print("successfully logged in with our user: ", user ?? "")
            self.waitForLoginGroup.leave()
            
            FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email, picture.width(250).height(250).as(picture), gender, friends"]).start { (connection, result, error) in
                
                if error != nil{
                    print("failed to start graph request: ", error ?? "no error message found")
                    return
                }
                
                let userId = Auth.auth().currentUser!.uid
                print(userId)
                var facebookPicUrlString = ""
                print(result ?? "no result found in graph request")
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: result!, options: .prettyPrinted)
                    let facebookUser = try JSONDecoder().decode(facebookUserInfo.self, from: jsonData)
                    print(facebookUser)
                    let timeLastChanged = "\(Date().millisecondsSince1970)"
                    
                    self.usersReference?.child(userId).child("email").setValue(facebookUser.email)
                    self.usersReference?.child(userId).child("name").setValue(facebookUser.name)
                    self.usersReference?.child(userId).child("gender").setValue(facebookUser.gender)
                    self.usersReference?.child(userId).child("timeLastChanged").setValue(timeLastChanged)
                    self.usersReference?.child(userId).child("facebookId").setValue(facebookUser.id)
                    self.databaseReference?.child("facebookIds").child(facebookUser.id!).setValue(userId)
                    print(facebookUser.id!)
                    
                    self.databaseReference?.child("publicProfiles").child(userId).child("name").setValue(facebookUser.name)
                    self.databaseReference?.child("publicProfiles").child(userId).child("gender").setValue(facebookUser.gender)
                    self.databaseReference?.child("publicProfiles").child(userId).child("timeLastChanged").setValue(timeLastChanged)
                    
                    var friendList = [String : String]()
                    if(facebookUser.friends != nil){
                        let myGroup = DispatchGroup()
                        print((facebookUser.friends?.data)!)
                        for friend in (facebookUser.friends?.data)!{
                            myGroup.enter()
                            self.facebookIdReference?.child(friend.id!).observeSingleEvent(of: .value, with: { (snapshot) in
                                if let firebaseId = snapshot.value as? String{
                                    friendList[firebaseId] = friend.name!
                                }
                                myGroup.leave()
                            })
                        }
                        myGroup.notify(queue: .main){
                            self.usersReference?.child(userId).child("facebookFriends").setValue(friendList)
                        }
                    }
                    
                    
                    facebookPicUrlString = (facebookUser.picture?.data?.url)!
                    self.usersReference?.child(userId).child("facebookPicUrl").setValue(facebookPicUrlString)
                    
//                    let session = URLSession(configuration: .default)
//                    let facebookPicUrl = URL(string: facebookPicUrlString)
//                    let getImageFromUrl = session.dataTask(with: facebookPicUrl!) { (data, response, error) in
//                        if let e = error {
//                            print("Error Occurred: \(e)")
//                        } else {
//                            if (response as? HTTPURLResponse) != nil {
//                                let profilePicref = self.storageRef?.child("pics/" + "Users/" + userId + "/profile.jpg")
//                                _ = profilePicref?.putData(data!, metadata: nil) { (metadata, error) in
//                                    guard metadata != nil else {
//                                        return
//                                    }
//                                }
//                            } else {
//                                print("No response from server")
//                            }
//                        }
//                    }
//                    getImageFromUrl.resume()
                    
                    
                } catch{
                    print("facebookgraphrequesterror at json decoder at login")
                }
            }
        }
    }
    
    func goToMainPage(){
        let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "MainPageViewControllerID") as! MainPageViewController
        self.present(nextViewController, animated: true, completion: nil)
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil{
            print(error)
            return
        }
        print("succesfully logged in with facebook")
        waitForLoginGroup.enter()
        getFacebookUserInfo()
        waitForLoginGroup.notify(queue: .main){
            print("ayy")
            self.goToMainPage()
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("logged out of facebook")
    }
}

