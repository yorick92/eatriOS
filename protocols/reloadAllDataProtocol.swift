//
//  reloadAllDataProtocol.swift
//  eatr
//
//  Created by Yorick Bolster on 10/05/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import Foundation
protocol reloadAllDataProtocol {
    func reloadEverything()
}
