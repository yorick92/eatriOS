//
//  addMoreInvitesProtocol.swift
//  eatr
//
//  Created by Yorick Bolster on 16/04/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import Foundation
protocol addMoreInvitesProtocol {
    var newInvites : [localContactStruct] {get set}
    
    func dismissMoreInvites()
}
