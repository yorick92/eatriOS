//
//  pickedRestProtocol.swift
//  eatr
//
//  Created by Yorick Bolster on 10/03/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import Foundation
protocol pickedRestProtocol {
    
    var indexOfPickedRest : Int {get set}
    
    func dismissPresentedView()
}
