//
//  Structs.swift
//  eatr
//
//  Created by Yorick Bolster on 19/04/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import Foundation
import UIKit

// for the chat function
struct message {
    let senderId : String!
    let message : String!
    let dateInMs : String!
    let messageId : String!
    let userName : String!
}

// for opening profile in OpenEvent
class MyTapGesture: UITapGestureRecognizer {
    var userId = String()
}

// used to get the json information you get from restaurants into a class structure.
struct restaurantInfoListStruct : Decodable {
    let results : [restaurantInfoStruct]
}

struct restaurantInfoStruct : Decodable {
    let name : String?
    let place_id: String?
    let price_level : Int?
    let rating : Float?
    let vicinity: String?
    let photos : [photosStruct]?
    let geometry : locationStruct?
    let icon : String?
}

struct photosStruct : Decodable {
    let photo_reference : String?
}

struct locationStruct : Decodable {
    let location : latLngStruct?
}

struct latLngStruct : Decodable {
    let lat : Float?
    let lng : Float?
}

// used to get json information you get from facebook api into a class structure
struct facebookUserInfo : Decodable {
    let email : String?
    let friends : friendsStruct?
    let gender : String?
    let id : String?
    let name : String?
    let picture : pictureStruct?
}

struct pictureStruct : Decodable {
    let data : pictureDataStruct?
}

struct pictureDataStruct : Decodable {
    let url : String?
}

struct friendsStruct : Decodable {
    let data : [friendStruct]?
}

struct friendStruct : Decodable {
    let id : String?
    let name : String?
}


// used to parse json data to a class structure when getting facebook friends
struct facebookFriendInfo : Decodable {
    let friends : friendsStruct?
}

// used to represent a preset in class form
struct localPresetStruct {
    let name : String?
    let id : String?
    let contacts : [localContactStruct]?
}

// used to represent a group in class form
struct localGroupStruct {
    let name : String?
    let id : String?
    let admin : String?
    let contacts : [localContactStruct]?
}

// used to represent a friend in class form (maybe not used anymore)
struct localFacebookFriendStruct{
    let id : String?
    let name : String?
}

// used to represent a friend in class form, but with cuisine
struct localContactStruct{
    let id : String?
    let name : String?
    let favCuis : String?
    let favRest : String?
    let gender : String?
    let picChangedAmount : String?
    let timeLastChanged : String?
}
