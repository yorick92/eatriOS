//
//  Event.swift
//  
//
//  Created by Yorick Bolster on 08/02/2018.
//

import Foundation

class Event{
    var creator : String
    var eventId : String
    var isGroupEvent : Bool
    var latitude : String
    var locationAddress : String
    var locationId : String
    var locationName : String
    var longitude : String
    var maxPeople: String
    var dateStart: Date
    var eventName : String
    var picChangedAmount : String
    var priceBegin: String
    var priceEnd: String
    var remarks : String
    var typeOfEvent : String
    var timeMade : String
    var timeLastChanged : String
    var nrOfPeopleInvited : String
    var joinedPeople : [localContactStruct]
    var invitedPeople : [localContactStruct]
    
    init(creator : String, eventId: String, isGroupEvent : Bool, latitude : String, locationAddress: String, locationId : String, locationName : String,
         longitude : String, maxPeople: String, dateStart: Date, eventName : String, picChangedAmount : String,
         priceBegin: String, priceEnd: String, remarks : String, typeOfEvent : String,
         timeMade : String, timeLastChanged : String, nrOfPeopleInvited : String, joinedPeople : [localContactStruct], invitedPeople : [localContactStruct]){
        self.creator = creator
        self.eventId = eventId
        self.isGroupEvent = isGroupEvent
        self.latitude = latitude
        self.locationAddress = locationAddress
        self.locationId = locationId
        self.locationName = locationName
        self.longitude = longitude
        self.maxPeople = maxPeople
        self.dateStart = dateStart
        self.eventName = eventName
        self.picChangedAmount = picChangedAmount
        self.priceBegin = priceBegin
        self.priceEnd = priceEnd
        self.remarks = remarks
        self.typeOfEvent = typeOfEvent
        self.timeMade = timeMade
        self.timeLastChanged = timeLastChanged
        self.joinedPeople = joinedPeople
        self.invitedPeople = invitedPeople
        self.nrOfPeopleInvited = nrOfPeopleInvited
    }
}
