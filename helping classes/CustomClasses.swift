//
//  CustomClasses.swift
//  eatr
//
//  Created by Yorick Bolster on 14/05/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import Foundation
import UIKit

class CustomNavigationBar : UINavigationBar{
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let newSize = CGSize(width: self.frame.size.width, height: 100)
        return newSize
    }
}
