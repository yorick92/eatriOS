//
//  RestaurantItem.swift
//  eatr
//
//  Created by Yorick Bolster on 09/03/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import Foundation

extension FloatingPoint{
    var degreesToRadians : Self { return self * .pi / 180}
    var radiansToDegrees : Self { return self * 180 / .pi}
}

func getDistance (lat1: String, long1: String, lat2: String, long2: String) -> Double{
    let uLat : Double = Double(lat1)!
    let uLong : Double = Double(long1)!
    
    let eLat : Double = Double(lat2)!
    let eLong : Double = Double(long2)!
    
    let radius = 6371.0 //km
    
    let deltaP = (eLat.degreesToRadians - uLat.degreesToRadians)
    let deltaL = (eLong.degreesToRadians - uLong.degreesToRadians)
    let a = sin(deltaP/2) * sin(deltaP/2) + cos(uLat.degreesToRadians) * cos(eLat.degreesToRadians) * sin(deltaL/2) * sin(deltaL/2)
    let c = 2 * atan2(sqrt(a), sqrt(1-a))
    var d = radius * c
    d = Double(round(d * 1000)/1000)
    
    return d
}

class RestaurantItem{
    var name : String
    var placeId : String
    var rating : String
    var vicinity : String
    var photoReference : String
    var lat : String
    var lng : String
    var distance : Double
    var icon : String
    var priceLevel : Int
    
    init(name : String, placeId: String, rating : String, vicinity : String, photoReference : String, lat : String, lng : String, personLat: String, personLng: String, icon : String, priceLevel : Int){
        self.name = name
        self.placeId = placeId
        self.rating = rating
        self.vicinity = vicinity
        self.photoReference = photoReference
        self.lat = lat
        self.lng = lng
        self.distance = getDistance(lat1: lat, long1: lng, lat2: personLat, long2: personLng)
        self.icon = icon
        self.priceLevel = priceLevel
    }
}

