//
//  Extensions.swift
//  eatr
//
//  Created by Yorick Bolster on 19/04/2018.
//  Copyright © 2018 Yorick Bolster. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import CoreData
import Firebase

// used to get miliseconds since 1970
extension Date {
    var millisecondsSince1970:Int {
        return Int((self.timeIntervalSince1970 * 1000.0).rounded())
    }
    
    init(milliseconds:Int) {
        self = Date(timeIntervalSince1970: TimeInterval(milliseconds / 1000))
    }
}

// used to easily initialize color
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        let newRed = CGFloat(red)/255
        let newGreen = CGFloat(green)/255
        let newBlue = CGFloat(blue)/255
        
        self.init(red: newRed, green: newGreen, blue: newBlue, alpha: 1.0)
    }
}

// used to compress images
extension UIImage {
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    func resizedto150KB() -> UIImage? {
        guard let imageData = UIImagePNGRepresentation(self) else { return nil }
        
        var resizingImage = self
        var imageSizeKB = Double(imageData.count) / 1024.0
        
        while imageSizeKB > 150 {
            var resizedImage : UIImage
            var imageData : Data
            if(imageSizeKB > 1000){
                resizedImage = resizingImage.resized(withPercentage: 0.5)!
                imageData = UIImagePNGRepresentation(resizedImage)!
            }else if(imageSizeKB > 300){
                resizedImage = resizingImage.resized(withPercentage: 0.75)!
                imageData = UIImagePNGRepresentation(resizedImage)!
            }else{
                resizedImage = resizingImage.resized(withPercentage: 0.90)!
                imageData = UIImagePNGRepresentation(resizedImage)!
            }
            resizingImage = resizedImage
            imageSizeKB = Double(imageData.count) / 1024.0
        }
        return resizingImage
    }
}

extension UILabel {
    func set(image: UIImage, with text: String) {
        let attachment = NSTextAttachment()
        attachment.image = image
        attachment.bounds = CGRect(x: 0, y: 0, width: 10, height: 10)
        let attachmentStr = NSAttributedString(attachment: attachment)
        
        let mutableAttributedString = NSMutableAttributedString()
        mutableAttributedString.append(attachmentStr)
        
        let textString = NSAttributedString(string: text, attributes: [.font: self.font])
        mutableAttributedString.append(textString)
        
        self.attributedText = mutableAttributedString
    }
}

extension UIView{
    
    func fadeOut(){
        UIView.animate(withDuration: 0.5, animations: {
            self.alpha = 0
        }, completion: { (value: Bool) in
            self.isHidden = true
        })
    }
    
    func fadeIn(){
        self.isHidden = false
        UIView.animate(withDuration: 0.5, animations: {
            self.alpha = 1
        }, completion: { (value: Bool) in
            
        })
    }
    
    func addBlurDark(_ alpha: CGFloat = 0.5) {
        // create effect
        let effect = UIBlurEffect(style: .dark)
        let effectView = UIVisualEffectView(effect: effect)
        
        // set boundry and alpha
        effectView.frame = self.bounds
        effectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        effectView.alpha = alpha
        
        self.addSubview(effectView)
    }
    
    func addBlurLight(_ alpha: CGFloat = 0.5) {
        // create effect
        let effect = UIBlurEffect(style: .extraLight)
        let effectView = UIVisualEffectView(effect: effect)
        
        // set boundry and alpha
        effectView.frame = self.bounds
        effectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        effectView.alpha = alpha
        
        self.addSubview(effectView)
    }
    
    func addTopBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0,y: 0, width:self.frame.size.width, height:width)
        self.layer.addSublayer(border)
    }
    
    func addRightBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: self.frame.size.width - width,y: 0, width:width, height:self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func addBottomBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0, y:self.frame.size.height - width, width:self.frame.size.width, height:width)
        self.layer.addSublayer(border)
    }
    
    func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x:0, y:0, width:width, height:self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    func addShadow(){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowRadius = 5
    }
    
    func addShadowBottom(){
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 1
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        self.layer.shadowRadius = 2
    }
}

extension UIImageView{
    func makePortrait(){
        self.layer.borderWidth = 1.0
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.cornerRadius = self.frame.width/2
        self.clipsToBounds = true
    }
    
}

extension UIViewController {
    
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    func addTopViewColor(colorNavBar : Bool){
        let topView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 20))
        topView.backgroundColor = UIColor(red: 191, green: 42, blue: 42)
        view.addSubview(topView)
        
        if(colorNavBar){
            navigationController?.navigationBar.setBackgroundImage(UIImage(named: "1x1clear.png"), for: .default)
            navigationController?.navigationBar.backgroundColor = UIColor(red: 191, green: 42, blue: 42)
        }
    }
    
    class func displaySpinner(onView : UIView) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        
        return spinnerView
    }
    
    class func removeSpinner(spinner :UIView) {
        DispatchQueue.main.async {
            spinner.removeFromSuperview()
        }
    }
    
    func showToast(message : String) {
        let toastLabel = UITextView(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 20;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func showNoInternetMessage() {
        let toastLabel = UITextView(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-135, width: 150, height: 70))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.text = "Your device is not connected to the internet,\n the app is in view only mode"
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 20;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func contactExists(contactId : String, entityName : String, context : NSManagedObjectContext!)->NSManagedObject?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(request)
            if results.count > 0{
                if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "userId") as! String == contactId}){
                    return result as? NSManagedObject;
                } else {
                    return nil
                }
            }
        } catch{
            return nil
        }
        return nil
    }
    
    func groupExists(groupId : String, context : NSManagedObjectContext!)->NSManagedObject?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Group")
        request.returnsObjectsAsFaults = false
        
        do {
            let results = try context.fetch(request)
            if results.count > 0{
                if let result = results.first(where: {($0 as! NSManagedObject).value(forKey: "id") as! String == groupId}){
                    return result as? NSManagedObject;
                } else {
                    return nil
                }
            }
        } catch{
            return nil
        }
        return nil
    }
    
    func getPicDataForUser(theUserId : String, picChangedAmount : String, completionHandler: @escaping (_ picData: NSData) -> ()){
//        print(picChangedAmount)
//        print(theUserId)
        if(UserDefaults.standard.object(forKey: theUserId + "/" + picChangedAmount ) != nil){
            completionHandler(UserDefaults.standard.object(forKey: theUserId + "/" + picChangedAmount) as! NSData)
        } else {
            // remove old one from local database
            var amountChanged : Int = Int(picChangedAmount)!
            amountChanged -= 1
            let oldEventChangedAmount = String(amountChanged)
            UserDefaults.standard.removeObject(forKey: theUserId + "/" + oldEventChangedAmount)
            
            let profilePicRef = Storage.storage().reference(withPath: "pics/Users/" + theUserId + "/profile.jpg")
            profilePicRef.getData(maxSize: 1 * 1024 * 1024) { data, error in
                if let error = error {
                    DispatchQueue.main.async {
                        let image = UIImage(named: "profile_icon.png")
                        let imageData : NSData = UIImagePNGRepresentation(image!)! as NSData
                        UserDefaults.standard.set(imageData, forKey: theUserId + "/" + picChangedAmount )
                        completionHandler(imageData)
                    }
                    print(error)
                } else {
                    DispatchQueue.main.async {
                        let image = UIImage(data: data!)
                        let imageData : NSData = UIImagePNGRepresentation(image!)! as NSData
                        UserDefaults.standard.set(imageData, forKey: theUserId + "/" + picChangedAmount )
                        completionHandler(imageData)
                    }
                }
            }
        }
    }
}
